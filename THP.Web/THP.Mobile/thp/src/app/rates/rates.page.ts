import { UserModel } from './../_shared/models/user.model';
import { BookingService } from './../_shared/services/booking.service';
import { UserService } from './../_shared/services/user.service';
import { BookingModel } from './../_shared/models/booking.model';
import { Component, OnInit, Input } from '@angular/core';
import { LocationModel } from '../_shared/models/location.model';
import { RateService } from '../_shared/services/rate.service';
import { forkJoin } from 'rxjs';
import { TravelRateModel } from '../_shared/models/travel-rate.model';
import { Router } from '@angular/router';
import { DiscountModel } from '../_shared/models/discount.model';
import { DiscountService } from '../_shared/services/discount.service';

@Component({
  selector: 'app-rates',
  templateUrl: './rates.page.html',
  styleUrls: ['./rates.page.scss'],
})
export class RatesPage implements OnInit {
  readonly BOOKING_STATUS_QUEUED: string = 'Queued';
  readonly BOOKING_STATUS_ONGOING: string = 'On Going';

  locations: Array<LocationModel> = new Array<LocationModel>();
  travelRates: Array<TravelRateModel> = new Array<TravelRateModel>();
  metroManilaLocationOnly: Array<LocationModel> = new Array<LocationModel>();
  selectedFrom: LocationModel = new LocationModel();
  selectedTo: LocationModel = new LocationModel();
  selectedTravelRate: TravelRateModel = new TravelRateModel();
  selectedTrip = 'One Way Trip';
  travelTrips: Array<string> = ['One Way Trip', 'Two Way Trip'];
  booking: BookingModel = new BookingModel();
  discount: DiscountModel = new DiscountModel();
  bookings: Array<BookingModel> = new Array<BookingModel>();
  activeUser: UserModel = new UserModel();
  unavailableDates: Array<string> = new Array<string>();

  myDate = '';
  datePickerObj: any = {
    showTodayButton: true,
    mondayFirst: true,
    setLabel: 'Set',
    todayLabel: 'Today',
    closeLabel: 'Close',
    titleLabel: 'Select a Date',
    monthsList: ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    weeksList: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    dateFormat: 'YYYY-MM-DD',
    clearButton: false,
    yearInAscending: true,
    btnCloseSetInReverse: true,
    btnProperties: {
      expand: 'block',
      fill: '',
      size: '',
      disabled: '',
      strong: '',
      color: ''
    },
    arrowNextPrev: {
      nextArrowSrc: 'assets/images/right-arrow.svg',
      prevArrowSrc: 'assets/images/left-arrow.svg'
    },
    fromDate: new Date()
  };

  constructor(
    private rateService: RateService,
    private router: Router,
    private userService: UserService,
    private bookingService: BookingService,
    private discountService: DiscountService
  ) { }

  ngOnInit(): void {
    this.initializeData();
  }

  initializeData(): void {
    this.discount = this.discountService.getActiveDiscount();
    this.activeUser = this.userService.getActiveAccount();
    this.getAllLocationsRatesBookings();
  }

  getAllLocationsRatesBookings(): void {
    forkJoin([this.rateService.getLocations(), this.rateService.getRates(),
      this.bookingService.getBookings(this.activeUser.id), this.bookingService.getUnavailableDates()])
      .subscribe(response => {
        this.locations = response[0];
        this.travelRates = response[1];
        this.bookings = response[2];
        this.unavailableDates = response[3];
        this.metroManilaLocationOnly = this.locations.filter((x: LocationModel) => x.name === 'Metro Manila');
        this.selectedFrom = this.locations.find((x: LocationModel) => x.name === 'Metro Manila');
      }, () => {
        alert('An error has occured.');
      });
  }

  computePrice(): void {
    this.selectedTravelRate = this.travelRates
      .find((x: TravelRateModel) => x.pickupPoint.id === this.selectedFrom.id && x.destinationPoint.id === this.selectedTo.id);
    if (this.selectedTrip === this.travelTrips[1]) {
      this.booking.totalPrice = this.selectedTravelRate.fixedPrice * 2;
    } else {
      this.booking.totalPrice = this.selectedTravelRate.fixedPrice;
    }
  }

  createBooking(): void {
    this.booking.userId = this.userService.getActiveAccount().id;
    this.booking.travelRate = this.selectedTravelRate;
    this.booking.currency = 'PHP';
    this.booking.discountId = this.discount.id;
  }

  goToPaymentOptions(): void {
    this.createBooking();
    this.bookingService.setActiveBooking(this.booking);
    this.router.navigate(['/payment-options']);
  }

  checkPendingBooking(): boolean {
    return this.bookings.find((x: BookingModel) => x.status === this.BOOKING_STATUS_QUEUED || x.status === this.BOOKING_STATUS_ONGOING) !== undefined;
  }
}
