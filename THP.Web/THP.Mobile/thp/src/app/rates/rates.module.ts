import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {MatDatepickerModule} from '@angular/material/datepicker';

import { IonicModule } from '@ionic/angular';

import { RatesPage } from './rates.page';
import { MatNativeDateModule } from '@angular/material';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';

const routes: Routes = [
  {
    path: '',
    component: RatesPage
  }
];

@NgModule({
  imports: [
    Ionic4DatepickerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RatesPage]
})
export class RatesPageModule {}
