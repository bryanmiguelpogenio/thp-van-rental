import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule',
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginPageModule'
  },
  {
    path: 'registration',
    loadChildren: './registration/registration.module#RegistrationPageModule'
  },
  { path: 'rates', loadChildren: './rates/rates.module#RatesPageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'payment-options', loadChildren: './payment-options/payment-options.module#PaymentOptionsPageModule' },
  { path: 'booked', loadChildren: './booked/booked.module#BookedPageModule' },
  { path: 'pending-booking', loadChildren: './pending-booking/pending-booking.module#PendingBookingPageModule' },
  { path: 'cancel-trip', loadChildren: './cancel-trip/cancel-trip.module#CancelTripPageModule' },
  { path: 'rate-trip', loadChildren: './rate-trip/rate-trip.module#RateTripPageModule' },
  { path: 'home-driver', loadChildren: './home-driver/home-driver.module#HomeDriverPageModule' },
  { path: 'history-driver', loadChildren: './history-driver/history-driver.module#HistoryDriverPageModule' },
  { path: 'driver-location', loadChildren: './driver-location/driver-location.module#DriverLocationPageModule' }




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
