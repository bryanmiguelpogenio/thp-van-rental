import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UserCarService } from './../_shared/services/user-car.service';
import { UserModel } from './../_shared/models/user.model';
import { UserService } from './../_shared/services/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { UserCarModel } from '../_shared/models/user-car.model';

@Component({
  selector: 'app-home-driver',
  templateUrl: './home-driver.page.html',
  styleUrls: ['./home-driver.page.scss'],
})
export class HomeDriverPage implements OnInit {
  activeDriver: UserModel;
  activeUserCar: UserCarModel;
  watcher: Observable<Geoposition>;

  constructor(
    private userService: UserService,
    private geoLocation: Geolocation,
    private userCarService: UserCarService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activeDriver = this.userService.getActiveAccount();
    this.getUserCar();
  }

  getUserCar(): void {
    this.userCarService.getUserCar(this.activeDriver.id).subscribe((userCarResponse: UserCarModel) => {
      this.activeUserCar = userCarResponse;
      this.userService.setIsUserLoggedIn(true);
    }, () => {
      this.userService.setIsUserLoggedIn(false);
      alert('An error has occured while setting up your location settings. You will be redirected to login page. Please try to relogin.');
      localStorage.clear();
      this.router.navigate(['/login']);
    }).add(() => {
      if (this.userService.getIsUserLoggedIn()) {
        this.setupDriverLocationWatch();
      }
    });
  }

  setupDriverLocationWatch(): void {
    // this.watcher = this.geoLocation.watchPosition();
    // this.watcher.subscribe((positionResponse: Geoposition) => {
    //   this.userCarService.updateUserCarLocation(this.activeUserCar.id, `${positionResponse.coords.latitude},${positionResponse.coords.longitude}`)
    //     .subscribe(() => {
    //       console.log('Updated');
    //     });
    // });

    const driverWatcherId = setInterval(() => {
      this.geoLocation.getCurrentPosition({ enableHighAccuracy: true, maximumAge: 0 })
      .then((locationResponse: Geoposition) => {
        this.userCarService.updateUserCarLocation(this.activeUserCar.id, `${locationResponse.coords.latitude},${locationResponse.coords.longitude}`)
        .subscribe(() => {
          console.log('Updated');
        });
      }).catch(() => {
        alert('An error has occurred while getting your current map details.');
      });
    }, 5000);

    this.userService.setDriverWatcherId(driverWatcherId);
  }
}
