import { UserModel } from './../_shared/models/user.model';
import { DiscountModel } from './../_shared/models/discount.model';
import { DiscountService } from './../_shared/services/discount.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { UserService } from '../_shared/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  discounts: Array<DiscountModel> = new Array<DiscountModel>();
  activeUser: UserModel = new UserModel();
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  constructor(
    private router: Router,
    private discountService: DiscountService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.initializeData();
  }

  initializeData(): void {
    this.activeUser = this.userService.getActiveAccount();
    forkJoin([this.discountService.getDiscounts()])
      .subscribe((response: Array<any>) => {
        this.discounts = response[0];
      }, () => {
        alert('An error has occured.');
      });
  }

  goToRates(): void {
    this.router.navigate(['/rates']);
  }

  goToRatesWithDiscount(discount: DiscountModel): void {
    this.discountService.setActiveDiscount(discount);
    this.router.navigate(['/rates']);
  }
}
