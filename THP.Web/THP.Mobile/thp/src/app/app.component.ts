import { UserModel } from './_shared/models/user.model';
import { Component, OnInit, ViewChild } from '@angular/core';

import { Platform, IonRouterOutlet } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { UserService } from './_shared/services/user.service';
import { UserRole } from './_shared/enums/user-role.enum';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {
  activeUser: UserModel;
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Profile',
      url: '/profile',
      icon: 'contact'
    },
    {
      title: 'History',
      url: '/list',
      icon: 'list'
    }
  ];

  @ViewChild(IonRouterOutlet) routerOutlet: IonRouterOutlet;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private userService: UserService
  ) {
    this.initializeApp();
  }

  ngOnInit(): void {
    this.activeUser = this.userService.getActiveAccount();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.platform.backButton.subscribeWithPriority(0, () => {
        if (this.router.url === '/login' || this.router.url === '/home' || this.router.url === '/home-driver') {
          navigator['app'].exitApp();
        } else {
          this.routerOutlet.pop();
        }
      });
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  logout(): void {
    clearInterval(this.userService.getDriverWatcherId());
    localStorage.clear();
  }

  onMenuOpen(): void {
    this.activeUser = this.userService.getActiveAccount();
    if (this.activeUser && this.activeUser.roleId === UserRole.Customer) {
      this.appPages[0].url = '/home';
      this.appPages[2].url = '/list';
    } else if (this.activeUser && this.activeUser.roleId === UserRole.Driver) {
      this.appPages[0].url = '/home-driver';
      this.appPages[2].url = '/history-driver';
    }
  }
}
