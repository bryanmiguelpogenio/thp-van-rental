import { UserCarModel } from './../_shared/models/user-car.model';
import { BookingService } from './../_shared/services/booking.service';
import { BookingModel } from './../_shared/models/booking.model';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GoogleMaps, GoogleMap, GoogleMapOptions, Marker, Environment } from '@ionic-native/google-maps';
import { UserCarService } from '../_shared/services/user-car.service';

@Component({
  selector: 'app-driver-location',
  templateUrl: './driver-location.page.html',
  styleUrls: ['./driver-location.page.scss'],
})
export class DriverLocationPage implements OnInit, OnDestroy {
  googleMap: GoogleMap;
  mobilePosition: Coordinates;
  activeBooking: BookingModel;
  refreshIntervalId: any;

  constructor(
    private geoLocation: Geolocation,
    private userCarService: UserCarService,
    private bookingService: BookingService
  ) { }

  ngOnInit(): void {
    this.activeBooking = this.bookingService.getActiveBooking();
    this.initializeMap();
  }

  ngOnDestroy(): void {
    clearInterval(this.refreshIntervalId);
  }

  initializeMap(): void {
    this.geoLocation.getCurrentPosition({ enableHighAccuracy: true, maximumAge: 0 })
      .then((locationResponse: Geoposition) => {
        this.mobilePosition = locationResponse.coords;
        this.loadMapDetails();
      }).catch(() => {
        alert('An error has occurred while getting your current map details.');
      });
  }

  loadMapDetails() {
    // This code is necessary for browser
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': '',
      'API_KEY_FOR_BROWSER_DEBUG': ''
    });

    const mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: this.mobilePosition.latitude,
          lng: this.mobilePosition.longitude
        },
        zoom: 11
      },

    };

    this.googleMap = GoogleMaps.create('mapContainer', mapOptions);

    this.googleMap.addMarkerSync({
      title: 'Your Position',
      icon: 'blue',
      animation: 'DROP',
      position: {
        lat: this.mobilePosition.latitude,
        lng: this.mobilePosition.longitude
      }
    });

    let driverLocation: Marker = null;
    this.userCarService.getUserCar(this.activeBooking.driverUser.id).subscribe((userCarResponse: UserCarModel) => {
      driverLocation = this.googleMap.addMarkerSync({
        title: 'Driver Position',
        icon: 'red',
        animation: 'DROP',
        position: {
          lat: Number(userCarResponse.location.split(',')[0]),
          lng: Number(userCarResponse.location.split(',')[1])
        }
      });

      driverLocation.showInfoWindow();
    });

    this.refreshIntervalId = setInterval(() => {
      this.userCarService.getUserCar(this.activeBooking.driverUser.id).subscribe((userCarResponse: UserCarModel) => {
        driverLocation.setPosition({ lat: Number(userCarResponse.location.split(',')[0]), lng: Number(userCarResponse.location.split(',')[1]) });
      });
    }, 5000);
  }
}
