import { UserService } from './../_shared/services/user.service';
import { UserModel } from './../_shared/models/user.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoleModel } from '../_shared/models/role.model';
import { UserRole } from '../_shared/enums/user-role.enum';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  accountRegister: UserModel = new UserModel();
  roles: Array<RoleModel> = new Array<RoleModel>();
  confirmPass = '';
  isRegistering = false;

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() { }

  initializeData(): void {
    this.userService.getRoles().subscribe((response: Array<RoleModel>) => {
      this.roles = response;
    });
  }

  register(): void {
    this.isRegistering = true;
    this.accountRegister.roleId = UserRole.Customer;
    this.userService.registerUser(this.accountRegister).subscribe((id: number) => {
      if (id) {
        alert('Username or Email has already been used.');
      } else if (this.validatePass()) {
        alert('Password Mismatch.');
      } else {
        alert('Account Successfully Created. Please wait for the email confirmation.');
        this.router.navigate(['/login']);
      }
    }, () => {
      alert('An error has occured');
    }).add(() => {
      this.isRegistering = false;
    });
  }

  validatePass(): boolean {
    if (this.confirmPass.length >= 3 && this.accountRegister.password.length >= 3) {
      return this.confirmPass === this.accountRegister.password ? false : true;
    }
    return false;
  }
}
