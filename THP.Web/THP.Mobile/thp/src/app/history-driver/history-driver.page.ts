import { BookingService } from './../_shared/services/booking.service';
import { BookingModel } from './../_shared/models/booking.model';
import { Component, OnInit } from '@angular/core';
import { UserModel } from '../_shared/models/user.model';
import { UserService } from '../_shared/services/user.service';
@Component({
  selector: 'app-history-driver',
  templateUrl: './history-driver.page.html',
  styleUrls: ['./history-driver.page.scss'],
})
export class HistoryDriverPage implements OnInit {
  readonly BOOKING_STATUS_ONGOING: string = 'On Going';
  readonly BOOKING_STATUS_QUEUED: string = 'Queued';
  bookings: Array<BookingModel> = new Array<BookingModel>();
  activeUser: UserModel = new UserModel();

  constructor(
    private bookingService: BookingService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.initializeData();
  }

  initializeData(): void {
    this.activeUser = this.userService.getActiveAccount();
    this.bookingService.getDriverBookings(this.activeUser.id).subscribe((response: Array<BookingModel>) => {
      this.bookings = response;
    }, () => {
      alert('An error has occured.');
    });
  }

  setToOnGoing(selectedBooking: BookingModel): void {
    selectedBooking.status = this.BOOKING_STATUS_ONGOING;
    this.bookingService.updateBooking(selectedBooking).subscribe(() => {
      alert('Booking successfully updated.');
      this.initializeData();
    });
  }
}
