import { BookingService } from './../_shared/services/booking.service';
import { BookingModel } from './../_shared/models/booking.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { PaymentType } from '../_shared/enums/payment-type.enum';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-payment-options',
  templateUrl: './payment-options.page.html',
  styleUrls: ['./payment-options.page.scss'],
})
export class PaymentOptionsPage implements OnInit {
  booking: BookingModel = new BookingModel();
  purchaseUrl: string;
  tokenUrl: string;
  isPaying = false;

  constructor(
    private router: Router,
    private bookingService: BookingService
  ) { }

  ngOnInit() {
    this.initializeData();
  }

  initializeData(): void {
    this.purchaseUrl = `${environment.endpoint}checkout/paywithpaypal`;
    this.tokenUrl = `${environment.endpoint}checkout/getclienttoken`;
    this.booking = this.bookingService.getActiveBooking();
  }

  onPaymentStatus(response: any) {
    if (response.Target || response.Transaction) {
      this.booking.transactionId = response.Target ? response.Target.Id : response.Transaction.Id;
      const responseDetails: any = response.Target ? response.Target : response.Transaction;
      this.booking.paymentTypeId = responseDetails.PayPalDetails ? PaymentType.Paypal : PaymentType.CreditCard;
      this.bookingService.createBooking(this.booking)
        .subscribe((bookingResponse: BookingModel) => {
          this.bookingService.setActiveBooking(bookingResponse);
          alert('Payment thru card succeeded.');
          this.router.navigate(['/booked']);
        }, () => {
          alert('An error has occured.');
        });
    } else {
      alert(`An error has occured: ${response.Message}`);
      this.router.navigate(['rates']);
    }
  }

  submitPayment(): void {
    this.isPaying = true;
    this.booking.paymentTypeId = PaymentType.CashOnHand;
    this.booking.transactionId = 'Cash';
    this.bookingService.createBooking(this.booking)
      .subscribe((bookingResponse: BookingModel) => {
        this.bookingService.setActiveBooking(bookingResponse);
        alert('Booking succeeded.');
        this.router.navigate(['/booked']);
      }, (errorResponse: HttpErrorResponse) => {
        if (errorResponse.status === 306) {
          alert('No available driver for your booking.');
        } else {
          alert('An error has occured.');
        }
      })
      .add(() => {
        this.isPaying = false;
      });
  }
}
