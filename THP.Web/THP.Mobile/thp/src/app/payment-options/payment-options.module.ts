import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PaymentOptionsPage } from './payment-options.page';

import { NgxBraintreeModule } from 'ngx-braintree';

const routes: Routes = [
  {
    path: '',
    component: PaymentOptionsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NgxBraintreeModule
  ],
  declarations: [PaymentOptionsPage]
})
export class PaymentOptionsPageModule {}
