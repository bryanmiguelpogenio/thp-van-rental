import { UserModel } from './../_shared/models/user.model';
import { UserService } from './../_shared/services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookingService } from '../_shared/services/booking.service';
import { BookingModel } from '../_shared/models/booking.model';
import { BookingReviewService } from '../_shared/services/booking-review.service';
import { BookingReviewModel } from '../_shared/models/booking-review.model';

@Component({
  selector: 'app-rate-trip',
  templateUrl: './rate-trip.page.html',
  styleUrls: ['./rate-trip.page.scss'],
})
export class RateTripPage implements OnInit {
  pendingBooking: BookingModel = new BookingModel();
  activeUser: UserModel = new UserModel();
  isRating = false;

  constructor(
    private router: Router,
    private bookingService: BookingService,
    private bookingReviewService: BookingReviewService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.initializeData();
  }

  initializeData(): void {
    this.activeUser = this.userService.getActiveAccount();
    this.pendingBooking = this.bookingService.getActiveBooking();
    this.pendingBooking.bookingReview = new BookingReviewModel();
    this.pendingBooking.bookingReview.bookingId = this.pendingBooking.Id;
    this.pendingBooking.bookingReview.userId = this.activeUser.id;
    this.pendingBooking.bookingReview.message = '';
    this.pendingBooking.bookingReview.rating = 0;
  }

  submitRating(): void {
    this.isRating = true;
    this.bookingReviewService.createBookingReview(this.pendingBooking.bookingReview)
      .subscribe(() => {
        alert('Your review has been submitted. Thanks for your cooperation.');
        this.bookingService.setActiveBooking(new BookingModel());
        this.router.navigate(['/home']);
      }, () => {
        alert('An error has occured.');
      })
      .add(() => {
        this.isRating = false;
      });
  }

}
