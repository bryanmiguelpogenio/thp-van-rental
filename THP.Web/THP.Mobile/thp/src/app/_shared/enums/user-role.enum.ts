export enum UserRole {
    Administrator = 1,
    Customer,
    Driver
}
