export enum PaymentType {
    CreditCard = 1,
    Paypal,
    CashOnHand
}
