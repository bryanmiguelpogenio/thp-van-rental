export class UserModel {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    emailAddress: string;
    userName: string;
    roleId: number;
    password: string;
}
