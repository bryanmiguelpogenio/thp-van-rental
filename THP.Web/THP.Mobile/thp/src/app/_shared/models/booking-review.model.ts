export class BookingReviewModel {
    bookingId: number;
    userId: number;
    message: string;
    rating: number;
    id: number;
}
