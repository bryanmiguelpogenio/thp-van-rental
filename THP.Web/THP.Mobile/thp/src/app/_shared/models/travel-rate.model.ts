import { LocationModel } from './location.model';

export class TravelRateModel {
    id?: number;
    pickupPoint?: LocationModel;
    destinationPoint?: LocationModel;
    fixedPrice?: number;
    overtime?: number;
    overnight?: number;
    maxHours?: number;
}
