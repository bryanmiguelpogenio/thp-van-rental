export class UserProfileModel {
    id?: number;
    userId?: number;
    birthdate?: Date;
    cellphoneNo?: string;
    telephoneNo?: string;
    gender?: string;
    address?: string;
    municipality?: string;
    city?: string;
    province?: string;
    region?: string;
}
