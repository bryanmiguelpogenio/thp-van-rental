export class DiscountModel {
    constructor () {
        this.id = 0;
        this.discountAmount = 0;
    }

    id: number;
    couponCode: string;
    discountAmount: number;
    name: string;
    endDate: Date;
    startDate: Date;
}
