export class LocationModel {
    id?: number;
    parentLocationId?: number;
    name?: string;
    description?: string;
}
