import { UserModel } from './user.model';
export class UserCarModel {
    id: number;
    driver: UserModel;
    brand: string;
    model: string;
    color: string;
    plateNumber: string;
    totalKilometer: number;
    location: string;
}
