import { BookingReviewModel } from './booking-review.model';
import { PaymentType } from './../enums/payment-type.enum';
import { TravelRateModel } from './travel-rate.model';
import { UserModel } from './user.model';

export class BookingModel {
    Id: number;
    bookingNo: string;
    driverUser: UserModel;
    travelRate: TravelRateModel;
    paymentTypeId: PaymentType;
    seater: number;
    totalPrice: number;
    currency: string;
    startDateScheduled: Date;
    endDateScheduled: Date;
    dateCompleted: Date;
    status: string;
    userId: number;
    discountId: number;
    transactionId: string;
    bookingReview?: BookingReviewModel;
}
