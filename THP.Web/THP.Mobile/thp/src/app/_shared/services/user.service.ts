import { UserProfileModel } from './../models/user-profile.model';
import { UserModel } from '../models/user.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LoginModel } from '../models/login.model';
import { RoleModel } from '../models/role.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private driverWatcherId: any;
  private isUserLoggedIn = false;
  options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  loginUser(loginAccount: LoginModel): Observable<UserModel> {
    return this.http.post<UserModel>(`${environment.endpoint}user/login`, loginAccount, this.options);
  }

  updateUser(user: UserModel): Observable<void> {
    return this.http.put<void>(`${environment.endpoint}user`, user, this.options);
  }

  externalLoginUser(loginAccount: LoginModel): Observable<UserModel> {
    return this.http.post<UserModel>(`${environment.endpoint}user/externallogin`, loginAccount, this.options);
  }

  registerUser(registerAccount: UserModel): Observable<number> {
    return this.http.post<number>(`${environment.endpoint}user`, registerAccount, this.options);
  }

  getUserProfile(userId: number): Observable<UserProfileModel> {
    return this.http.get<UserProfileModel>(`${environment.endpoint}userdetail/${userId}`);
  }

  createUserProfile(userProfile: UserProfileModel): Observable<number> {
    return this.http.post<number>(`${environment.endpoint}userdetail`, userProfile, this.options);
  }

  updateUserProfile(userProfile: UserProfileModel): Observable<number> {
    return this.http.put<number>(`${environment.endpoint}userdetail`, userProfile, this.options);
  }

  getRoles(): Observable<Array<RoleModel>> {
    return this.http.get<Array<RoleModel>>(`${environment.endpoint}role`);
  }

  setActiveAccount(user: UserModel): void {
    localStorage.setItem('user', JSON.stringify(user));
  }

  getActiveAccount(): UserModel {
    return JSON.parse(localStorage.getItem('user'));
  }

  setIsUserLoggedIn(isUserLoggedIn: boolean): void {
    this.isUserLoggedIn = isUserLoggedIn;
  }

  getIsUserLoggedIn(): boolean {
    return this.isUserLoggedIn;
  }

  setDriverWatcherId(driverWatcherId: any): void {
    this.driverWatcherId = driverWatcherId;
  }

  getDriverWatcherId(): any {
    return this.driverWatcherId;
  }
}
