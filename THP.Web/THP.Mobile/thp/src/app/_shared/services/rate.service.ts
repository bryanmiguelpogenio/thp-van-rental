import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LocationModel } from '../models/location.model';
import { Observable } from 'rxjs';
import { TravelRateModel } from '../models/travel-rate.model';

@Injectable({
  providedIn: 'root'
})
export class RateService {
  travelRates: Array<TravelRateModel> = new Array<TravelRateModel>();
  locations: Array<LocationModel> = new Array<LocationModel>();

  constructor(private http: HttpClient) { }

  getLocations(): Observable<Array<LocationModel>> {
    return this.http.get<Array<LocationModel>>(`${environment.endpoint}location`);
  }

  getRates(): Observable<Array<TravelRateModel>> {
    return this.http.get<Array<TravelRateModel>>(`${environment.endpoint}travelrate`);
  }

  setAllTravelRates(travelRates: Array<TravelRateModel>): void {
    this.travelRates = travelRates;
  }

  getAllTravelRates(): Array<TravelRateModel> {
    return this.travelRates;
  }

  setAllLocations(locations: Array<LocationModel>): void {
    this.locations = locations;
  }

  getAllLocations(): Array<LocationModel> {
    return this.locations;
  }
}
