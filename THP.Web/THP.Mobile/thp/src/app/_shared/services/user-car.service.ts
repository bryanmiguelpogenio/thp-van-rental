import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserCarModel } from '../models/user-car.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserCarService {
  options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private http: HttpClient) { }

  getUserCar(userId: number): Observable<UserCarModel> {
    return this.http.get<UserCarModel>(`${environment.endpoint}usercar/${userId}`);
  }

  updateUserCarLocation(id: number, location: string): Observable<void> {
    return this.http.put<void>(`${environment.endpoint}usercar`, { id: id, location: location }, this.options);
  }
}
