import { BookingModel } from './../models/booking.model';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  private booking: BookingModel = new BookingModel();

  options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  createBooking(booking: BookingModel): Observable<BookingModel> {
    return this.http.post<BookingModel>(`${environment.endpoint}booking`, booking, this.options);
  }

  getBookings(userId: number): Observable<Array<BookingModel>> {
    return this.http.get<Array<BookingModel>>(`${environment.endpoint}booking/${userId}`);
  }

  getDriverBookings(driverId: number): Observable<Array<BookingModel>> {
    return this.http.get<Array<BookingModel>>(`${environment.endpoint}booking/driver/${driverId}`);
  }

  getUnavailableDates(): Observable<Array<string>> {
    return this.http.get<Array<string>>(`${environment.endpoint}booking`);
  }

  updateBooking(booking: BookingModel): Observable<void> {
    return this.http.put<void>(`${environment.endpoint}booking`, booking, this.options);
  }

  setActiveBooking(booking: BookingModel): void {
    this.booking = booking;
  }

  getActiveBooking(): BookingModel {
    return this.booking;
  }
}
