import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BookingReviewModel } from '../models/booking-review.model';

@Injectable({
  providedIn: 'root'
})
export class BookingReviewService {
  options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  createBookingReview(bookingReview: BookingReviewModel): Observable<void> {
    return this.http.post<void>(`${environment.endpoint}bookingreview`, bookingReview, this.options);
  }

  getBookingReviews(userId: number): Observable<Array<BookingReviewModel>> {
    return this.http.get<Array<BookingReviewModel>>(`${environment.endpoint}bookingreview/${userId}`, this.options);
  }
}
