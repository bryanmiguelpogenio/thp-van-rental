import { Injectable } from '@angular/core';
import { DiscountModel } from '../models/discount.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DiscountService {
  private activeDiscount: DiscountModel = new DiscountModel();

  constructor(private http: HttpClient) { }

  getDiscounts(): Observable<Array<DiscountModel>> {
    return this.http.get<Array<DiscountModel>>(`${environment.endpoint}discount`);
  }

  setActiveDiscount(discount: DiscountModel): void {
    this.activeDiscount = discount;
  }

  getActiveDiscount(): DiscountModel {
    return this.activeDiscount;
  }
}
