import { BookingService } from './../_shared/services/booking.service';
import { BookingModel } from './../_shared/models/booking.model';
import { Component, OnInit } from '@angular/core';
import { UserModel } from '../_shared/models/user.model';
import { UserService } from '../_shared/services/user.service';
import { BookingReviewService } from '../_shared/services/booking-review.service';
import { forkJoin } from 'rxjs';
import { BookingReviewModel } from '../_shared/models/booking-review.model';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  readonly BOOKING_STATUS_COMPLETE: string = 'Complete';
  readonly BOOKING_STATUS_CANCELLED = 'Cancelled';
  bookings: Array<BookingModel> = new Array<BookingModel>();
  bookingReviews: Array<BookingReviewModel> = new Array<BookingReviewModel>();
  activeUser: UserModel = new UserModel();
  bookingsWithReview: Array<BookingModel> = new Array<BookingModel>();

  constructor(
    private bookingService: BookingService,
    private bookingReviewService: BookingReviewService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.initializeData();
  }

  initializeData(): void {
    this.activeUser = this.userService.getActiveAccount();
    forkJoin([this.bookingService.getBookings(this.activeUser.id), this.bookingReviewService.getBookingReviews(this.activeUser.id)])
      .subscribe((response: Array<any>) => {
        this.bookings = response[0].filter((x: BookingModel) => x.status === this.BOOKING_STATUS_COMPLETE || x.status === this.BOOKING_STATUS_CANCELLED);
        this.bookingReviews = response[1];
        this.bookings.forEach((x: BookingModel) => {
          x.bookingReview = this.bookingReviews.find((y: BookingReviewModel) => y.bookingId === x.Id);
        });
        this.bookingsWithReview = this.bookings.filter((x: BookingModel) => x.bookingReview || x.status === this.BOOKING_STATUS_CANCELLED);
      }, () => {
        alert('An error has occured.');
      });
  }
}
