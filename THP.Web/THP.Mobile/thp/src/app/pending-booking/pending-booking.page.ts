import { BookingReviewService } from './../_shared/services/booking-review.service';
import { BookingReviewModel } from './../_shared/models/booking-review.model';
import { UserModel } from './../_shared/models/user.model';
import { UserService } from './../_shared/services/user.service';
import { BookingModel } from './../_shared/models/booking.model';
import { BookingService } from './../_shared/services/booking.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-pending-booking',
  templateUrl: './pending-booking.page.html',
  styleUrls: ['./pending-booking.page.scss'],
})
export class PendingBookingPage implements OnInit {
  readonly BOOKING_STATUS_QUEUED: string = 'Queued';
  readonly BOOKING_STATUS_ONGOING: string = 'On Going';
  readonly BOOKING_STATUS_COMPLETE: string = 'Complete';
  bookings: Array<BookingModel> = new Array<BookingModel>();
  bookingReviews: Array<BookingReviewModel> = new Array<BookingReviewModel>();
  pendingBooking: BookingModel;
  activeUser: UserModel = new UserModel();
  bookingsWithoutReview: Array<BookingModel> = new Array<BookingModel>();
  toRateBooking: BookingModel = new BookingModel();

  constructor(
    private router: Router,
    private bookingService: BookingService,
    private bookingReviewService: BookingReviewService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.initializeData();
  }

  initializeData(): void {
    this.activeUser = this.userService.getActiveAccount();
    forkJoin([this.bookingService.getBookings(this.activeUser.id), this.bookingReviewService.getBookingReviews(this.activeUser.id)])
      .subscribe((response: Array<any>) => {
        this.bookings = response[0].filter((x: BookingModel) => x.status === this.BOOKING_STATUS_COMPLETE);
        this.bookingReviews = response[1];
        this.bookings.forEach((x: BookingModel) => {
          x.bookingReview = this.bookingReviews.find((y: BookingReviewModel) => y.bookingId === x.Id);
        });
        this.pendingBooking = response[0].find((x: BookingModel) => x.status === this.BOOKING_STATUS_ONGOING || x.status === this.BOOKING_STATUS_QUEUED);
        this.bookingsWithoutReview = this.bookings.filter((x: BookingModel) => !x.bookingReview);
      }, () => {
        alert('An error has occured.');
      });
  }

  cancelTrip(): void {
    this.bookingService.setActiveBooking(this.pendingBooking);
    this.router.navigate(['/cancel-trip']);
  }

  rateTrip(bookingId: number): void {
    this.toRateBooking = this.bookingsWithoutReview.find((x: BookingModel) => x.Id === bookingId);
    this.bookingService.setActiveBooking(this.toRateBooking);
    this.router.navigate(['/rate-trip']);
  }

}
