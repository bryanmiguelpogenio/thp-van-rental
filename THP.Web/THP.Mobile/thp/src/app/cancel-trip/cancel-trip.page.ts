import { BookingModel } from './../_shared/models/booking.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookingService } from '../_shared/services/booking.service';

@Component({
  selector: 'app-cancel-trip',
  templateUrl: './cancel-trip.page.html',
  styleUrls: ['./cancel-trip.page.scss'],
})
export class CancelTripPage implements OnInit {
  readonly BOOKING_STATUS_CANCELLED = 'Cancelled';
  readonly BOOKING_STATUS_ONGOING: string = 'On Going';
  pendingBooking: BookingModel = new BookingModel();
  isCancelling = false;
  isCheckingLocation = false;

  constructor(
    private router: Router,
    private bookingService: BookingService
  ) { }

  ngOnInit() {
    this.initializeData();
  }

  initializeData(): void {
    this.pendingBooking = this.bookingService.getActiveBooking();
  }

  cancelBooking(): void {
    this.isCancelling = true;
    this.pendingBooking.status = this.BOOKING_STATUS_CANCELLED;
    this.bookingService.updateBooking(this.pendingBooking)
      .subscribe(() => {
        alert('Successfully canceled booking.');
        this.bookingService.setActiveBooking(new BookingModel());
        this.router.navigate(['/home']);
      }, () => {
        alert('an error has occured.');
      })
      .add(() => {
        this.isCancelling = false;
      });
  }

  goToDriversLocation(): void {
    this.router.navigate(['/driver-location']);
  }
}
