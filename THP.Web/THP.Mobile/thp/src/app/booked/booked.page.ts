import { UserCarService } from './../_shared/services/user-car.service';
import { BookingModel } from './../_shared/models/booking.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookingService } from '../_shared/services/booking.service';
import { UserCarModel } from '../_shared/models/user-car.model';

@Component({
  selector: 'app-booked',
  templateUrl: './booked.page.html',
  styleUrls: ['./booked.page.scss'],
})
export class BookedPage implements OnInit {
  readonly BOOKING_STATUS_CANCELLED = 'Cancelled';
  activeBooking: BookingModel = new BookingModel();
  userCarDetail: UserCarModel;

  constructor(
    private router: Router,
    private bookingService: BookingService,
    private userCarService: UserCarService
  ) { }

  ngOnInit() {
    this.initializeData();
  }

  initializeData(): void {
    this.activeBooking = this.bookingService.getActiveBooking();
    this.userCarService.getUserCar(this.activeBooking.driverUser.id)
      .subscribe((response: UserCarModel) => {
        this.userCarDetail = response;
      });
  }

  goToHome(): void {
    this.router.navigate(['/home']);
  }

  cancelBooking(): void {
    this.activeBooking.status = this.BOOKING_STATUS_CANCELLED;
    this.bookingService.updateBooking(this.activeBooking)
      .subscribe(() => {
        alert('Successfully canceled booking.');
        this.bookingService.setActiveBooking(new BookingModel());
        this.router.navigate(['/home']);
      }, () => {
        alert('an error has occured.');
      });
  }
}
