import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { UserModel } from './../_shared/models/user.model';
import { LoginModel } from './../_shared/models/login.model';
import { UserService } from './../_shared/services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserRole } from '../_shared/enums/user-role.enum';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  account: LoginModel = new LoginModel();
  facebookGoogleAccount: UserModel = new UserModel();
  isLoggingIn = false;
  isGoogleLoggingIn = false;
  isFacebookLoggingIn = false;

  constructor(
    private router: Router,
    private userService: UserService,
    private facebook: Facebook,
    private googlePlus: GooglePlus) { }

  ngOnInit() { }

  login(): void {
    this.isLoggingIn = true;
    this.userService.loginUser(this.account).subscribe((response: UserModel) => {
      if (response) {
        if (response.roleId === UserRole.Customer) {
          this.userService.setActiveAccount(response);
          this.router.navigate(['/home']);
        } else if (response.roleId === UserRole.Driver) {
          this.userService.setActiveAccount(response);
          this.router.navigate(['/home-driver']);
        } else {
          alert('This application is for Customer and Driver only.');
        }
      } else {
        alert('Incorrect Username or Password.');
      }
    }).add(() => {
      this.isLoggingIn = false;
    });
  }

  facebookLogin(): void {
    this.isFacebookLoggingIn = true;
    this.facebook.login(['public_profile', 'user_friends', 'email'])
      .then(() => {
        this.facebook.api('me?fields=id,email,first_name,middle_name,last_name', [])
          .then(result => {
            this.facebookGoogleAccount.emailAddress = result['email'];
            this.facebookGoogleAccount.firstName = result['first_name'];
            this.facebookGoogleAccount.middleName = result['middle_name'];
            this.facebookGoogleAccount.lastName = result['last_name'];
            this.facebookGoogleAccount.userName = result['email'];
            this.facebookGoogleAccount.roleId = 2;
            this.userService.externalLoginUser(this.facebookGoogleAccount)
              .subscribe((response: UserModel) => {
                if (response.roleId === UserRole.Customer) {
                  this.userService.setActiveAccount(response);
                  this.router.navigate(['/home']);
                } else {
                  alert('This application is for Customer only.');
                }
              }, () => {
                alert('Error creating facebook account');
              });
          });
      })
      .catch(error => {
        alert('Error logging into Facebook');
      }).finally(() => {
        this.isFacebookLoggingIn = false;
      });

    this.facebook.logEvent(this.facebook.EVENTS.EVENT_NAME_ADDED_TO_CART);
  }

  googleLogin(): void {
    this.isGoogleLoggingIn = true;
    this.googlePlus.login({})
      .then(result => {
        this.facebookGoogleAccount.emailAddress = result['email'];
        this.facebookGoogleAccount.firstName = result['givenName'];
        this.facebookGoogleAccount.middleName = result['middleName'];
        this.facebookGoogleAccount.lastName = result['familyName'];
        this.facebookGoogleAccount.userName = result['email'];
        this.facebookGoogleAccount.roleId = 2;
        this.userService.externalLoginUser(this.facebookGoogleAccount).subscribe((response: UserModel) => {
          if (response.roleId === UserRole.Customer) {
            this.userService.setActiveAccount(response);
            this.router.navigate(['/home']);
          } else {
            alert('This application is for Customer only.');
          }
        }, () => {
          alert('Error creating google account');
        });
      })
      .catch(err => {
        alert('Error logging into Google');
      })
      .finally(() => {
        this.isGoogleLoggingIn = false;
      });
  }
}
