import { UserModel } from './../_shared/models/user.model';
import { UserProfileModel } from './../_shared/models/user-profile.model';
import { UserService } from './../_shared/services/user.service';
import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Router } from '@angular/router';
import { UserRole } from '../_shared/enums/user-role.enum';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  userProfile: UserProfileModel = new UserProfileModel();
  user: UserModel = new UserModel();
  userRole = UserRole;

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initializeData();
  }

  initializeData(): void {
    this.user = this.userService.getActiveAccount();
    this.userService.getUserProfile(this.user.id)
      .subscribe((response: UserProfileModel) => {
        if (response) {
          this.userProfile = response;
        }
      }, () => {
          alert('An error has occured.');
        });
  }

  updateUserProfile(): void {
    this.userProfile.userId = this.user.id;
    if (this.userProfile.id) {
      forkJoin([this.userService.updateUserProfile(this.userProfile), this.userService.updateUser(this.user)])
        .subscribe(() => {
          alert('Your profile has been updated.');
        }, () => {
          alert('An error has occured.');
        });
    } else {
      forkJoin([this.userService.createUserProfile(this.userProfile), this.userService.updateUser(this.user)])
        .subscribe(response => {
          this.userProfile.id = response[0];
          alert('Your profile has been updated.');
        }, () => {
          alert('An error has occured.');
        });
    }
  }

  goToPendingBooking(): void {
    this.router.navigate(['/pending-booking']);
  }
}
