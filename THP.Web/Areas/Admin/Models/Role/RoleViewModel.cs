﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace THP.Web.Areas.Admin.Models.Role
{
    public class RoleViewModel
    {
        [Display(Name = "Name")]
        public string Name { get; set; }

        public List<Role> Roles { get; set; }
    }
}