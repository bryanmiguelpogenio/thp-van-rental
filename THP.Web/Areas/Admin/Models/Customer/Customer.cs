﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using EntityRole = THP.Web.Models.Entities.Data.Role; 

namespace THP.Web.Areas.Admin.Models.Customer
{
    public class Customer
    {
        public Customer()
        {
            Roles = new List<SelectListItem>();
            Genders = new List<SelectListItem> {
                new SelectListItem { Text = "Male", Value = "Male"},
                new SelectListItem { Text = "Female", Value = "Female"}
            };
        }

        public int CustomerId { get; set; }

        [Required(ErrorMessage = "Firstname is required")]
        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        [Display(Name = "Firstname")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Middlename is required")]
        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        [Display(Name = "Middlename")]
        public string Middlename { get; set; }

        [Required(ErrorMessage = "Lastname is required")]
        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        [Display(Name = "Lastname")]
        public string Lastname { get; set; }

        [Required(ErrorMessage = "Email Address is required")]
        [Remote("CheckExistingEmailAddress","Customer", AdditionalFields = "CustomerId", 
            ErrorMessage = "Email Address is already existing", HttpMethod = "POST")]
        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        [Display(Name = "Username")]
        public string Username { get; set; }
        
        [Display(Name = "Password")]
        public string Password { get; set; }
        
        [Display(Name = "Role")]
        public int RoleID { get; set; }
        public List<SelectListItem> Roles { get; set; }
        public EntityRole Role { get; set; }

        public int CustomerDetailId { get; set; }

        [Display(Name = "Birthdate")]
        public DateTime? Birthdate { get; set; }

        [StringLength(12, ErrorMessage = "Maximum characters accepted is twelve(12)")]
        [Display(Name = "Cellphone #")]
        public string CellphoneNo { get; set; }

        [StringLength(7, ErrorMessage = "Maximum characters accepted is seven(7)")]
        [Display(Name = "Telephone #")]
        public string TelephoneNo { get; set; }

        [StringLength(6, ErrorMessage = "Maximum characters accepted is gender(6)")]
        [Display(Name = "Gender")]
        public string Gender { get; set; }
        public List<SelectListItem> Genders { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        [Display(Name ="Municipality")]
        public string Municipality { get; set; }

        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        [Display(Name = "City")]
        public string City { get; set; }

        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        [Display(Name = "Province")]
        public string Province { get; set; }

        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        [Display(Name = "Region")]
        public string Region { get; set; }

        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Date Updated")]
        public DateTime DateUpdated { get; set; }
        [Display(Name = "Updated By")]
        public string UpdatedBy { get; set; }

        public string Fullname
        {
            get { return $"{Firstname} {Middlename} {Lastname}".Trim(); }
        }
    }
}