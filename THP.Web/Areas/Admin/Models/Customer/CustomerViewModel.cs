﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace THP.Web.Areas.Admin.Models.Customer
{
    public class CustomerViewModel
    {
        [Display(Name = "Customer Name")]
        public string Fullname { get; set; }
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }
        [Display(Name = "Date Created")]
        public string DateCreated { get; set; }
        [Display(Name = "Date Updated")]
        public string DateUpdated { get; set; }

        public List<Customer> Customers { get; set; }
    }
}