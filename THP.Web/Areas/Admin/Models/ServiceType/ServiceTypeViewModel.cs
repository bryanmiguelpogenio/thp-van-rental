﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace THP.Web.Areas.Admin.Models.ServiceType
{
    public class ServiceTypeViewModel
    {
        [Display(Name = "Name")]
        public string Name { get; set; }

        public List<ServiceType> ServiceTypes { get; set; }
    }
}