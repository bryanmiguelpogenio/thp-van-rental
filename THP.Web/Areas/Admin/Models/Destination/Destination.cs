﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Areas.Admin.Models.Destination
{
    public class Destination
    {
        public Destination()
        {
            Locations = new List<SelectListItem>();
        }

        public int LocationID { get; set; }

        [Display(Name = "Location")]
        [Required(ErrorMessage = "Name is Required")]
        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        public string Name { get; set; }

        [Display(Name = "Province/State")]
        public string ProvinceState { get; set; }
        [Display(Name = "Province/State")]
        public int ParentLocationID { get; set; }
        public List<SelectListItem> Locations { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Date Updated")]
        public DateTime DateUpdated { get; set; }
        [Display(Name = "Updated By")]
        public string UpdatedBy { get; set; }
    }
}