﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Areas.Admin.Models.Destination
{
    public class DestinationViewModel
    {
        #region Ctor

        public DestinationViewModel()
        {
            lstLocation = new List<SelectListItem>();
        }

        #endregion

        [Display(Name = "Location")]
        public string Name { get; set; }

        [Display(Name = "Province")]
        public int ProvinceId { get; set; }
        public List<SelectListItem> lstLocation { get; set; }

        public List<Destination> Destinations { get; set; }
    }
}