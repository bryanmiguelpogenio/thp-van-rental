﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Areas.Admin.Models.Car
{
    public class Car
    {
        public Car()
        {
            Users = new List<SelectListItem>();
        }

        public int CarId { get; set; }

        [Display(Name = "Driver Name")]
        [Required(ErrorMessage = "Driver is required")]
        public int UserId { get; set; }
        public List<SelectListItem> Users { get; set; }

        [Display(Name = "Driver Name")]
        public string DriverName { get; set; }

        [Display(Name = "Brand")]
        [Required(ErrorMessage = "Brand is required")]
        [StringLength(100, ErrorMessage = "Maximum characters accepted is one hundred(100)")]
        public string Brand { get; set; }
        [Display(Name = "Model")]
        [Required(ErrorMessage = "Model is required")]
        [StringLength(100, ErrorMessage = "Maximum characters accepted is one hundred(100)")]
        public string CarModel { get; set; }
        [Display(Name = "Color")]
        [Required(ErrorMessage = "Color is required")]
        [StringLength(100, ErrorMessage = "Maximum characters accepted is one hundred(100)")]
        public string Color { get; set; }
        [Display(Name = "Plate Number")]
        [Required(ErrorMessage = "Plate Number is required")]
        [StringLength(10, ErrorMessage = "Maximum characters accepted is ten(10)")]
        public string PlateNumber { get; set; }
        [Display(Name = "Total Kilometer")]
        public decimal? TotalKilometer { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Date Updated")]
        public DateTime DateUpdated { get; set; }
        [Display(Name = "Updated By")]
        public string UpdatedBy { get; set; }
    }
}