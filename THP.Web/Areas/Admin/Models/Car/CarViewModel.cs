﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Areas.Admin.Models.Car
{
    public class CarViewModel
    {
        public CarViewModel()
        {
            Drivers = new List<SelectListItem>();
        }

        [Display(Name = "Driver")]
        public int Driver { get; set; }
        public List<SelectListItem> Drivers { get; set; }

        public List<Car> Cars { get; set; }
    }
}