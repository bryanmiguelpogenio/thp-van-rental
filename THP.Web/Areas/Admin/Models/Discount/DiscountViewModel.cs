﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace THP.Web.Areas.Admin.Models.Discount
{
    public class DiscountViewModel
    {
        public DiscountViewModel()
        {
            Discounts = new List<Discount>();
        }

        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        public List<Discount> Discounts { get; set; }
    }
}