﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Areas.Admin.Models.Discount
{
    public class Discount
    {
        public int DiscountId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        public string Name { get; set; }
        [Display(Name = "Discount Amount")]        
        public decimal DiscountAmount { get; set; }
        [Display(Name = "Coupon Code Required")]
        public bool HasCouponCode { get; set; }
        [Display(Name = "Coupon Code")]
        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        [Remote("IsCouponCodeExisting", "Discount", HttpMethod = "POST", AdditionalFields = "DiscountId", 
            ErrorMessage = "Coupon Code already exists")]
        public string CouponCode { get; set; }
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Date Updated")]
        public DateTime DateUpdated { get; set; }
        [Display(Name = "Updated By")]
        public string UpdatedBy { get; set; }
    }
}