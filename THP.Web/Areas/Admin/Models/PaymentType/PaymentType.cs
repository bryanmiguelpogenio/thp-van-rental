﻿using System;
using System.ComponentModel.DataAnnotations;

namespace THP.Web.Areas.Admin.Models.PaymentType
{
    public class PaymentType
    {
        public int PaymentTypeId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required")]
        [StringLength(50)]
        public string Name { get; set; }
        [Display(Name = "Description")]
        [StringLength(255, ErrorMessage = "Maximum characters accepted is two hundred fifty five(255)")]
        public string Description { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Date Updated")]
        public DateTime DateUpdated { get; set; }
        [Display(Name = "Updated By")]
        public string UpdatedBy { get; set; }
    }
}