﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace THP.Web.Areas.Admin.Models.PaymentType
{
    public class PaymentTypeViewModel
    {
        [Display(Name = "Name")]
        public string Name { get; set; }

        public List<PaymentType> PaymentTypes { get; set; }
    }
}