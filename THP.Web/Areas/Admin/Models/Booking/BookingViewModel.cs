﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using THP.Web.Models;

namespace THP.Web.Areas.Admin.Models.Booking
{
    public class BookingViewModel
    {
        public BookingViewModel()
        {
            Statuses = new List<SelectListItem>();

            Statuses.Add(new SelectListItem
            {
                Text = Constant.BOOKING_STATUS_ONGOING,
                Value = Constant.BOOKING_STATUS_ONGOING
            });
            Statuses.Add(new SelectListItem
            {
                Text = Constant.BOOKING_STATUS_QUEUED,
                Value = Constant.BOOKING_STATUS_QUEUED
            });
            Statuses.Add(new SelectListItem
            {
                Text = Constant.BOOKING_STATUS_COMPLETE,
                Value = Constant.BOOKING_STATUS_COMPLETE
            });
            Statuses.Add(new SelectListItem
            {
                Text = Constant.BOOKING_STATUS_CANCELLED,
                Value = Constant.BOOKING_STATUS_CANCELLED
            });
        }

        [Display(Name = "Booking #")]
        public string BookingNo { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
        public List<SelectListItem> Statuses { get; set; }
        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
        public List<Booking> Bookings { get; set; }
    }
}