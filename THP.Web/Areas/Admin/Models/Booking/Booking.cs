﻿using System;
using System.ComponentModel.DataAnnotations;

namespace THP.Web.Areas.Admin.Models.Booking
{
    public class Booking
    {
        [Display(Name = "Booking ID")]
        public int BookingID { get; set; }
        [Display(Name = "Booking #")]
        public string BookingNo { get; set; }
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
        [Display(Name = "Driver Name")]
        public string DriverName { get; set; }
        public int TravelRateId { get; set; }
        public TravelRate.TravelRate TravelRate { get; set; }

        [Display(Name = "Payment Type")]
        public string PaymentType { get; set; }
        [Display(Name = "Seater")]
        public int Seater { get; set; }
        [Display(Name = "Total Price")]
        public string TotalPrice { get; set; }
        [Display(Name = "Start Date Scheduled")]
        public DateTime StartDateScheduled { get; set; }
        [Display(Name = "End Date Scheduled")]
        public DateTime EndDateScheduled { get; set; }
        [Display(Name = "Date Completed")]
        public DateTime? DateCompleted { get; set; }
        [Display(Name = "Booking Status")]
        public string Status { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Date Updated")]
        public DateTime DateUpdated { get; set; }
        [Display(Name = "Updated By")]
        public string UpdatedBy { get; set; }

        public BookingReview BookingReview { get; set; }
        [Display(Name = "Discount Code")]
        public string DiscountCode { get; set; }
    }
}