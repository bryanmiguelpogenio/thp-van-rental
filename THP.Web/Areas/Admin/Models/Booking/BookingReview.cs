﻿namespace THP.Web.Areas.Admin.Models.Booking
{
    public class BookingReview
    {
        public int BookingReviewID { get; set; }
        public int Rating { get; set; }
        public string Message { get; set; }
    }
}