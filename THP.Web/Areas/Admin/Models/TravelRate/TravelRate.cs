﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Areas.Admin.Models.TravelRate
{
    public class TravelRate
    {
        public TravelRate()
        {
            PickupPoints = new List<SelectListItem>();
            DestinationPoints = new List<SelectListItem>();
        }

        public int TravelRateId { get; set; }

        [Display(Name = "From")]
        public string DestinationFrom { get; set; }
        [Display(Name = "From")]
        [Required(ErrorMessage = "PickupPoint is required")]
        public int PickupPointID { get; set; }
        public List<SelectListItem> PickupPoints { get; set; }

        [Display(Name = "To")]
        public string DestinationTo { get; set; }
        [Display(Name = "To")]
        [Required(ErrorMessage = "DestinationPoint is required")]
        public int DestinationPointID { get; set; }
        public List<SelectListItem> DestinationPoints { get; set; }

        [Display(Name = "Fixed Price")]
        public decimal? FixedPrice { get; set; }
        [Display(Name = "Overnight")]
        public decimal? Overnight { get; set; }
        [Display(Name = "Overtime")]
        public decimal? Overtime { get; set; }
        [Display(Name = "Max Hours")]
        public int MaxHours { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Date Updated")]
        public DateTime DateUpdated { get; set; }
        [Display(Name = "Updated By")]
        public string UpdatedBy { get; set; }
    }
}