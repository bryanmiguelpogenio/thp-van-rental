﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Areas.Admin.Models.TravelRate
{
    public class TravelRateViewModel
    {
        public TravelRateViewModel()
        {
            DestinationFrom = new List<SelectListItem>();
            DestinationTo = new List<SelectListItem>();
        }

        [Display(Name = "From")]
        public int From { get; set; }
        public List<SelectListItem> DestinationFrom { get; set; }

        [Display(Name = "To")]
        public int To { get; set; }
        public List<SelectListItem> DestinationTo { get; set; }

        public List<TravelRate> TravelRates { get; set; }
    }
}