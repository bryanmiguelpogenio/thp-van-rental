﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace THP.Web.Areas.Admin.Models.TopicPage
{
    public class TopicPageViewModel
    {
        [Display(Name = "Name")]
        public string Name { get; set; }

        public List<Topic> Topics { get; set; }
    }
}