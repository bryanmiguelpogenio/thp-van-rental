﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Areas.Admin.Models.TopicPage
{
    public class Topic
    {
        public int TopicPageID { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, ErrorMessage = "Maximum characters accepted is fifty(50)")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Url is required")]
        [StringLength(255, ErrorMessage = "Maximum characters accepted is two-hunder fifty five(255)")]
        [Remote("CheckExistingUrlSlug", "TopicPage", AdditionalFields = "TopicPageID",
            ErrorMessage = "Url is already existing", HttpMethod = "POST")]
        [Display(Name = "Url")]
        public string UrlSlug { get; set; }

        [Display(Name = "Html Source")]
        [AllowHtml]
        public string Html { get; set; }

        [Display(Name = "Display")]
        public bool Display { get; set; }

        [Display(Name = "Display Order")]
        public int DisplayOrder { get; set; }

        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Date Updated")]
        public DateTime DateUpdated { get; set; }
        [Display(Name = "Updated By")]
        public string UpdatedBy { get; set; }
    }
}