﻿using System;
using System.Linq;
using System.Web.Mvc;
using THP.Web.Areas.Admin.Models.Role;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Areas.Admin.Controllers
{
    public class RoleController : Controller
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor
        public RoleController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region Methods

        [CustomAuthorize(Roles = "Admin")]
        // GET: Admin/Role
        public ActionResult Index()
        {
            RoleViewModel model = new RoleViewModel();
            model.Roles = _unitOfWork.Role.GetRoles(isDeleted: false).ToList();
            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Index(RoleViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.Roles = _unitOfWork.Role
                        .GetRoles(name: model.Name, isDeleted: false)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Add()
        {
            return View(new Role());
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Add(Role model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.Role.AddRole(model);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { @id = model.RoleId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.Role.GetRole(id.Value);

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(Role model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.Role.UpdateRole(model);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { id = model.RoleId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.Role.Get(id.Value);
            model.Deleted = true;

            _unitOfWork.Role.Update(model);
            _unitOfWork.SaveChanges();

            return RedirectToAction("Index");

        }

        #endregion
    }
}