﻿using System;
using System.Linq;
using System.Web.Mvc;
using THP.Web.Areas.Admin.Models.TravelRate;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Areas.Admin.Controllers
{
    public class TravelRateController : Controller
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public TravelRateController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region Methods

        // GET: Admin/TravelRate
        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Index()
        {
            TravelRateViewModel model = new TravelRateViewModel();

            model.TravelRates = _unitOfWork.TravelRate
                        .GetTravelRates(isDeleted: false)
                        .ToList();
            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Index(TravelRateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.TravelRates = _unitOfWork.TravelRate
                        .GetTravelRates(fromId: model.From, toId: model.To, isDeleted: false)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Add()
        {
            var model = new TravelRate();
            _unitOfWork.Location
                .Find(m => m.ID == 1)
                .ToList()
                .ForEach(m => model.PickupPoints.Add(
                    new SelectListItem
                    {
                        Text = m.Address,
                        Value = m.ID.ToString()
                    }));

            _unitOfWork.Location
                .Find(m => m.ParentLocationID != 0)
                .ToList()
                .ForEach(m => model.DestinationPoints.Add(
                    new SelectListItem
                    {
                        Text = m.Address,
                        Value = m.ID.ToString()
                    }));

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Add(TravelRate model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.TravelRate.AddTravelRate(model);
                    _unitOfWork.SaveChanges();
                }

                _unitOfWork.Location
                    .Find(m => m.ID == 1)
                    .ToList()
                    .ForEach(m => model.PickupPoints.Add(
                        new SelectListItem
                        {
                            Text = m.Address,
                            Value = m.ID.ToString()
                        }));

                _unitOfWork.Location
                    .Find(m => m.ParentLocationID != 0)
                    .ToList()
                    .ForEach(m => model.DestinationPoints.Add(
                        new SelectListItem
                        {
                            Text = m.Address,
                            Value = m.ID.ToString()
                        }));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { @id = model.TravelRateId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.TravelRate.GetTravelRate(id.Value);
            _unitOfWork.Location
                .Find(m => m.ID == 1)
                .ToList()
                .ForEach(m => model.PickupPoints.Add(
                    new SelectListItem
                    {
                        Text = m.Address,
                        Value = m.ID.ToString()
                    }));

            _unitOfWork.Location
                .Find(m => m.ParentLocationID != 0)
                .ToList()
                .ForEach(m => model.DestinationPoints.Add(
                    new SelectListItem
                    {
                        Text = m.Address,
                        Value = m.ID.ToString()
                    }));

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(TravelRate model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.TravelRate.UpdateTravelRate(model);
                    _unitOfWork.SaveChanges();
                }

                _unitOfWork.Location
                    .Find(m => m.ID == 1)
                    .ToList()
                    .ForEach(m => model.PickupPoints.Add(
                        new SelectListItem
                        {
                            Text = m.Address,
                            Value = m.ID.ToString()
                        }));

                _unitOfWork.Location
                    .Find(m => m.ParentLocationID != 0)
                    .ToList()
                    .ForEach(m => model.DestinationPoints.Add(
                        new SelectListItem
                        {
                            Text = m.Address,
                            Value = m.ID.ToString()
                        }));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { id = model.TravelRateId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.TravelRate.Get(id.Value);
            model.Deleted = true;

            _unitOfWork.TravelRate.Update(model);
            _unitOfWork.SaveChanges();

            return RedirectToAction("Index");

        }

        #endregion

    }
}