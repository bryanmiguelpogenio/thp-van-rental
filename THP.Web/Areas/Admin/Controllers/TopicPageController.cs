﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using THP.Web.Areas.Admin.Models.TopicPage;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Areas.Admin.Controllers
{
    public class TopicPageController : Controller
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public TopicPageController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region Utilities

        [HttpPost]
        public JsonResult CheckExistingUrlSlug(string UrlSlug, int TopicPageID)
        {
            var status = true;
            var topicPages = _unitOfWork.TopicPage.Find(m => m.UrlSlug == UrlSlug & m.ID != TopicPageID);

            if (topicPages == null)
            {
                // Url is available to use
                status = true;
            }
            else
            {
                // Url already registered
                status = false;
            }

            return Json(status);
        }

        #endregion

        #region Methods

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var model = new TopicPageViewModel();
            model.Topics = _unitOfWork.TopicPage.GetTopicPages(isDeleted: false)
                .OrderBy(m => m.DisplayOrder)
                .ToList();
            return View(model);
        }

        [HttpPost]
        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Index(TopicPageViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.Topics = _unitOfWork.TopicPage
                        .GetTopicPages(name: model.Name, isDeleted: false)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Add()
        {
            return View(new Topic());
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Add(Topic model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.TopicPage.AddTopic(model);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { @id = model.TopicPageID });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.TopicPage.GetTopic(id.Value);

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(Topic model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.TopicPage.UpdateTopic(model);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { id = model.TopicPageID });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.TopicPage.Get(id.Value);
            model.Deleted = true;

            _unitOfWork.TopicPage.Update(model);
            _unitOfWork.SaveChanges();

            return RedirectToAction("Index");
        }

        #endregion
    }
}