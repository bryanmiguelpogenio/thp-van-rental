﻿using System;
using System.Linq;
using System.Web.Mvc;
using THP.Web.Areas.Admin.Models.ServiceType;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Areas.Admin.Controllers
{
    public class ServiceTypeController : Controller
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public ServiceTypeController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region Methods

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Index()
        {
            ServiceTypeViewModel model = new ServiceTypeViewModel();
            model.ServiceTypes = _unitOfWork.ServiceType.GetServiceType(isDeleted: false).ToList();
            return View(model);
        }


        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Index(ServiceTypeViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.ServiceTypes = _unitOfWork.ServiceType
                        .GetServiceType(name: model.Name, isDeleted: false)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Add()
        {
            return View(new ServiceType());
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Add(ServiceType model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.ServiceType.AddServiceType(model);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { @id = model.ServiceTypeId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.ServiceType.GetServiceType(id.Value);

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(ServiceType model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.ServiceType.UpdateServiceType(model);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { id = model.ServiceTypeId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.ServiceType.Get(id.Value);
            model.Deleted = true;

            _unitOfWork.ServiceType.Update(model);
            _unitOfWork.SaveChanges();

            return RedirectToAction("Index");

        }
        #endregion
    }
}