﻿using System;
using System.Linq;
using System.Web.Mvc;
using THP.Web.Areas.Admin.Models.Customer;
using THP.Web.Models;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Areas.Admin.Controllers
{
    public class CustomerController : Controller
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public CustomerController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region Utilities

        [HttpPost]
        public JsonResult CheckExistingEmailAddress(string EmailAddress, int CustomerId)
        {
            var status = true;
            var user = _unitOfWork.User.Find(m => m.EmailAddress == EmailAddress & m.ID != CustomerId);

            if (user == null)
            {
                // Email Address is available to use
                status = true;
            }
            else
            {
                // Email Address already registered
                status = false;
            }

            return Json(status);
        }

        #endregion

        #region Methods

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Index()
        {
            CustomerViewModel model = new CustomerViewModel();
            model.Customers = _unitOfWork.User.GetCustomers(isDeleted: false).ToList();

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Index(CustomerViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.Customers = _unitOfWork.User
                        .GetCustomers(fullName: model.Fullname, 
                        emailAddress: model.EmailAddress,
                        isDeleted: false)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Add()
        {
            var model = new Customer();
            model.Roles = _unitOfWork.Role.Find(m => m.Deleted == false)
                .Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.ID.ToString()
                }).ToList();

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Add(Customer model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.User.AddCustomer(model);
                    _unitOfWork.SaveChanges();
                }

                model.Roles = _unitOfWork.Role.Find(m => m.Deleted == false)
                    .Select(m => new SelectListItem
                    {
                        Text = m.Name,
                        Value = m.ID.ToString()
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { @id = model.CustomerId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            Customer model = _unitOfWork.User.GetCustomer(id.Value);
            model.Roles = _unitOfWork.Role.Find(m => m.Deleted == false)
                .Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.ID.ToString()
                }).ToList();


            return View(model);

        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(Customer model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.User.UpdateCustomer(model);
                    _unitOfWork.SaveChanges();
                }

                model.Roles = _unitOfWork.Role.Find(m => m.Deleted == false)
                    .Select(m => new SelectListItem
                    {
                        Text = m.Name,
                        Value = m.ID.ToString()
                    }).ToList();

            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { id = model.CustomerId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.User.Get(id.Value);
            model.Deleted = true;

            _unitOfWork.User.Update(model);
            _unitOfWork.SaveChanges();

            return RedirectToAction("Index");

        }
        #endregion

    }
}