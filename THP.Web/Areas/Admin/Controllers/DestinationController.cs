﻿using System;
using System.Linq;
using System.Web.Mvc;
using THP.Web.Areas.Admin.Models.Destination;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Areas.Admin.Controllers
{
    public class DestinationController : Controller
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public DestinationController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region Methods
        
        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Index()
        {
            DestinationViewModel model = new DestinationViewModel();
            model.Destinations = _unitOfWork.Location.GetDestinations(isDeleted: false).ToList();

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Index(DestinationViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.Destinations = _unitOfWork.Location
                        .GetDestinations(name: model.Name, provinceId: model.ProvinceId, isDeleted: false)
                        .ToList();
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Add()
        {
            var model = new Destination();
            model.Locations = _unitOfWork.Location.Find(m => m.Deleted == false & m.ParentLocationID == 0)
                .Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.ID.ToString()
                }).ToList();


            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Add(Destination model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.Location.AddDestination(model);
                    _unitOfWork.SaveChanges();
                }

                model.Locations = _unitOfWork.Location.Find(m => m.Deleted == false & m.ParentLocationID == 0)
                    .Select(m => new SelectListItem
                    {
                        Text = m.Name,
                        Value = m.ID.ToString()
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", "Destination", routeValues: new { @id = model.LocationID });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            Destination model = _unitOfWork.Location.GetDestination(id.Value);
            model.Locations = _unitOfWork.Location.Find(m => m.Deleted == false & m.ParentLocationID == 0)
                .Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.ID.ToString()
                }).ToList();


            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(Destination model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.Location.UpdateDestination(model);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { id = model.LocationID });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.Location.Get(id.Value);
            model.Deleted = true;

            _unitOfWork.Location.Update(model);
            _unitOfWork.SaveChanges();

            return RedirectToAction("Index");

        }
        #endregion
    }
}