﻿using System;
using System.Linq;
using System.Web.Mvc;
using THP.Web.Areas.Admin.Models.PaymentType;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Areas.Admin.Controllers
{
    public class PaymentTypeController : Controller
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public PaymentTypeController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region Methods

        [CustomAuthorize(Roles = "Admin")]
        // GET: Admin/PaymentType
        public ActionResult Index()
        {
            PaymentTypeViewModel model = new PaymentTypeViewModel();
            model.PaymentTypes = _unitOfWork.PaymentType.GetPaymentTypes(isDeleted: false).ToList();
            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Index(PaymentTypeViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.PaymentTypes = _unitOfWork.PaymentType
                        .GetPaymentTypes(name: model.Name, isDeleted: false)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Add()
        {
            return View(new PaymentType());
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Add(PaymentType model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.PaymentType.AddPaymentType(model);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { @id = model.PaymentTypeId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            PaymentType model = _unitOfWork.PaymentType.GetPaymentType(id.Value);

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(PaymentType model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.PaymentType.UpdatePaymentType(model);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { id = model.PaymentTypeId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.PaymentType.Get(id.Value);
            model.Deleted = true;

            _unitOfWork.PaymentType.Update(model);
            _unitOfWork.SaveChanges();

            return RedirectToAction("Index");

        }

        #endregion
    }
}