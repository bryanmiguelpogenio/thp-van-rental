﻿using System;
using System.Linq;
using System.Web.Mvc;
using THP.Web.Areas.Admin.Models.Discount;
using THP.Web.Models;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Areas.Admin.Controllers
{
    public class DiscountController : Controller
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public DiscountController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region Utilities

        [HttpPost]
        public JsonResult IsCouponCodeExisting(string CouponCode, int DiscountId)
        {
            var status = true;
            var user = _unitOfWork.Discount.Find(m => m.CouponCode == CouponCode & m.ID != DiscountId);

            if (user == null)
            {
                // Coupon Code is available to use
                status = true;
            }
            else
            {
                // Coupon Code already registered
                status = false;
            }

            return Json(status);
        }

        #endregion

        #region Methods

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var model = new DiscountViewModel();
            model.Discounts = _unitOfWork.Discount.GetDiscounts(isDeleted: false).ToList();
            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Index(DiscountViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.Discounts = _unitOfWork.Discount.GetDiscounts(name: model.Name,
                        startDate: model.StartDate,
                        endDate: model.EndDate,
                        isDeleted: false)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Add()
        {
            return View(new Discount());
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Add(Discount model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.Discount.AddDiscount(model);
                    _unitOfWork.SaveChanges();
                }

                var errors = ModelState.Values.SelectMany(m => m.Errors).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { @id = model.DiscountId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.Discount.GetDiscount(id.Value);

            return View(model);
        }

        [CustomAuthorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(Discount model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.Discount.UpdateDiscount(model);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { id = model.DiscountId });
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.Discount.Get(id.Value);
            model.Deleted = true;

            _unitOfWork.Discount.Update(model);
            _unitOfWork.SaveChanges();

            return RedirectToAction("Index");

        }

        #endregion

    }
}