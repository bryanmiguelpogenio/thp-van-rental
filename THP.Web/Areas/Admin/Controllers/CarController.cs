﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using THP.Web.Areas.Admin.Models.Car;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Areas.Admin.Controllers
{
    public class CarController : Controller
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public CarController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region Methods

        [CustomAuthorize]
        // GET: Admin/Car
        public ActionResult Index()
        {
            var model = new CarViewModel();
            model.Cars = _unitOfWork.UserCar.GetCars(isDeleted: false);
            model.Drivers = _unitOfWork.User.GetDrivers(isDeleted: false)
                .Select(m => new SelectListItem
                {
                    Text = m.FullName,
                    Value = m.ID.ToString()
                })
                .ToList();
            return View(model);
        }

        [CustomAuthorize]
        [HttpPost]
        public ActionResult Index(CarViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Driver == 0)
                    {
                        model.Cars = _unitOfWork.UserCar.GetCars(isDeleted: false);
                    }
                    else
                    {
                        model.Cars = _unitOfWork.UserCar.GetCars(userId: model.Driver, isDeleted: false);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            model.Drivers = _unitOfWork.User.GetDrivers(isDeleted: false)
                .Select(m => new SelectListItem
                {
                    Text = m.FullName,
                    Value = m.ID.ToString()
                })
                .ToList();

            return View(model);
        }

        [CustomAuthorize]
        public ActionResult Add()
        {
            var model = new Car();
            model.Users = _unitOfWork.User.GetDrivers(isDeleted: false)
                .Select(m => new SelectListItem
                {
                    Text = m.FullName,
                    Value = m.ID.ToString()
                })
                .ToList();

            return View(model);
        }

        [CustomAuthorize]
        [HttpPost]
        public ActionResult Add(Car model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.UserCar.AddCar(model);
                    _unitOfWork.SaveChanges();
                }

                var errors = ModelState.Values.SelectMany(m => m.Errors).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            model.Users = _unitOfWork.User.GetDrivers(isDeleted: false)
                .Select(m => new SelectListItem
                {
                    Text = m.FullName,
                    Value = m.ID.ToString()
                })
                .ToList();

            return RedirectToAction("Edit", routeValues: new { @id = model.CarId });
        }

        [CustomAuthorize]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.UserCar.GetCar(id.Value);

            model.Users = _unitOfWork.User.GetDrivers(isDeleted: false)
                .Select(m => new SelectListItem
                {
                    Text = m.FullName,
                    Value = m.ID.ToString()
                })
                .ToList();

            return View(model);
        }

        [CustomAuthorize]
        [HttpPost]
        public ActionResult Edit(Car model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.UserCar.UpdateCar(model);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RedirectToAction("Edit", routeValues: new { id = model.CarId });
        }

        [CustomAuthorize]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.UserCar.Get(id.Value);
            model.Deleted = true;

            _unitOfWork.UserCar.Update(model);
            _unitOfWork.SaveChanges();

            return RedirectToAction("Index");

        }
        #endregion

    }
}