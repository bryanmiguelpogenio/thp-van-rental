﻿using System;
using System.Linq;
using System.Web.Mvc;
using THP.Web.Areas.Admin.Models.Booking;
using THP.Web.Models;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.Configuration;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Areas.Admin.Controllers
{
    public class BookingController : Controller
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly IBraintreeConfiguration _braintreeConfiguration;

        #endregion

        #region Ctor

        public BookingController(IUnitOfWork unitOfWork, IBraintreeConfiguration braintreeConfiguration)
        {
            this._unitOfWork = unitOfWork;
            this._braintreeConfiguration = braintreeConfiguration;
        }

        #endregion

        #region Utilities

        [HttpPost]
        public JsonResult ApplyDiscountCode(string discountCode)
        {
            var discount = _unitOfWork.Discount.GetDiscountByDiscountCode(discountCode);

            var dateToday = DateTime.UtcNow;
            if (discount.StartDate <= dateToday && discount.EndDate >= dateToday)
            {
                return Json(new { id = discount.ID, amount = discount.DiscountAmount, ErrorMessage = string.Empty });
            }
            else
                return Json(new { ErrorMessage = "Discount does not exist." });
        }


        [HttpPost]
        public JsonResult CompleteBooking(int bookingId)
        {
            var booking = _unitOfWork.Booking.Get(bookingId);

            // Check if deleted 
            if (booking.Deleted)
            {
                return Json(new { Message = "Booking does not exist" });
            }

            // Check if date is already done
            var dateToday = DateTime.UtcNow;
            if (booking.EndDateScheduled > dateToday)
            {
                return Json(new { Message = "Schedule of booking is not yet finished" });
            }

            booking.Status = Constant.BOOKING_STATUS_COMPLETE;
            booking.DateUpdated = DateTime.UtcNow;
            booking.UpdatedBy = AccountConstant.UserID;
            booking.DateCompleted = DateTime.UtcNow;

            var userCar = _unitOfWork.UserCar
                .Find(m => m.UserID == booking.DriverUserID)
                .LastOrDefault();

            if (userCar != null)
            {
                var currentTotalKm = userCar.TotalKilometer.HasValue ? userCar.TotalKilometer.Value : 0;
                userCar.TotalKilometer = currentTotalKm + booking.Kilometer;

                _unitOfWork.UserCar.Update(userCar);
            }

            _unitOfWork.Booking.Update(booking);

            // Create a notifcation for customer
            var notification = new Notification
            {
                UserID = booking.UserID,
                Message = $"{booking.BookingNo} - is now completed",
                IsClicked = true,
                CreatedBy = booking.UserID,
                DateCreated = DateTime.UtcNow,
                UpdatedBy = booking.UserID,
                DateUpdated = DateTime.UtcNow
            };

            _unitOfWork.Notification.Add(notification);

            notification = new Notification
            {
                UserID = booking.DriverUserID,
                Message = $"{booking.BookingNo} - is now completed",
                IsClicked = false,
                CreatedBy = booking.DriverUserID,
                DateCreated = DateTime.UtcNow,
                UpdatedBy = booking.DriverUserID,
                DateUpdated = DateTime.UtcNow
            };

            _unitOfWork.Notification.Add(notification);

            _unitOfWork.SaveChanges();

            return Json(new { Message = "Booking Complete!" });
        }

        [HttpPost]
        public JsonResult OnGoingBooking(int bookingId)
        {
            var booking = _unitOfWork.Booking.Get(bookingId);

            // Check if deleted 
            if (booking.Deleted)
            {
                return Json(new { Message = "Booking does not exist" });
            }

            // Check if date is already done
            var dateToday = DateTime.UtcNow;
            if (booking.EndDateScheduled > dateToday)
            {
                return Json(new { Message = "Schedule of booking is not yet starting" });
            }

            booking.Status = Constant.BOOKING_STATUS_ONGOING;
            booking.DateUpdated = DateTime.UtcNow;
            booking.UpdatedBy = AccountConstant.UserID;
            booking.DateCompleted = DateTime.UtcNow;

            _unitOfWork.Booking.Update(booking);

            return Json(new { Message = "Booking is now On Going!" });
        }

        [HttpPost]
        public JsonResult CancelBooking(int bookingId)
        {
            var booking = _unitOfWork.Booking.Get(bookingId);

            // Check if deleted 
            if (booking.Deleted)
            {
                return Json(new { Message = "Booking does not exist" });
            }

            //// Check if date is already done
            //var dateToday = DateTime.UtcNow;
            //if (booking.EndDateScheduled > dateToday)
            //{
            //    return Json(new { Message = "Schedule of booking is not yet finished" });
            //}

            // Braintree gateway configuration
            var gateway = _braintreeConfiguration.GetGateway();
            var transaction = gateway.Transaction.Refund(booking.TransactionID, booking.TotalPrice);
            if (transaction.IsSuccess())
            {
                booking.Status = Constant.BOOKING_STATUS_CANCELLED;
                booking.DateUpdated = DateTime.UtcNow;
                booking.UpdatedBy = AccountConstant.UserID;

                _unitOfWork.Booking.Update(booking);

                // Create a notifcation for customer
                var notifcation = new Notification
                {
                    UserID = booking.UserID,
                    Message = $"{booking.BookingNo} - has been cancelled",
                    IsClicked = true,
                    CreatedBy = booking.UserID,
                    DateCreated = DateTime.UtcNow,
                    UpdatedBy = booking.UserID,
                    DateUpdated = DateTime.UtcNow
                };

                _unitOfWork.Notification.Add(notifcation);

                notifcation = new Notification
                {
                    UserID = booking.DriverUserID,
                    Message = $"{booking.BookingNo} - has been cancelled",
                    IsClicked = false,
                    CreatedBy = booking.DriverUserID,
                    DateCreated = DateTime.UtcNow,
                    UpdatedBy = booking.DriverUserID,
                    DateUpdated = DateTime.UtcNow
                };

                _unitOfWork.Notification.Add(notifcation);

                _unitOfWork.SaveChanges();

                return Json(new { Message = "Booking Cancelled!" });
            }
            else
            {
                return Json(new { Message = transaction.Message, IsSuccess = false });
            }
        }

        #endregion

        #region Methods

        [CustomAuthorize]
        public ActionResult Index()
        {
            var model = new BookingViewModel();
            model.Bookings = _unitOfWork.Booking.GetAdminBookings(isDeleted: false)
                .ToList();
            return View(model);
        }

        [CustomAuthorize]
        [HttpPost]
        public ActionResult Index(BookingViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.Bookings = _unitOfWork.Booking.GetAdminBookings(bookingNo: model.BookingNo, status: model.Status, 
                        startDate: model.StartDate, endDate: model.EndDate, isDeleted: false)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return View(model);
        }

        [CustomAuthorize]
        public ActionResult Details(int? id)
        {

            if (!id.HasValue || id.Value == 0)
                return RedirectToAction("Index");

            var model = _unitOfWork.Booking.GetAdminBooking(id.Value);

            return View(model);
        }
        #endregion
    }
}