﻿using System.Threading;
using System.Web;

namespace THP.Web.Models
{
    public class AccountConstant
    {
        public static string Username = StaticMethods.getUsername(HttpContext.Current.Session["username"]);
        public static int UserID = StaticMethods.getUserID(HttpContext.Current.Session["userID"]);
        public static string DateTimeFormat = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
    }
}