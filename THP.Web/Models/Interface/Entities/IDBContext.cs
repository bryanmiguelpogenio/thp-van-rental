﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace THP.Web.Models.Interface.Entities
{
    public interface IDBContext
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry Entry(object entity);
        int SaveChanges();
        void Dispose();
    }
}
