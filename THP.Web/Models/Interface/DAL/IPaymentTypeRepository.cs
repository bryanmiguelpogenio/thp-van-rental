﻿using System.Collections.Generic;
using THP.Web.Models.Entities.Data;
using AdminPaymentType = THP.Web.Areas.Admin.Models.PaymentType.PaymentType;

namespace THP.Web.Models.Interface.DAL
{
    public interface IPaymentTypeRepository : IRepository<PaymentType>
    {
        IEnumerable<AdminPaymentType> GetPaymentTypes(string name = "", bool? isDeleted = null);
        AdminPaymentType GetPaymentType(int? id = null);
        void UpdatePaymentType(AdminPaymentType model);
        void AddPaymentType(AdminPaymentType model);
    }
}