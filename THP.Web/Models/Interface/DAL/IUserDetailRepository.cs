﻿using THP.Web.Models.DAL.Data.Accounts;
using THP.Web.Models.Entities.Data;

namespace THP.Web.Models.Interface.DAL
{
    public interface IUserDetailRepository : IRepository<UserDetail>
    {
        void CreateUserDetail(ProfileViewModel model, int userID);

        int CreateUserDetailMobile(UserDetail model);

        void UpdateUserDetailMobile(UserDetail model);
    }
}
