﻿using System.Collections.Generic;
using THP.Web.Areas.Admin.Models.Car;
using THP.Web.Models.Entities.Data;

namespace THP.Web.Models.Interface.DAL
{
    public interface IUserCarRepository : IRepository<UserCar>
    {
        List<Car> GetCars(int? userId = null, bool? isDeleted = null);
        Car GetCar(int? id);
        void UpdateCar(Car model);
        void AddCar(Car model);
    }
}