﻿using THP.Web.Models.Entities.Data;

namespace THP.Web.Models.Interface.DAL
{
    public interface IUserCarSpecificationRepository : IRepository<UserCarSpecification>
    {
    }
}