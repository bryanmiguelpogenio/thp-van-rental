﻿using System;
using THP.Web.THP.Mobile.Api.Repositories.BookingRepository;

namespace THP.Web.Models.Interface.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IBookingRepository Booking { get; }
        IBookingRepositoryMobile MobileBooking { get; }
        ILocationRepository Location { get; }
        IPageRepository Page { get; }
        IPaymentTypeRepository PaymentType { get; }
        IRoleRepository Role { get; }
        IServiceTypeRepository ServiceType { get; }
        ISpecificationRepository Specification { get; }
        ITravelRateRepository TravelRate { get; }
        IUserCarRepository UserCar { get; }
        IUserCarSpecificationRepository UserCarSpecification { get; }
        IUserDetailRepository UserDetail { get; }
        IUserRepository User { get; }
        IDiscountRepository Discount { get; }
        IDiscountUsageHistoryRepository DiscountUsageHistory { get; }
        IBookingReviewRepository BookingReview { get; }
        ITopicPageRepository TopicPage { get; }
        INotificationRepository Notification { get; }

        int SaveChanges();

        void DefaultCulture();
    }
}
