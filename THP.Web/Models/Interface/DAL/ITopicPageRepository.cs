﻿using System.Collections.Generic;
using THP.Web.Areas.Admin.Models.TopicPage;
using THP.Web.Models.Entities.Data;

namespace THP.Web.Models.Interface.DAL
{
    public interface ITopicPageRepository : IRepository<TopicPage>
    {
        IEnumerable<Topic> GetTopicPages(string name = "", bool? isDeleted = null);
        Topic GetTopic(int? id = null);
        void UpdateTopic(Topic model);
        void AddTopic(Topic model);
    }
}
