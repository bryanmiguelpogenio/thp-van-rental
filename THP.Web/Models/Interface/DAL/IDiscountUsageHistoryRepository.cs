﻿using System.Collections.Generic;
using THP.Web.Models.Entities.Data;

namespace THP.Web.Models.Interface.DAL
{
    public interface IDiscountUsageHistoryRepository : IRepository<DiscountUsageHistory>
    {
        DiscountUsageHistory GetDiscountUsageHistoryByBookingId(int? id);
        IEnumerable<DiscountUsageHistory> GetDiscountUsageHistoryByDiscountId(int? id);
    }
}
