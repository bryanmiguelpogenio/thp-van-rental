﻿using System;
using System.Collections.Generic;
using THP.Web.Areas.Admin.Models.Customer;
using THP.Web.Models.DAL.Data.Accounts;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Security;

namespace THP.Web.Models.Interface.DAL
{
    public interface IUserRepository : IRepository<User>
    {
        Account GetUser(string username);
        User CreateUser(AccountRegisterViewModel model);
        void VerifyUser(string username);
        Customer GetCustomer(int? id = null);
        IEnumerable<Customer> GetCustomers(string fullName = "", string emailAddress = "", int? roleId = null,
            DateTime? startDateTimeCreated = null, DateTime? endDateTimeCreated = null,
            DateTime? startdateTimeUpdated = null, DateTime? enddateTimeUpdated = null, bool? isDeleted = null);
        IEnumerable<User> GetDrivers(bool? isDeleted = null);
        void UpdateCustomer(Customer model);
        void AddCustomer(Customer model);
    }
}
