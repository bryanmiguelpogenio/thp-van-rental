﻿using System.Collections.Generic;
using THP.Web.Areas.Admin.Models.Destination;
using THP.Web.Models.Entities.Data;

namespace THP.Web.Models.Interface.DAL
{
    public interface ILocationRepository : IRepository<Location>
    {
        IEnumerable<Destination> GetDestinations(string name = "", int? provinceId = null, bool? isDeleted = null);
        Destination GetDestination(int? Id = null);
        void UpdateDestination(Destination model);
        void AddDestination(Destination model);
    }
}