﻿using THP.Web.Models.Entities.Data;

using AdminBookingReview = THP.Web.Areas.Admin.Models.Booking.BookingReview;

namespace THP.Web.Models.Interface.DAL
{
    public interface IBookingReviewRepository : IRepository<BookingReview>
    {
        AdminBookingReview GetBookingReviewByBookingId(int? id);

        void AddBookingReview(BookingReview model);
    }
}
