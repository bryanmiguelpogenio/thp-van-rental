﻿using System.Collections.Generic;
using THP.Web.Models.Entities.Data;

using AdminServiceType = THP.Web.Areas.Admin.Models.ServiceType.ServiceType;

namespace THP.Web.Models.Interface.DAL
{
    public interface IServiceTypeRepository : IRepository<ServiceType>
    {
        IEnumerable<AdminServiceType> GetServiceType(string name = "", bool? isDeleted = null);
        AdminServiceType GetServiceType(int? id = null);
        void UpdateServiceType(AdminServiceType model);
        void AddServiceType(AdminServiceType model);
    }
}