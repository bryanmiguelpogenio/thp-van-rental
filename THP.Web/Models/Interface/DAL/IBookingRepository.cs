﻿using System;
using System.Collections.Generic;
using THP.Web.Models.DAL.Data.Booking;
using THP.Web.Models.DAL.Data.Home;
using THP.Web.Models.DAL.Data.Order;
using THP.Web.Models.Entities.Data;

using AdminBookingViewModel = THP.Web.Areas.Admin.Models.Booking.BookingViewModel;
using AdminBooking = THP.Web.Areas.Admin.Models.Booking.Booking;

namespace THP.Web.Models.Interface.DAL
{
    public interface IBookingRepository : IRepository<Booking>
    {
        void CreateBooking(BookingViewModel model);
        void CreateBooking(HomeQuickBookingViewModel model);
        decimal ComputeTotalPrice(decimal? price, DateTime? startDate, DateTime? endDate, decimal? discountAmount = null);
        List<ReservedSchedule> GetReservedSchedules(List<Booking> bookings);
        OrderViewModel GetBooking(int? id);

        List<AdminBooking> GetAdminBookings(string bookingNo = null, string status = null, DateTime? startDate = null, DateTime? endDate = null, bool? isDeleted = null);
        AdminBooking GetAdminBooking(int? id);
        List<DayOfWeek> GetDriverCoding(int userId = 0);
        int GetAvailableDriver(DateTime? startDate, DateTime? endDate);
    }
}