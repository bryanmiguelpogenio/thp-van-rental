﻿using System.Collections.Generic;
using THP.Web.Models.Entities.Data;
using AdminTravelRate = THP.Web.Areas.Admin.Models.TravelRate.TravelRate;

namespace THP.Web.Models.Interface.DAL
{
    public interface ITravelRateRepository : IRepository<TravelRate>
    {
        IEnumerable<AdminTravelRate> GetTravelRates(int? fromId = null, int? toId = null, bool? isDeleted = null);
        AdminTravelRate GetTravelRate(int? id = null);
        void UpdateTravelRate(AdminTravelRate model);
        void AddTravelRate(AdminTravelRate model);
    }
}