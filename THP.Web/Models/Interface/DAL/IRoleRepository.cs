﻿using System.Collections.Generic;
using THP.Web.Models.Entities.Data;

using AdminRole = THP.Web.Areas.Admin.Models.Role.Role;

namespace THP.Web.Models.Interface.DAL
{
    public interface IRoleRepository : IRepository<Role>
    {
        IEnumerable<AdminRole> GetRoles(string name = "", bool? isDeleted = null);
        AdminRole GetRole(int? id = null);
        void UpdateRole(AdminRole model);
        void AddRole(AdminRole model);
    }
}
