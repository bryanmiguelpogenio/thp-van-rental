﻿using System;
using System.Collections.Generic;
using THP.Web.Models.Entities.Data;
using AdminDiscount = THP.Web.Areas.Admin.Models.Discount.Discount;

namespace THP.Web.Models.Interface.DAL
{
    public interface IDiscountRepository : IRepository<Discount>
    {
        IEnumerable<AdminDiscount> GetDiscounts(string name = "", DateTime? startDate = null, DateTime? endDate = null, bool? isDeleted = null);
        AdminDiscount GetDiscount(int? id = null);
        Discount GetDiscountByDiscountCode(string discountCode = "");
        void UpdateDiscount(AdminDiscount model);
        void AddDiscount(AdminDiscount model);
    }
}
