﻿using THP.Web.Models.Entities.Data;

namespace THP.Web.Models.Interface.DAL
{
    public interface IPageRepository : IRepository<Page>
    {
    }
}
