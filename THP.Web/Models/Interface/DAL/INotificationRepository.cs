﻿using THP.Web.Models.Entities.Data;

namespace THP.Web.Models.Interface.DAL
{
    public interface INotificationRepository : IRepository<Notification>
    {

    }
}