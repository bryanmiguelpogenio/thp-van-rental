using System.Data.Entity;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.Entities;

namespace THP.Web.Models.Entities
{
    public partial class DBContext : DbContext, IDBContext
    {
        public DBContext()
            : base("name=DBContext")
        {
        }

        public virtual DbSet<Page> Pages { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserDetail> UserDetails { get; set; }
        public virtual DbSet<PaymentType> PaymentTypes { get; set; }
        public virtual DbSet<ServiceType> ServiceTypes { get; set; }
        public virtual DbSet<Specification> Specifications { get; set; }
        public virtual DbSet<TravelRate> TravelRates { get; set; }
        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<UserCar> UserCars { get; set; }
        public virtual DbSet<UserCarSpecification> UserCarSpecifications { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<DiscountUsageHistory> DiscountUsageHistories { get; set; }
        public virtual DbSet<BookingReview> BookingReviews { get; set; }
        public virtual DbSet<TopicPage> TopicPages { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Booking>()
                .Property(e => e.TotalPrice)
                .HasPrecision(7, 2);

            modelBuilder.Entity<TravelRate>()
                .Property(e => e.FixedPrice)
                .HasPrecision(7, 2);

            modelBuilder.Entity<TravelRate>()
                .Property(e => e.Overtime)
                .HasPrecision(7, 2);

            modelBuilder.Entity<TravelRate>()
                .Property(e => e.Overnight)
                .HasPrecision(7, 2);

            modelBuilder.Entity<UserCar>()
                .Property(e => e.TotalKilometer)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Discount>()
                .Property(e => e.DiscountAmount)
                .HasPrecision(7, 2);
        }
    }
}
