namespace THP.Web.Models.Entities.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserDetail")]
    public partial class UserDetail
    {
        public int ID { get; set; }

        [ForeignKey("User")]
        public int UserID { get; set; }
        public virtual User User { get; set; }

        public DateTime? Birthdate { get; set; }

        [StringLength(12)]
        public string CellphoneNo { get; set; }

        [StringLength(7)]
        public string TelephoneNo { get; set; }

        [StringLength(6)]
        public string Gender { get; set; }

        public string Address { get; set; }

        [StringLength(50)]
        public string Municipality { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string Province { get; set; }

        [StringLength(50)]
        public string Region { get; set; }

        public bool Deleted { get; set; }

        public int CreatedBy { get; set; }

        public DateTime DateCreated { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime DateUpdated { get; set; }
    }
}
