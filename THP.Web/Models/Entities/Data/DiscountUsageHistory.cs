﻿namespace THP.Web.Models.Entities.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("DiscountUsageHistory")]
    public partial class DiscountUsageHistory
    {
        public int ID { get; set; }

        [Required]
        [ForeignKey("Discount")]
        public int DiscountID { get; set; }
        public virtual Discount Discount { get; set; }

        [Required]
        [ForeignKey("Booking")]
        public int BookingID { get; set; }
        public virtual Booking Booking { get; set; }

        public int CreatedBy { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
