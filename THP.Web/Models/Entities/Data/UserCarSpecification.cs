namespace THP.Web.Models.Entities.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserCarSpecification")]
    public partial class UserCarSpecification
    {
        public int ID { get; set; }

        public int UserCarID { get; set; }

        public int SpecificationID { get; set; }

        [Required]
        public string Value { get; set; }

        public bool Deleted { get; set; }

        public int CreatedBy { get; set; }

        public DateTime DateCreated { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime DateUpdated { get; set; }
    }
}
