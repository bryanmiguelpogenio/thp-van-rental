namespace THP.Web.Models.Entities.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TravelRate")]
    public partial class TravelRate
    {
        public int ID { get; set; }

        [Required]
        [ForeignKey("PickupPoint")]
        public int PickupPointID { get; set; }
        public virtual Location PickupPoint { get; set; }

        [Required]
        [ForeignKey("DestinationPoint")]
        public int DestinationPointID { get; set; }
        public virtual Location DestinationPoint { get; set; }

        public decimal? Kilometer { get; set; }

        public decimal? FixedPrice { get; set; }

        public decimal? Overtime { get; set; }

        public decimal? Overnight { get; set; }

        public int MaxHours { get; set; }

        public bool Deleted { get; set; }

        public int CreatedBy { get; set; }

        public DateTime DateCreated { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime DateUpdated { get; set; }

        //[NotMapped]
        //public string Route
        //{
        //    get { return $"{PickupPoint} - {DestinationPoint}".Trim(); }
        //}
    }
}
