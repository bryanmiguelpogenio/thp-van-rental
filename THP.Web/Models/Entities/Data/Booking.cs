namespace THP.Web.Models.Entities.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Booking")]
    public partial class Booking
    {
        public int ID { get; set; }
        
        [Required]
        [StringLength(10)]
        public string BookingNo { get; set; }
        
        [ForeignKey("User")]
        public int UserID { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("DriverUser")]
        public int DriverUserID { get; set; }
        public virtual User DriverUser { get; set; }

        [ForeignKey("TravelRate")]
        public int TravelRateID { get; set; }
        public virtual TravelRate TravelRate { get; set; }

        [ForeignKey("PaymentType")]
        public int PaymetTypeID { get; set; }
        public virtual PaymentType PaymentType { get; set; }

        [Required]
        [StringLength(100)]
        public string TransactionID { get; set; }

        public int Seater { get; set; }

        public decimal TotalPrice { get; set; }

        [Required]
        [StringLength(10)]
        public string Currency { get; set; }

        public DateTime StartDateScheduled { get; set; }

        public DateTime EndDateScheduled { get; set; }

        public DateTime? DateCompleted { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; }

        [Required]
        public decimal? Kilometer { get; set; }

        public bool Deleted { get; set; }

        public int CreatedBy { get; set; }

        public DateTime DateCreated { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime DateUpdated { get; set; }

        [NotMapped]
        public string CurrencyPrice
        {
            get { return $"{Currency} {TotalPrice}"; }
        }
    }
}
