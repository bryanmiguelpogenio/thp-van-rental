namespace THP.Web.Models.Entities.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Location")]
    public partial class Location
    {
        public int ID { get; set; }

        [ForeignKey("ParentLocation")]
        public int ParentLocationID { get; set; }
        public virtual Location ParentLocation { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public string Description { get; set; }

        public bool Deleted { get; set; }

        public int CreatedBy { get; set; }

        public DateTime DateCreated { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime DateUpdated { get; set; }

        [NotMapped]
        public string Address
        {
            get
            {
                if (ParentLocation != null)
                {
                    return $"{Name}, {ParentLocation.Name}";
                }
                else
                {
                    return Name;
                }
            }
        }
    }
}
