﻿using System.Configuration;
using System.Web;
using THP.Web.Models.Entities.Data;

namespace THP.Web.Models
{
    public class Constant
    {
        public static string DOMAIN_NAME = HttpContext.Current.Request.Url.Authority;
        public static ConnectionStringSettings CONNECTION_STRING = ConfigurationManager.ConnectionStrings["DBContext"];

        public static string USER_TABLE = typeof(User).Name;
        public static string USER_COLUMN_ID = "ID";
        public static string USER_COLUMN_USERNAME = "Username";

        public static string ADMIN_ROLE = "Administrator";
        public static string CUSTOMER_ROLE = "Customer";
        public static string DRIVER_ROLE = "Driver";
        public static string GUEST_ROLE = "Guest";

        //public static int ADMIN_ROLE_ID = 1;
        //public static int CUSTOMER_ROLE_ID = 2;
        //public static int DRIVER_ROLE_ID = 3;
        //public static int GUEST_ROLE_ID = 4;

        public static string BOOKING_STATUS_QUEUED = "Queued";
        public static string BOOKING_STATUS_ONGOING = "On Going";
        public static string BOOKING_STATUS_CANCELLED = "Cancelled";
        public static string BOOKING_STATUS_COMPLETE = "Complete";

        public static string DEFAULT_CURRENCY = "PHP";
    }
}