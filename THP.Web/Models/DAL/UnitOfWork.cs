﻿using System.Globalization;
using System.Threading;
using THP.Web.Models.DAL.Repositories;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;
using THP.Web.Models.Interface.Security;
using THP.Web.THP.Mobile.Api.Repositories.BookingRepository;

namespace THP.Web.Models.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDBContext _dbContext;
        public readonly IEncryptionService _encryption;

        public UnitOfWork(IDBContext dbContext, IEncryptionService encryption)
        {
            this._dbContext = dbContext;
            this._encryption = encryption;

            Role = new RoleRepository(_dbContext);
            Page = new PageRepository(_dbContext);
            UserDetail = new UserDetailRepository(_dbContext);
            User = new UserRepository(_dbContext, _encryption, Role, UserDetail);
            UserCar = new UserCarRepository(_dbContext, User);
            TravelRate = new TravelRateRepository(_dbContext, User);
            BookingReview = new BookingReviewRepository(_dbContext, User);
            Discount = new DiscountRepository(_dbContext, User);
            DiscountUsageHistory = new DiscountUsageHistoryRepository(_dbContext);
            Notification = new NotificationRepository(_dbContext);
            Booking = new BookingRepository(_dbContext, User, TravelRate, BookingReview, Discount, DiscountUsageHistory, UserCar, Notification);
            MobileBooking = new BookingRepositoryMobile(_dbContext, User, Discount, DiscountUsageHistory, UserCar, Notification);
            Location = new LocationRepository(_dbContext, User);
            PaymentType = new PaymentTypeRepository(_dbContext, User);
            ServiceType = new ServiceTypeRepository(_dbContext, User);
            Specification = new SpecificationRepository(_dbContext);
            UserCarSpecification = new UserCarSpecificationRepository(_dbContext);
            TopicPage = new TopicPageRepository(_dbContext, User);
            Notification = new NotificationRepository(_dbContext);
        }

        public IRoleRepository Role { get; private set; }
        public IPageRepository Page { get; private set; }
        public IUserDetailRepository UserDetail { get; private set; }
        public IUserRepository User { get; private set; }
        public IBookingRepository Booking { get; private set; }
        public IBookingRepositoryMobile MobileBooking { get; private set; }
        public ILocationRepository Location { get; private set; }
        public IPaymentTypeRepository PaymentType { get; private set; }
        public IServiceTypeRepository ServiceType { get; private set; }
        public ISpecificationRepository Specification { get; private set; }
        public ITravelRateRepository TravelRate { get; private set; }
        public IUserCarRepository UserCar { get; private set; }
        public IUserCarSpecificationRepository UserCarSpecification { get; private set; }
        public IDiscountRepository Discount { get; private set; }
        public IDiscountUsageHistoryRepository DiscountUsageHistory { get; private set; }
        public IBookingReviewRepository BookingReview { get; private set; }
        public ITopicPageRepository TopicPage { get; private set; }
        public INotificationRepository Notification { get; private set; }

        public void DefaultCulture()
        {
            CultureInfo culture = new CultureInfo("en-PH", true);
            culture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy HH:mm:ss";

            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }
    }
}