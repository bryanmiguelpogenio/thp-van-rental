﻿namespace THP.Web.Models.DAL.Data.Enum
{
    public enum RoleType
    {
        Admin = 1,
        Customer = 2,
        Driver = 3
    }
}