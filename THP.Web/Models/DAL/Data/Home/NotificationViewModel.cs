﻿namespace THP.Web.Models.DAL.Data.Home
{
    public class NotificationViewModel
    {
        public int UserId { get; set; }
        public string Message { get; set; }
    }
}