﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace THP.Web.Models.DAL.Data.Home
{
    public class HomeQuickBookingViewModel
    {
        public HomeQuickBookingViewModel()
        {
            lst_originLocationID = new List<SelectListItem>();
            lst_destinationLocationID = new List<SelectListItem>();
            //lst_unavailableDates = new List<DateTime>();

            lst_Rides = new List<SelectListItem>();
            lst_Rides.Add(new SelectListItem
            {
                Text = "One way Ride",
                Value = 1.ToString()
            });
            lst_Rides.Add(new SelectListItem
            {
                Text = "Two way Ride",
                Value = 2.ToString()
            });
        }

        //public List<DateTime> lst_unavailableDates { get; set; }

        [Display(Name = "Pick-Up Point:")]
        [Required(ErrorMessage = "Please specify your location")]
        public int originLocationID { get; set; }
        public List<SelectListItem> lst_originLocationID { get; set; }

        [Display(Name = "Destination:")]
        [Required(ErrorMessage = "Please specify your laction")]
        public int destinationLocationID { get; set; }
        public List<SelectListItem> lst_destinationLocationID { get; set; }

        [Display(Name = "Seats:")]
        [Required(ErrorMessage = "Please select a number of seats")]
        public int Seater { get; set; }

        [Display(Name = "Pick up Date:")]
        [Required(ErrorMessage = "Please select a starting date")]
        public DateTime? StartDateScheduled { get; set; }

        [Display(Name = "Return Date:")]
        [Required(ErrorMessage = "Please select a end date")]
        public DateTime? EndDateScheduled { get; set; }

        public int DiscountId { get; set; }
        public string OrderId { get; set; }

        [Display(Name = "Ride:")]
        [Required(ErrorMessage = "Please select if One-way or Two-way")]
        public int Ride { get; set; }
        public List<SelectListItem> lst_Rides { get; set; }

        public string Distance { get; set; }
    }
}