﻿using System;
using System.Collections.Generic;

namespace THP.Web.Models.DAL.Data.Booking
{
    public class ReservedSchedule
    {
        public List<int> DriverIds { get; set; }
        public DateTime ReservedDate { get; set; }
    }
}