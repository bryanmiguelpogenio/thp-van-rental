﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Models.DAL.Data.Booking
{
    public class BookingViewModel
    {

        #region Ctor
        public BookingViewModel()
        {
            lst_unavailableDates = new List<DateTime>();
            lst_PickUpPointID = new List<SelectListItem>();
            lst_DestinationPointID = new List<SelectListItem>();

            lst_Rides = new List<SelectListItem>();
            lst_Rides.Add(new SelectListItem {
                Text = "One way Ride",
                Value = 1.ToString()
            });
            lst_Rides.Add(new SelectListItem
            {
                Text = "Two way Ride",
                Value = 2.ToString()
            });
        }
        #endregion

        public List<DateTime> lst_unavailableDates { get; set; }
        
        [Display(Name = "Pick-Up Point:")]
        [Required(ErrorMessage = "Please select a pick-up point")]
        public int PickUpPointID { get; set; }
        public List<SelectListItem> lst_PickUpPointID { get; set; }

        [Display(Name = "Destination:")]
        [Required(ErrorMessage = "Please select a destination point")]
        public int DestinationPointID { get; set; }
        public List<SelectListItem> lst_DestinationPointID { get; set; }

        [Display(Name = "Seats:")]
        [Required(ErrorMessage = "Please select a number of seats")]
        public int Seater { get; set; }

        [Display(Name = "Start Date:")]
        [Required(ErrorMessage = "Please select a starting date")]
        public DateTime? StartDateScheduled { get; set; }

        [Display(Name = "End Date:")]
        [Required(ErrorMessage = "Please select a end date")]
        public DateTime? EndDateScheduled { get; set; }
        
        public int DiscountId { get; set; }
        public string OrderId { get; set; }

        [Display(Name = "Ride:")]
        [Required(ErrorMessage = "Please select if One-way or Two-way")]
        public int Ride { get; set; }
        public List<SelectListItem> lst_Rides { get; set; }

        public string Distance { get; set; }
    }
}