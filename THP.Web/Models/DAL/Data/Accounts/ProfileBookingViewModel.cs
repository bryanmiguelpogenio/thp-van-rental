﻿using System;

namespace THP.Web.Models.DAL.Data.Accounts
{
    public class ProfileBookingViewModel
    {
        public int Id { get; set; }
        public string BookingNo { get; set; }
        public string DriverName { get; set; }
        public string CustomerName { get; set; }
        public string Route { get; set; }
        public string Status { get; set; }
        public DateTime? StartDateScheduled { get; set; }
        public DateTime? EndDateScheduled { get; set; }
        public string DateCompleted { get; set; }
        public string TotalPrice { get; set; }
    }
}