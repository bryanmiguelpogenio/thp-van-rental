﻿using System.ComponentModel.DataAnnotations;

namespace THP.Web.Models.DAL.Data.Accounts
{
    public class AccountLoginViewModel
    {
        [Display(Name = "Username")]
        [Required(ErrorMessage = "Please enter a username")]
        public string Username { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Pease enter a password")]
        public string Password { get; set; }
    }
}