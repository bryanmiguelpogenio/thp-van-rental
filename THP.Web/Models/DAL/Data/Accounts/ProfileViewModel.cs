﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Models.DAL.Data.Accounts
{
    public class ProfileViewModel
    {
        #region Ctor
        public ProfileViewModel()
        {
            lstProfileBooking = new List<ProfileBookingViewModel>();
            //lstRole = new List<SelectListItem>();
            lstGender = new List<SelectListItem>();

            lstGender.Add(new SelectListItem
            {
                Text = "Male",
                Value = "Male"
            });
            lstGender.Add(new SelectListItem
            {
                Text = "Female",
                Value = "Female"
            });

            // Default user details
            CellphoneNo = string.Empty;
            TelephoneNo = string.Empty;
            Gender = string.Empty;
            Address = string.Empty;
            Municipality = string.Empty;
            City = string.Empty;
            Province = string.Empty;
            Region = string.Empty;
        }
        #endregion

        // User
        [Display(Name = "Firstname:")]
        [Required(ErrorMessage = "Please enter your firstname")]
        public string Firstname { get; set; }

        [Display(Name = "Middlename:")]
        [Required(ErrorMessage = "Please enter your middlename")]
        public string Middlename { get; set; }

        [Display(Name = "Lastname:")]
        [Required(ErrorMessage = "Please enter your lastname")]
        public string Lastname { get; set; }

        [Display(Name = "Email Address:")]
        [Required(ErrorMessage = "Please enter a valid email address")]
        public string EmailAddress { get; set; }

        [Display(Name = "Username:")]
        [Required(ErrorMessage = "Please enter username")]
        public string Username { get; set; }

        public string Password { get; set; }
        
        [Display(Name = "Role:")]
        //[Required(ErrorMessage = "Please select a role")]
        //public int RoleID { get; set; }
        //public List<SelectListItem> lstRole { get; set;}
        public string RoleName { get; set; }

        // User Details
        [Display(Name = "Birthdate:")]
        [Required(ErrorMessage = "Please enter your birthdate")]
        public DateTime? Birthdate { get; set; }

        [Display(Name = "Cellphone #:")]
        [Required(ErrorMessage = "Please enter your cellphone #")]
        public string CellphoneNo { get; set; }

        [Display(Name = "Telephone #:")]
        [Required(ErrorMessage = "Please enter your telephone #")]
        public string TelephoneNo { get; set; }

        [Display(Name = "Gender:")]
        [Required(ErrorMessage = "Please select your gender")]
        public string Gender { get; set; }
        public List<SelectListItem> lstGender { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Please enter your address")]
        public string Address { get; set; }

        [Display(Name = "Municipality")]
        [Required(ErrorMessage = "Please enter your municipality")]
        public string Municipality { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "Please enter your city")]
        public string City { get; set; }

        [Display(Name = "Province")]
        [Required(ErrorMessage = "Please enter your province")]
        public string Province { get; set; }

        [Display(Name = "Region")]
        [Required(ErrorMessage = "Please enter your region")]
        public string Region { get; set; }

        // Booking History
        public List<ProfileBookingViewModel> lstProfileBooking { get; }
    }
}