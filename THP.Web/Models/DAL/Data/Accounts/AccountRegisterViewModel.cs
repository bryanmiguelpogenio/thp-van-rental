﻿using ExpressiveAnnotations.Attributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Models.DAL.Data.Accounts
{
    public class AccountRegisterViewModel
    {
        #region Ctor
        public AccountRegisterViewModel()
        {
            lstRole = new List<SelectListItem>();
        }
        #endregion

        [Display(Name = "Firstname")]
        [Required(ErrorMessage = "Firstname is required")]
        public string Firstname { get; set; }

        [Display(Name = "Middlename")]
        [Required(ErrorMessage = "Middlename is required")]
        public string Middlename { get; set; }

        [Display(Name = "Lastname")]
        [Required(ErrorMessage = "Lastname is required")]
        public string Lastname { get; set; }

        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Email Address is required")]
        public string EmailAddress { get; set; }

        [Display(Name = "Role")]
        [Required(ErrorMessage = "Role is required")]
        public int RoleID { get; set; }
        public List<SelectListItem> lstRole { get; set; }

        [Display(Name = "Username")]
        [Required(ErrorMessage = "Username is required")]
        public string Username { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The Password and Confirmation Password does not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Brand")]
        [RequiredIf("RoleID == 3", ErrorMessage = "Brand of the car is required.")]
        public string Brand { get; set; }

        [Display(Name = "Model")]
        [RequiredIf("RoleID == 3", ErrorMessage = "Model of the car is required.")]
        public string CarModel { get; set; }

        [Display(Name = "Color")]
        [RequiredIf("RoleID == 3", ErrorMessage = "Color of the car is required.")]
        public string Color { get; set; }

        [Display(Name = "Plate #")]
        [RequiredIf("RoleID == 3", ErrorMessage = "Plate # of the car is required")]
        [StringLength(7, MinimumLength = 7, ErrorMessage = "Plate # should be 6 characters")]
        public string PlateNumber { get; set; }
    }
}