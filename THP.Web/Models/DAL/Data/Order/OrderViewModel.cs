﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace THP.Web.Models.DAL.Data.Order
{
    public class OrderViewModel
    {
        public OrderViewModel()
        {
            Ratings = new List<SelectListItem>();
            Ratings.Add(new SelectListItem
            {
                Text = 1.ToString(),
                Value = 1.ToString()
            });
            Ratings.Add(new SelectListItem
            {
                Text = 2.ToString(),
                Value = 2.ToString()
            });
            Ratings.Add(new SelectListItem
            {
                Text = 3.ToString(),
                Value = 3.ToString()
            });
            Ratings.Add(new SelectListItem
            {
                Text = 4.ToString(),
                Value = 4.ToString()
            });
            Ratings.Add(new SelectListItem
            {
                Text = 5.ToString(),
                Value = 5.ToString()
            });
        }

        [Display(Name = "Booking Id")]
        public int BookingId { get; set; }
        [Display(Name = "Booking No")]
        public string BookingNo { get; set; }
        [Display(Name = "Driver Name")]
        public string DriverName { get; set; }
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
        [Display(Name = "Start Date Schedule")]
        public DateTime StartDateSchedule { get; set; }
        [Display(Name = "End Date Schedule")]
        public DateTime EndDateSchedule { get; set; }
        [Display(Name = "Pick-up Point")]
        public string PickupPoint { get; set; }
        [Display(Name = "Destinaation Point")]
        public string DestinationPoint { get; set; }
        [Display(Name = "Total Price")]
        public decimal TotalPrice { get; set; }
        [Display(Name = "Currency")]
        public string Currency { get; set; }
        [Display(Name = "Payment Type")]
        public string PaymentType { get; set; }
        [Display(Name = "Booking Status")]
        public string Status { get; set; }
        [Display(Name = "Seater")]
        public int Seater { get; set; }

        public int CustomerID { get; set; }
        public int BookingReviewID { get; set; }

        [Display(Name = "Review Message")]
        public string Message { get; set; }
        [Display(Name = "Ratings")]
        public int Rating { get; set; }
        public List<SelectListItem> Ratings { get; set; }
    }
}