﻿namespace THP.Web.Models.DAL.Data.Tracking
{
    public class GeolocationViewModel
    {
        public decimal lat { get; set; }
        public decimal lang { get; set; }
    }
}