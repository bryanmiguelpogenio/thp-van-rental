﻿using System;
using THP.Web.Models.DAL.Data.Accounts;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

namespace THP.Web.Models.DAL.Repositories
{
    public class UserDetailRepository : Repository<UserDetail>, IUserDetailRepository
    {
        public readonly IDBContext _dbContext;

        public UserDetailRepository(IDBContext dbContext) :
            base(dbContext)
        {
            this._dbContext = dbContext;
        }

        public int CreateUserDetailMobile(UserDetail model)
        {
            model = this._dbContext.Set<UserDetail>().Add(model);
            this._dbContext.SaveChanges();
            return model.ID;
        }

        public void UpdateUserDetailMobile(UserDetail model)
        {
            var userDetail = this.FirstOrDefault(x => x.ID == model.ID);

            userDetail.Birthdate = model.Birthdate;
            userDetail.Gender = model.Gender;
            userDetail.CellphoneNo = model.CellphoneNo;
            userDetail.TelephoneNo = model.TelephoneNo;
            userDetail.Address = model.Address;
            userDetail.City = model.City;
            userDetail.Municipality = model.Municipality;
            userDetail.Province = model.Province;
            userDetail.Region = model.Region;
            userDetail.UpdatedBy = model.UserID;
            userDetail.DateUpdated = DateTime.UtcNow;

            this.Update(userDetail);
        }

        public void CreateUserDetail(ProfileViewModel model, int userID)
        {
            var entity = this.FirstOrDefault(m => m.UserID == userID);

            if (entity != null)
            {
                // Update entity
                entity.Birthdate = model.Birthdate;
                entity.Gender = model.Gender;

                entity.CellphoneNo = model.CellphoneNo;
                entity.TelephoneNo = model.TelephoneNo;
                entity.Address = model.Address;
                entity.City = model.City;
                entity.Municipality = model.Municipality;
                entity.Province = model.Province;
                entity.Region = model.Region;

                var modifier = AccountConstant.UserID;

                entity.UpdatedBy = modifier;
                entity.DateUpdated = DateTime.UtcNow;

                this.Update(entity);
            }
            else
            {
                // Create entity
                entity = new UserDetail();

                entity.UserID = userID;

                entity.Birthdate = model.Birthdate;
                entity.Gender = model.Gender;

                entity.CellphoneNo = model.CellphoneNo;
                entity.TelephoneNo = model.TelephoneNo;
                entity.Address = model.Address;
                entity.City = model.City;
                entity.Municipality = model.Municipality;
                entity.Province = model.Province;
                entity.Region = model.Region;

                var modifier = AccountConstant.UserID;

                entity.CreatedBy = modifier;
                entity.DateCreated = DateTime.UtcNow;
                entity.UpdatedBy = modifier;
                entity.DateUpdated = DateTime.UtcNow;

                this.Add(entity);
            }
        }
    }
}