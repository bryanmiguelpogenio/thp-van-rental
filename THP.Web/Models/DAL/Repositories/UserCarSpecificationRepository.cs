﻿using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

namespace THP.Web.Models.DAL.Repositories
{
    public class UserCarSpecificationRepository : Repository<UserCarSpecification>, IUserCarSpecificationRepository
    {
        public readonly IDBContext _dbContext;

        public UserCarSpecificationRepository(IDBContext dbContext) :
            base(dbContext)
        {
            this._dbContext = dbContext;
        }
    }
}