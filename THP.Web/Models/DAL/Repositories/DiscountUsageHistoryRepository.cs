﻿using System.Collections.Generic;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

namespace THP.Web.Models.DAL.Repositories
{
    public class DiscountUsageHistoryRepository : Repository<DiscountUsageHistory>, IDiscountUsageHistoryRepository
    {
        private readonly IDBContext _dBContext;

        public DiscountUsageHistoryRepository(IDBContext dbContext) :
            base(dbContext)
        {
            this._dBContext = dbContext;
        }
        public DiscountUsageHistory GetDiscountUsageHistoryByBookingId(int? id)
        {
            return FirstOrDefault(m => m.ID == id.Value);
        }
        public IEnumerable<DiscountUsageHistory> GetDiscountUsageHistoryByDiscountId(int? id)
        {
            return Find(m => m.DiscountID == id.Value);
        }
    }
}