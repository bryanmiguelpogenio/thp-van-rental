﻿using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

using AdminBookingReview = THP.Web.Areas.Admin.Models.Booking.BookingReview;

namespace THP.Web.Models.DAL.Repositories
{
    public class BookingReviewRepository : Repository<BookingReview>, IBookingReviewRepository
    {
        private readonly IDBContext _dbContext;
        private readonly IUserRepository _userRepository;

        public BookingReviewRepository(IDBContext dbContext, IUserRepository userRepository) :
            base(dbContext)
        {
            this._dbContext = dbContext;
            this._userRepository = userRepository;
        }

        public void AddBookingReview(BookingReview model)
        {
            Add(model);
        }

        public AdminBookingReview GetBookingReviewByBookingId(int? id)
        {
            var bookingReview = FirstOrDefault(m => m.BookingID == id.Value);

            if (bookingReview != null)
            {
                return new AdminBookingReview
                {
                    BookingReviewID = bookingReview.ID,
                    Message = bookingReview.Message,
                    Rating = bookingReview.Rating
                };
            }
            else
            {
                return new AdminBookingReview();
            }
        }
    }
}