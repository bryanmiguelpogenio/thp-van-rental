﻿using System;
using System.Collections.Generic;
using System.Linq;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

using AdminDiscount = THP.Web.Areas.Admin.Models.Discount.Discount;

namespace THP.Web.Models.DAL.Repositories
{

    public class DiscountRepository : Repository<Discount>, IDiscountRepository
    {
        private readonly IDBContext _dBContext;
        private readonly IUserRepository _userRepository;

        public DiscountRepository(IDBContext dbContext, IUserRepository userRepository) :
            base(dbContext)
        {
            this._dBContext = dbContext;
            this._userRepository = userRepository;
        }

        public IEnumerable<AdminDiscount> GetDiscounts(string name = "", DateTime? startDate = null, DateTime? endDate = null, bool? isDeleted = null)
        {
            var discounts = GetAllAsQuery();

            if (isDeleted.HasValue)
                discounts = discounts.Where(m => m.Deleted == isDeleted.Value);

            if (!string.IsNullOrEmpty(name))
                discounts = discounts.Where(m => m.Name.Contains(name));

            var adminDiscount = discounts.Select(m => new AdminDiscount
            {
                DiscountId = m.ID,
                Name = m.Name,
                DiscountAmount = m.DiscountAmount,
                HasCouponCode = m.HasCouponCode,
                CouponCode = m.CouponCode,
                StartDate = m.StartDate,
                EndDate = m.EndDate,
                DateCreated = m.DateCreated,
                CreatedBy = m.CreatedBy.ToString(),
                DateUpdated = m.DateUpdated,
                UpdatedBy = m.UpdatedBy.ToString()
            }).ToList();

            foreach (var role in adminDiscount)
            {
                var value = 0;
                int.TryParse(role.CreatedBy, out value);
                var createdBy = _userRepository.Get(value);
                role.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                int.TryParse(role.UpdatedBy, out value);
                var updatedBy = _userRepository.Get(value);
                role.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;
            }

            return adminDiscount.AsEnumerable();
        }

        public AdminDiscount GetDiscount(int? id = null)
        {
            if (id.HasValue)
            {
                var discount = Get(id.Value);

                var adminDiscount = new AdminDiscount
                {
                    DiscountId = discount.ID,
                    Name = discount.Name,
                    DiscountAmount = discount.DiscountAmount,
                    HasCouponCode = discount.HasCouponCode,
                    CouponCode = discount.CouponCode,
                    StartDate = discount.StartDate,
                    EndDate = discount.EndDate,
                    DateCreated = discount.DateCreated,
                    DateUpdated = discount.DateUpdated
                };

                var createdBy = _userRepository.Get(discount.CreatedBy);
                adminDiscount.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                var updatedBy = _userRepository.Get(discount.UpdatedBy);
                adminDiscount.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;

                return adminDiscount;
            }
            else
            {
                return new AdminDiscount();
            }
        }

        public Discount GetDiscountByDiscountCode(string discountCode)
        {
            return FirstOrDefault(m => m.CouponCode == discountCode && !m.Deleted);
        }

        public void AddDiscount(AdminDiscount model)
        {
            var discount = new Discount
            {
                Name = model.Name,
                DiscountAmount = model.DiscountAmount,
                HasCouponCode = model.HasCouponCode,
                CouponCode = model.CouponCode,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                DateUpdated = DateTime.UtcNow,
                UpdatedBy = AccountConstant.UserID,
                DateCreated = DateTime.UtcNow,
                CreatedBy = AccountConstant.UserID
            };

            Add(discount);
        }

        public void UpdateDiscount(AdminDiscount model)
        {
            var discount = Get(model.DiscountId);

            discount.Name = model.Name;
            discount.DiscountAmount = model.DiscountAmount;
            discount.HasCouponCode = model.HasCouponCode;
            discount.CouponCode = model.CouponCode;
            discount.StartDate = model.StartDate;
            discount.EndDate = model.EndDate;
            discount.DateUpdated = DateTime.UtcNow;
            discount.UpdatedBy = AccountConstant.UserID;

            Update(discount);
        }
    }
}