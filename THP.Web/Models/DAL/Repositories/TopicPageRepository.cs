﻿using System;
using System.Collections.Generic;
using System.Linq;
using THP.Web.Areas.Admin.Models.TopicPage;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

namespace THP.Web.Models.DAL.Repositories
{
    public class TopicPageRepository : Repository<TopicPage>, ITopicPageRepository
    {
        public readonly IDBContext _dbContext;
        private readonly IUserRepository _userRepository;

        public TopicPageRepository(IDBContext dbContext, IUserRepository userRepository) :
            base(dbContext)
        {
            this._dbContext = dbContext;
            this._userRepository = userRepository;
        }

        public IEnumerable<Topic> GetTopicPages(string name = "", bool? isDeleted = null)
        {
            var topicPages = GetAllAsQuery();

            if (isDeleted.HasValue)
                topicPages = topicPages.Where(m => m.Deleted == isDeleted.Value);

            if (!string.IsNullOrEmpty(name))
                topicPages = topicPages.Where(m => m.Name.Contains(name));

            var topics = topicPages.Select(m => new Topic
            {
                TopicPageID = m.ID,
                Name = m.Name,
                UrlSlug = m.UrlSlug,
                Html = m.Html,
                Display = m.Display,
                DisplayOrder = m.DisplayOrder,
                DateCreated = m.DateCreated,
                CreatedBy = m.CreatedBy.ToString(),
                DateUpdated = m.DateUpdated,
                UpdatedBy = m.UpdatedBy.ToString()
            }).ToList();

            foreach (var topic in topics)
            {
                var value = 0;
                int.TryParse(topic.CreatedBy, out value);
                var createdBy = _userRepository.Get(value);
                topic.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                int.TryParse(topic.UpdatedBy, out value);
                var updatedBy = _userRepository.Get(value);
                topic.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;
            }

            return topics.AsEnumerable();
        }

        public Topic GetTopic(int? id = null)
        {
            if (id.HasValue)
            {
                var topicPage = Get(id.Value);

                var topic = new Topic
                {
                    TopicPageID = topicPage.ID,
                    Name = topicPage.Name,
                    UrlSlug = topicPage.UrlSlug,
                    Html = topicPage.Html,
                    Display = topicPage.Display,
                    DisplayOrder = topicPage.DisplayOrder,
                    DateCreated = topicPage.DateCreated,
                    DateUpdated = topicPage.DateUpdated,
                };

                var createdBy = _userRepository.Get(topicPage.CreatedBy);
                topic.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                var updatedBy = _userRepository.Get(topicPage.UpdatedBy);
                topic.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;

                return topic;
            }
            else
            {
                return new Topic();
            }
        }

        public void UpdateTopic(Topic model)
        {
            var topic = Get(model.TopicPageID);
            
            topic.Name = model.Name;
            topic.UrlSlug = model.UrlSlug;
            topic.Html = model.Html;
            topic.Display = model.Display;
            topic.DisplayOrder = model.DisplayOrder;
            topic.DateUpdated = DateTime.UtcNow;
            topic.UpdatedBy = AccountConstant.UserID;

            Update(topic);
        }

        public void AddTopic(Topic model)
        {
            var topicPage = new TopicPage
            {
                Name = model.Name,
                UrlSlug = model.UrlSlug,
                Html = model.Html,
                Display = model.Display,
                DisplayOrder = model.DisplayOrder,
                Deleted = false,
                DateUpdated = DateTime.UtcNow,
                UpdatedBy = AccountConstant.UserID,
                DateCreated = DateTime.UtcNow,
                CreatedBy = AccountConstant.UserID
            };

            Add(topicPage);
        }
    }
}