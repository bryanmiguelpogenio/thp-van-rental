﻿using System;
using System.Collections.Generic;
using System.Linq;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

using AdminPaymentType = THP.Web.Areas.Admin.Models.PaymentType.PaymentType;

namespace THP.Web.Models.DAL.Repositories
{
    public class PaymentTypeRepository : Repository<PaymentType>, IPaymentTypeRepository
    {
        public readonly IDBContext _dbContext;
        private readonly IUserRepository _userRepository;

        public PaymentTypeRepository(IDBContext dbContext, IUserRepository userRepository) :
            base(dbContext)
        {
            this._dbContext = dbContext;
            this._userRepository = userRepository;
        }

        public IEnumerable<AdminPaymentType> GetPaymentTypes(string name = "", bool? isDeleted = null)
        {
            var paymentTypes = GetAllAsQuery();

            if (isDeleted.HasValue)
                paymentTypes = paymentTypes.Where(m => m.Deleted == isDeleted.Value);

            if (!string.IsNullOrEmpty(name))
                paymentTypes = paymentTypes.Where(m => m.Name.Contains(name));

            var adminPaymentTypes = paymentTypes.Select(m => new AdminPaymentType
            {
                Name = m.Name,
                DateCreated = m.DateCreated,
                CreatedBy = m.CreatedBy.ToString(),
                DateUpdated = m.DateUpdated,
                UpdatedBy = m.UpdatedBy.ToString()
            }).ToList();

            foreach (var paymentType in adminPaymentTypes)
            {
                var value = 0;
                int.TryParse(paymentType.CreatedBy, out value);
                var createdBy = _userRepository.Get(value);
                paymentType.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                int.TryParse(paymentType.UpdatedBy, out value);
                var updatedBy = _userRepository.Get(value);
                paymentType.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;
            }

            return adminPaymentTypes.AsEnumerable();

        }

        public AdminPaymentType GetPaymentType(int? id = null)
        {
            if (id.HasValue)
            {
                var paymentType = Get(id.Value);

                AdminPaymentType payment = new AdminPaymentType
                {
                    PaymentTypeId = paymentType.ID,
                    Name = paymentType.Name,
                    Description = paymentType.Description,
                    DateCreated = paymentType.DateCreated,
                    DateUpdated = paymentType.DateUpdated
                };

                var createdBy = _userRepository.Get(paymentType.CreatedBy);
                payment.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                var updatedBy = _userRepository.Get(paymentType.UpdatedBy);
                payment.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;

                return payment;
            }
            else
            {
                return new AdminPaymentType();
            }
        }

        public void UpdatePaymentType(AdminPaymentType model)
        {
            var paymentType = Get(model.PaymentTypeId);

            paymentType.Name = model.Name;
            paymentType.Description = model.Description;
            paymentType.DateUpdated = DateTime.UtcNow;
            paymentType.UpdatedBy = AccountConstant.UserID;

            Update(paymentType);
        }

        public void AddPaymentType(AdminPaymentType model)
        {
            var paymentType = new PaymentType
            {
                Name = model.Name,
                Description = model.Description,
                DateUpdated = DateTime.UtcNow,
                UpdatedBy = AccountConstant.UserID,
                DateCreated = DateTime.UtcNow,
                CreatedBy = AccountConstant.UserID,
            };

            Add(paymentType);
        }
    }
}