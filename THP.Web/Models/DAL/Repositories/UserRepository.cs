﻿using System;
using System.Collections.Generic;
using System.Linq;
using THP.Web.Areas.Admin.Models.Customer;
using THP.Web.Models.DAL.Data.Accounts;
using THP.Web.Models.DAL.Data.Enum;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;
using THP.Web.Models.Interface.Security;
using THP.Web.Models.Security;

namespace THP.Web.Models.DAL.Repositories
{

    public class UserRepository : Repository<User>, IUserRepository
    {
        public readonly IDBContext _dbContext;
        public readonly IEncryptionService _encryptionService;
        public readonly IRoleRepository _roleRepository;
        private readonly IUserDetailRepository _userDetailRepository;

        public UserRepository(IDBContext dbContext,
            IEncryptionService encryptionService,
            IRoleRepository roleRepository,
            IUserDetailRepository userDetailRepository) :
            base(dbContext)
        {
            this._dbContext = dbContext;
            this._encryptionService = encryptionService;
            this._roleRepository = roleRepository;
            this._userDetailRepository = userDetailRepository;
        }

        public Account GetUser(string username)
        {
            Account model = new Account();

            var user = Get(m => m.Username == username);

            if (user != null)
            {
                model.Username = user.Username;
                model.Password = user.Password;
                model.Roles = user.Role.Name;
            }

            return model;
        }

        public User CreateUser(AccountRegisterViewModel model)
        {
            string saltKey = _encryptionService.CreateSaltKey(5);
            //var role = _roleRepository.Get(m => m.Name == Constant.CUSTOMER_ROLE);


            var user = new User
            {
                Firstname = model.Firstname,
                Middlename = model.Middlename,
                Lastname = model.Lastname,
                EmailAddress = model.EmailAddress,
                Username = model.Username,
                Password = _encryptionService.CreatePasswordHash(model.Password, saltKey), // Encrypt password
                PasswordSaltKey = saltKey, // input saltkey being used
                RoleID = model.RoleID,
                Verified = false,
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow
            };

            Add(user);

            return user;
        }

        public void VerifyUser(string username)
        {
            var user = Get(m => m.Username == username);
            user.Verified = true;
            Update(user);
        }

        public Customer GetCustomer(int? id = null)
        {
            if (id.HasValue)
            {
                var user = Get(id.Value);

                var customer = new Customer{

                    CustomerId = user.ID,
                    Firstname = user.Firstname,
                    Middlename = user.Middlename,
                    Lastname = user.Lastname,
                    Username = user.Username,
                    EmailAddress = user.EmailAddress,

                    Role = user.Role,
                    RoleID = user.Role != null ? user.Role.ID : 0,
                    DateCreated = user.DateCreated,
                    DateUpdated = user.DateUpdated
                };

                var userDetail = _userDetailRepository.FirstOrDefault(m => m.UserID == user.ID);

                if (userDetail != null)
                {
                    customer.CustomerDetailId = userDetail.ID;
                    customer.Birthdate = userDetail.Birthdate;
                    customer.CellphoneNo = userDetail.CellphoneNo;
                    customer.TelephoneNo = userDetail.TelephoneNo;
                    customer.Address = userDetail.Address;
                    customer.Gender = userDetail.Gender;
                    customer.Municipality = userDetail.Municipality;
                    customer.City = userDetail.City;
                    customer.Province = userDetail.Province;
                    customer.Region = userDetail.Region;
                }
                
                var createdBy = Get(user.CreatedBy);
                customer.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;
                
                var updatedBy = Get(user.UpdatedBy);
                customer.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;

                return customer;
            }
            else
            {
                return new Customer();
            }
        }

        public IEnumerable<Customer> GetCustomers(string fullName = "", string emailAddress = "", int? roleId = null, 
            DateTime? startDateTimeCreated = null, DateTime? endDateTimeCreated = null, 
            DateTime? startdateTimeUpdated = null, DateTime? enddateTimeUpdated = null, bool? isDeleted = null)
        {
            var users = GetAllAsQuery();

            if (isDeleted.HasValue)
                users = users.Where(m => m.Deleted == isDeleted.Value);

            if (!string.IsNullOrEmpty(fullName))
                users = users.Where(m => m.Firstname.Contains(fullName));

            if (!string.IsNullOrEmpty(emailAddress))
                users = users.Where(m => m.EmailAddress.Contains(emailAddress));

            if (roleId.HasValue && roleId.Value != 0)
                users = users.Where(m => m.RoleID == roleId.Value);

            var startDateTime = new DateTime();
            var endDateTime = new DateTime();

            if (startDateTimeCreated.HasValue && endDateTimeCreated.HasValue)
            {
                startDateTime = startDateTimeCreated.Value;
                endDateTime = endDateTimeCreated.Value.AddHours(23).AddMinutes(59);

                users.Where(m => m.DateCreated >= startDateTime && m.DateCreated <= endDateTime);
            }

            if (startdateTimeUpdated.HasValue && enddateTimeUpdated.HasValue)
            {
                startDateTime = startDateTimeCreated.Value;
                endDateTime = endDateTimeCreated.Value.AddHours(23).AddMinutes(59);

                users.Where(m => m.DateUpdated >= startDateTime && m.DateUpdated <= endDateTime);
            }

            var customers = new List<Customer>();

            Find(users)
                .ToList()
                .ForEach(m => customers.Add(new Customer
                {
                    CustomerId = m.ID,
                    Firstname = m.Firstname,
                    Middlename = m.Middlename,
                    Lastname = m.Lastname,
                    Username = m.Username,
                    EmailAddress = m.EmailAddress,

                    Role = m.Role,
                    RoleID = m.Role != null ? m.Role.ID : 0,
                    CreatedBy = m.CreatedBy.ToString(),
                    DateCreated = m.DateCreated,
                    UpdatedBy = m.UpdatedBy.ToString(),
                    DateUpdated = m.DateUpdated

                }));

            foreach(var customer in customers)
            {
                var userDetail = _userDetailRepository.FirstOrDefault(m => m.UserID == customer.CustomerId);

                if (userDetail != null)
                {
                    customer.CustomerDetailId = userDetail.ID;
                    customer.Birthdate = userDetail.Birthdate;
                    customer.CellphoneNo = userDetail.CellphoneNo;
                    customer.TelephoneNo = userDetail.TelephoneNo;
                    customer.Address = userDetail.Address;
                    customer.Gender = userDetail.Gender;
                    customer.Municipality = userDetail.Municipality;
                    customer.City = userDetail.City;
                    customer.Province = userDetail.Province;
                    customer.Region = userDetail.Region;
                }

                var value = 0;
                int.TryParse(customer.CreatedBy, out value);
                var createdBy = Get(value);
                customer.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                int.TryParse(customer.UpdatedBy, out value);
                var updatedBy = Get(value);
                customer.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;
            }

            return customers;
        }

        public void UpdateCustomer(Customer model)
        {
            var user = Get(model.CustomerId);

            user.Firstname = model.Firstname;
            user.Middlename = model.Middlename;
            user.Lastname = model.Lastname;
            user.EmailAddress = model.EmailAddress;
            user.RoleID = model.RoleID;
            user.Username = model.Username;

            user.DateUpdated = DateTime.UtcNow;
            user.UpdatedBy = AccountConstant.UserID;

            Update(user);

            var userDetail = _userDetailRepository.Get(model.CustomerDetailId);

            if (userDetail != null)
            {
                // Update data if record exists

                userDetail.Birthdate = model.Birthdate;
                userDetail.CellphoneNo = model.CellphoneNo;
                userDetail.TelephoneNo = model.TelephoneNo;
                userDetail.Address = model.Address;
                userDetail.Gender = model.Gender;
                userDetail.Municipality = model.Municipality;
                userDetail.City = model.City;
                userDetail.Province = model.Province;
                userDetail.Region = model.Region;

                userDetail.DateUpdated = DateTime.UtcNow;
                userDetail.UpdatedBy = AccountConstant.UserID;

                _userDetailRepository.Update(userDetail);
            }
            else
            {
                // Create a new record if doesn't exists

                userDetail = new UserDetail();

                userDetail.UserID = model.CustomerId;

                userDetail.Birthdate = model.Birthdate;
                userDetail.Gender = model.Gender;

                userDetail.CellphoneNo = model.CellphoneNo;
                userDetail.TelephoneNo = model.TelephoneNo;
                userDetail.Address = model.Address;
                userDetail.City = model.City;
                userDetail.Municipality = model.Municipality;
                userDetail.Province = model.Province;
                userDetail.Region = model.Region;
                userDetail.Deleted = false;

                var modifier = AccountConstant.UserID;

                userDetail.CreatedBy = modifier;
                userDetail.DateCreated = DateTime.UtcNow;
                userDetail.UpdatedBy = modifier;
                userDetail.DateUpdated = DateTime.UtcNow;

                _userDetailRepository.Add(userDetail);

            }
        }

        public void AddCustomer(Customer model)
        {
            string saltKey = _encryptionService.CreateSaltKey(5);

            var user = new User
            {
                Firstname = model.Firstname,
                Middlename = model.Middlename,
                Lastname = model.Lastname,
                EmailAddress = model.EmailAddress,
                Username = model.Username,
                Password = _encryptionService.CreatePasswordHash(model.Password, saltKey), // Encrypt password
                PasswordSaltKey = saltKey, // input saltkey being used
                RoleID = model.RoleID,
                Verified = true,
                DateCreated = DateTime.UtcNow,
                CreatedBy = AccountConstant.UserID,
                DateUpdated = DateTime.UtcNow,
                UpdatedBy = AccountConstant.UserID
            };

            Add(user);

            var userDetail = new UserDetail
            {
                Birthdate = model.Birthdate,
                Address = model.Address,
                City = model.City,
                Municipality = model.Municipality,
                Province = model.Province,
                Region = model.Region,
                CellphoneNo = model.CellphoneNo,
                TelephoneNo = model.TelephoneNo,
                Gender = model.Gender,
                Deleted = false,
                DateCreated = DateTime.UtcNow,
                CreatedBy = AccountConstant.UserID,
                DateUpdated = DateTime.UtcNow,
                UpdatedBy = AccountConstant.UserID
            };

            _userDetailRepository.Add(userDetail);
        }

        public IEnumerable<User> GetDrivers(bool? isDeleted = null)
        {
            var users = GetAllAsQuery();

            if (isDeleted.HasValue)
                users = users.Where(m => m.Deleted == isDeleted.Value);

            return users.Where(m => m.RoleID == (int)RoleType.Driver).AsEnumerable();
        }
    }
}