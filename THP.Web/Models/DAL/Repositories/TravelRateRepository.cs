﻿using System;
using System.Collections.Generic;
using System.Linq;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

using AdminTravelRate = THP.Web.Areas.Admin.Models.TravelRate.TravelRate;

namespace THP.Web.Models.DAL.Repositories
{
    public class TravelRateRepository : Repository<TravelRate>, ITravelRateRepository
    {
        public readonly IDBContext _dbContext;
        private readonly IUserRepository _userRepository;

        public TravelRateRepository(IDBContext dbContext, IUserRepository userRepository) :
            base(dbContext)
        {
            this._dbContext = dbContext;
            this._userRepository = userRepository;
        }

        public IEnumerable<AdminTravelRate> GetTravelRates(int? fromId = null, int? toId = null, bool? isDeleted = null)
        {
            var travelRates = GetAllAsQuery();

            if (isDeleted.HasValue)
                travelRates = travelRates.Where(m => m.Deleted == isDeleted);

            if (fromId.HasValue && fromId.Value != 0)
                travelRates = travelRates.Where(m => m.PickupPointID == fromId.Value);

            if (toId.HasValue && toId.Value != 0)
                travelRates = travelRates.Where(m => m.DestinationPointID == toId.Value);

            var adminTravelRates = travelRates.Select(m => new AdminTravelRate {
                TravelRateId = m.ID,
                DestinationFrom = m.PickupPoint != null ? m.PickupPoint.Name : string.Empty,
                DestinationTo = m.DestinationPoint != null ? m.DestinationPoint.Name : string.Empty,
                FixedPrice = m.FixedPrice,
                Overnight = m.Overnight,
                Overtime = m.Overtime,
                MaxHours = m.MaxHours,
                DateUpdated = m.DateUpdated,
                DateCreated = m.DateCreated
            }).ToList();

            foreach (var travelRate in adminTravelRates)
            {
                var value = 0;
                int.TryParse(travelRate.CreatedBy, out value);
                var createdBy = _dbContext.Set<User>().FirstOrDefault(m => m.ID == value);
                travelRate.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                int.TryParse(travelRate.UpdatedBy, out value);
                var updatedBy = _dbContext.Set<User>().FirstOrDefault(m => m.ID == value);
                travelRate.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;
            }

            return adminTravelRates.AsEnumerable();
        }

        public AdminTravelRate GetTravelRate(int? id = null)
        {
            if (id.HasValue)
            {
                var travelRate = Get(id.Value);

                var adminTravelRate = new AdminTravelRate
                {
                    TravelRateId = travelRate.ID,
                    PickupPointID = travelRate.PickupPointID,
                    DestinationFrom = travelRate.PickupPoint != null ? travelRate.PickupPoint.Name : string.Empty,
                    DestinationPointID = travelRate.DestinationPointID,
                    DestinationTo = travelRate.DestinationPoint != null ? travelRate.DestinationPoint.Name : string.Empty,
                    FixedPrice = travelRate.FixedPrice,
                    MaxHours = travelRate.MaxHours,
                    Overnight = travelRate.Overnight,
                    Overtime = travelRate.Overtime,
                    DateCreated = travelRate.DateCreated,
                    DateUpdated = travelRate.DateUpdated
                };

                var createdBy = _userRepository.Get(travelRate.CreatedBy);
                adminTravelRate.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                var updatedBy = _userRepository.Get(travelRate.UpdatedBy);
                adminTravelRate.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;

                return adminTravelRate;
            }
            else
            {
                return new AdminTravelRate();
            }
        }

        public void UpdateTravelRate(AdminTravelRate model)
        {
            var travelRate = Get(model.TravelRateId);

            travelRate.PickupPointID = model.PickupPointID;
            travelRate.DestinationPointID = model.DestinationPointID;
            travelRate.FixedPrice = model.FixedPrice;
            travelRate.MaxHours = model.MaxHours;
            travelRate.Overnight = model.Overnight;
            travelRate.Overtime = model.Overtime;            
            travelRate.DateUpdated = DateTime.UtcNow;
            travelRate.UpdatedBy = AccountConstant.UserID;

            Update(travelRate);
        }

        public void AddTravelRate(AdminTravelRate model)
        {
            var travelRate = new TravelRate
            {
                PickupPointID = model.PickupPointID,
                DestinationPointID = model.DestinationPointID,
                FixedPrice = model.FixedPrice,
                MaxHours = model.MaxHours,
                Overnight = model.Overnight,
                Overtime = model.Overtime,
                DateCreated = DateTime.UtcNow,
                CreatedBy = AccountConstant.UserID,
                DateUpdated = DateTime.UtcNow,
                UpdatedBy = AccountConstant.UserID
            };

            Add(travelRate);
        }
    }
}