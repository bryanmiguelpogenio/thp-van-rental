﻿using System;
using System.Collections.Generic;
using System.Linq;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

using AdminRole = THP.Web.Areas.Admin.Models.Role.Role;

namespace THP.Web.Models.DAL.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public readonly IDBContext _dbContext;

        public RoleRepository(IDBContext dbContext) :
            base(dbContext)
        {
            this._dbContext = dbContext;
        }

        public IEnumerable<AdminRole> GetRoles(string name = "", bool? isDeleted = null)
        {
            var roles = GetAllAsQuery();

            if (isDeleted.HasValue)
                roles = roles.Where(m => m.Deleted == isDeleted.Value);

            if (!string.IsNullOrEmpty(name))
                roles = roles.Where(m => m.Name.Contains(name));

            var adminRoles = roles.Select(m => new AdminRole
            {
                RoleId = m.ID,
                Name = m.Name,
                Description = m.Description,
                DateCreated = m.DateCreated,
                CreatedBy = m.CreatedBy.ToString(),
                DateUpdated = m.DateUpdated,
                UpdatedBy = m.UpdatedBy.ToString()
            }).ToList();

            foreach (var role in adminRoles)
            {
                var value = 0;
                int.TryParse(role.CreatedBy, out value);
                var createdBy = _dbContext.Set<User>().FirstOrDefault(m => m.ID == value);
                role.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                int.TryParse(role.UpdatedBy, out value);
                var updatedBy = _dbContext.Set<User>().FirstOrDefault(m => m.ID == value);
                role.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;
            }

            return adminRoles.AsEnumerable();

        }

        public AdminRole GetRole(int? id = null)
        {
            if (id.HasValue)
            {
                var role = Get(id.Value);

                AdminRole adminRole = new AdminRole
                {
                    RoleId = role.ID,
                    Name = role.Name,
                    Description = role.Description,
                    DateCreated = role.DateCreated,
                    DateUpdated = role.DateUpdated
                };

                var createdBy = _dbContext.Set<User>().FirstOrDefault(m => m.ID == role.CreatedBy);
                adminRole.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                var updatedBy = _dbContext.Set<User>().FirstOrDefault(m => m.ID == role.UpdatedBy);
                adminRole.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;

                return adminRole;
            }
            else
            {
                return new AdminRole();
            }
        }

        public void UpdateRole(AdminRole model)
        {
            var role = Get(model.RoleId);

            role.Name = model.Name;
            role.Description = model.Description;
            role.DateUpdated = DateTime.UtcNow;
            role.UpdatedBy = AccountConstant.UserID;

            Update(role);
        }

        public void AddRole(AdminRole model)
        {
            var role = new Role
            {
                Name = model.Name,
                Description = model.Description,
                DateUpdated = DateTime.UtcNow,
                UpdatedBy = AccountConstant.UserID,
                DateCreated = DateTime.UtcNow,
                CreatedBy = AccountConstant.UserID
            };

            Add(role);
        }
    }
}