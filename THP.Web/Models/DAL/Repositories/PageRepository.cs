﻿using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

namespace THP.Web.Models.DAL.Repositories
{
    public class PageRepository : Repository<Page>, IPageRepository
    {
        public readonly IDBContext _dbContext;

        public PageRepository(IDBContext dbContext) :
            base(dbContext)
        {
            this._dbContext = dbContext;

        }
    }
}