﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using THP.Web.Areas.Admin.Models.Destination;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

namespace THP.Web.Models.DAL.Repositories
{
    public class LocationRepository : Repository<Location>, ILocationRepository
    {
        public readonly IDBContext _dbContext;
        private readonly IUserRepository _userRepository;

        public LocationRepository(IDBContext dbContext, IUserRepository userRepository) :
            base(dbContext)
        {
            this._dbContext = dbContext;
            this._userRepository = userRepository;
        }

        public IEnumerable<Destination> GetDestinations(string name = "", int? provinceId = null, bool? isDeleted = null)
        {
            var locations = GetAllAsQuery();

            if (isDeleted.HasValue)
                locations = locations.Where(m => m.Deleted == isDeleted.Value);

            if (!string.IsNullOrEmpty(name))
                locations = locations.Where(m => m.Name.Contains(name));

            if (provinceId.HasValue && provinceId.Value != 0)
                locations = locations.Where(m => m.ParentLocationID == provinceId.Value);

            var destinations = locations.Select(m => new Destination
            {
                LocationID = m.ID,
                Name = m.Name,
                ParentLocationID = m.ParentLocationID,
                ProvinceState = m.ParentLocation.Name,
                Description = m.Description,
                DateCreated = m.DateCreated,
                DateUpdated = m.DateUpdated
            }).ToList();

            foreach (var destination in destinations)
            {
                var value = 0;
                int.TryParse(destination.CreatedBy, out value);
                var createdBy = _userRepository.Get(value);
                destination.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                int.TryParse(destination.UpdatedBy, out value);
                var updatedBy = _userRepository.Get(value);
                destination.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;
            }

            return destinations.AsEnumerable();

        }

        public Destination GetDestination(int? Id = null)
        {
            if (Id.HasValue)
            {
                var location = Get(Id.Value);

                var destination = new Destination
                {
                    LocationID = location.ID,
                    Name = location.Name,
                    ParentLocationID = location.ParentLocationID,
                    Description = location.Description,
                    DateUpdated = location.DateUpdated,
                    DateCreated = location.DateCreated,
                };

                var createdBy = _userRepository.Get(location.CreatedBy);
                destination.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                var updatedBy = _userRepository.Get(location.UpdatedBy);
                destination.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;

                return destination;
            }
            else
            {
                return new Destination();
            }
        }

        public void UpdateDestination(Destination model)
        {
            var location = Get(model.LocationID);

            location.Name = model.Name;
            location.ParentLocationID = model.ParentLocationID;
            location.Description = model.Description;
            location.DateUpdated = DateTime.UtcNow;
            location.UpdatedBy = AccountConstant.UserID;

            Update(location);
        }

        public void AddDestination(Destination model)
        {
            var location = new Location
            {
                Name = model.Name,
                ParentLocationID = model.ParentLocationID,
                Description = model.Description,
                Deleted = false,
                DateCreated = DateTime.UtcNow,
                CreatedBy = AccountConstant.UserID,
                DateUpdated = DateTime.UtcNow,
                UpdatedBy = AccountConstant.UserID
            };

            Add(location);
        }
    }
}