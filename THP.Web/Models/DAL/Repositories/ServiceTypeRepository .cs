﻿using System;
using System.Collections.Generic;
using System.Linq;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

using AdminServiceType = THP.Web.Areas.Admin.Models.ServiceType.ServiceType;

namespace THP.Web.Models.DAL.Repositories
{
    public class ServiceTypeRepository : Repository<ServiceType>, IServiceTypeRepository
    {
        public readonly IDBContext _dbContext;
        private readonly IUserRepository _userRepository;

        public ServiceTypeRepository(IDBContext dbContext, IUserRepository userRepository) :
            base(dbContext)
        {
            this._dbContext = dbContext;
            this._userRepository = userRepository;
        }

        public IEnumerable<AdminServiceType> GetServiceType(string name = "", bool? isDeleted = null)
        {
            var serviceTypes = GetAllAsQuery();

            if (isDeleted.HasValue)
                serviceTypes = serviceTypes.Where(m => m.Deleted == isDeleted.Value);

            if (!string.IsNullOrEmpty(name))
                serviceTypes = serviceTypes.Where(m => m.Name.Contains(name));

            var adminServiceTypes = serviceTypes.Select(m => new AdminServiceType
            {
                ServiceTypeId = m.ID,
                Name = m.Name,
                Description = m.Description,
                DateCreated = m.DateCreated,
                DateUpdated = m.DateUpdated
            }).ToList();

            foreach (var serviceType in adminServiceTypes)
            {
                var value = 0;
                int.TryParse(serviceType.CreatedBy, out value);
                var createdBy = _userRepository.Get(value);
                serviceType.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                int.TryParse(serviceType.UpdatedBy, out value);
                var updatedBy = _userRepository.Get(value);
                serviceType.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;
            }

            return adminServiceTypes.AsEnumerable();

        }

        public AdminServiceType GetServiceType(int? id = null)
        {
            if (id.HasValue)
            {
                var serviceType = Get(id.Value);

                var adminServiceType = new AdminServiceType
                {
                    ServiceTypeId = serviceType.ID,
                    Name = serviceType.Name,
                    Description = serviceType.Description,
                    DateCreated = serviceType.DateCreated,
                    DateUpdated = serviceType.DateUpdated
                };

                var createdBy = _userRepository.Get(serviceType.CreatedBy);
                adminServiceType.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                var updatedBy = _userRepository.Get(serviceType.UpdatedBy);
                adminServiceType.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;

                return adminServiceType;
            }
            else
            {
                return new AdminServiceType();
            }
        }

        public void UpdateServiceType(AdminServiceType model)
        {
            var serviceType = Get(model.ServiceTypeId);

            serviceType.Name = model.Name;
            serviceType.Description = model.Description;
            serviceType.DateUpdated = DateTime.UtcNow;
            serviceType.UpdatedBy = AccountConstant.UserID;

            Update(serviceType);
        }

        public void AddServiceType(AdminServiceType model)
        {
            var serviceType = new ServiceType
            {
                Name = model.Name,
                Description = model.Description,
                DateUpdated = DateTime.UtcNow,
                UpdatedBy = AccountConstant.UserID,
                DateCreated = DateTime.UtcNow,
                CreatedBy = AccountConstant.UserID
            };

            Add(serviceType);
        }
    }
}