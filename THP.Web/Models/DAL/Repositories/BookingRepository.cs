﻿using System;
using System.Collections.Generic;
using System.Linq;
using THP.Web.Models.DAL.Data.Booking;
using THP.Web.Models.DAL.Data.Home;
using THP.Web.Models.DAL.Data.Order;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

using AdminBookingViewModel = THP.Web.Areas.Admin.Models.Booking.BookingViewModel;
using AdminBooking = THP.Web.Areas.Admin.Models.Booking.Booking;
using AdminBookingReview = THP.Web.Areas.Admin.Models.Booking.BookingReview;
using System.Text.RegularExpressions;

namespace THP.Web.Models.DAL.Repositories
{
    public class BookingRepository : Repository<Booking>, IBookingRepository
    {
        public readonly IDBContext _dbContext;
        private readonly IUserRepository _userRepository;
        private readonly ITravelRateRepository _travelRateRepository;
        private readonly IBookingReviewRepository _bookingReviewRepository;
        private readonly IDiscountRepository _discountRepository;
        private readonly IDiscountUsageHistoryRepository _discountUsageHistoryRepository;
        private readonly IUserCarRepository _userCarRepository;
        private readonly INotificationRepository _notificationRepository;

        public BookingRepository(IDBContext dbContext, 
            IUserRepository userRepository, 
            ITravelRateRepository travelRateRepository,
            IBookingReviewRepository bookingReviewRepository,
            IDiscountRepository discountRepository,
            IDiscountUsageHistoryRepository discountUsageHistoryRepository,
            IUserCarRepository userCarRepository,
            INotificationRepository notificationRepository) :
            base(dbContext)
        {
            this._dbContext = dbContext;
            this._userRepository = userRepository;
            this._travelRateRepository = travelRateRepository;
            this._bookingReviewRepository = bookingReviewRepository;
            this._discountRepository = discountRepository;
            this._discountUsageHistoryRepository = discountUsageHistoryRepository;
            this._userCarRepository = userCarRepository;
            this._notificationRepository = notificationRepository;
        }

        // Mainpage Booking
        public void CreateBooking(BookingViewModel model)
        {

            var booking = new Booking();

            var userID = AccountConstant.UserID;

            booking.BookingNo = CreateBookingNo();
            booking.UserID = userID;
            booking.Seater = model.Seater;
            booking.StartDateScheduled = model.StartDateScheduled.Value;
            booking.EndDateScheduled = model.EndDateScheduled.Value;

            booking.DriverUserID = GetAvailableDriver(booking.StartDateScheduled, booking.EndDateScheduled);
            booking.PaymetTypeID = 1;

            var travelRate = _dbContext.Set<TravelRate>()
                .FirstOrDefault(m => m.PickupPointID == model.PickUpPointID & m.DestinationPointID == model.DestinationPointID);
            var discount = _discountRepository.Get(model.DiscountId);

            var totalPrice = ComputeTotalPrice(travelRate.FixedPrice.Value,
                booking.StartDateScheduled,
                booking.EndDateScheduled,
                (discount != null) ? discount.DiscountAmount : 0);

            booking.TransactionID = model.OrderId;
            booking.TravelRateID = travelRate.ID;
            booking.TotalPrice = totalPrice;
            booking.Currency = Constant.DEFAULT_CURRENCY;

            booking.Status = Constant.BOOKING_STATUS_QUEUED;

            decimal kmValue = 0;
            decimal.TryParse(Regex.Replace(model.Distance.ToLower(), "km", string.Empty).Trim(), out kmValue);
            booking.Kilometer = kmValue;

            booking.CreatedBy = userID;
            booking.DateCreated = DateTime.UtcNow;
            booking.UpdatedBy = userID;
            booking.DateUpdated = DateTime.UtcNow;

            Add(booking);

            if (model.DiscountId != 0)
            {
                var discountUsageHistory = new DiscountUsageHistory
                {
                    BookingID = booking.ID,
                    DiscountID = model.DiscountId,
                    DateCreated = DateTime.UtcNow,
                    CreatedBy = userID
                };

                _discountUsageHistoryRepository.Add(discountUsageHistory);
            }

            // Create a notifcation for customer
            var notification = new Notification
            {
                UserID = userID,
                Message = $"You've created a booking ({booking.BookingNo}) between dates {booking.StartDateScheduled} and {booking.EndDateScheduled}",
                IsClicked = true,
                CreatedBy = userID,
                DateCreated = DateTime.UtcNow,
                UpdatedBy = userID,
                DateUpdated = DateTime.UtcNow
            };

            _notificationRepository.Add(notification);

            notification = new Notification
            {
                UserID = booking.DriverUserID,
                Message = $"You've been booked ({booking.BookingNo}) between dates {booking.StartDateScheduled} and {booking.EndDateScheduled}",
                IsClicked = false,
                CreatedBy = userID,
                DateCreated = DateTime.UtcNow,
                UpdatedBy = userID,
                DateUpdated = DateTime.UtcNow
            };

            _notificationRepository.Add(notification);
        }

        // Homepage Booking
        public void CreateBooking(HomeQuickBookingViewModel model)
        {
            var booking = new Booking();

            var userID = AccountConstant.UserID;

            booking.BookingNo = CreateBookingNo();
            booking.UserID = userID;
            booking.Seater = model.Seater;
            booking.StartDateScheduled = model.StartDateScheduled.Value;
            booking.EndDateScheduled = model.EndDateScheduled.Value;

            booking.DriverUserID = GetAvailableDriver(booking.StartDateScheduled, booking.EndDateScheduled);
            booking.PaymetTypeID = 1;

            var travelRate = _dbContext.Set<TravelRate>()
                .FirstOrDefault(m => m.PickupPointID == model.originLocationID & m.DestinationPointID == model.destinationLocationID);
            var discount = _discountRepository.Get(model.DiscountId);

            var totalPrice = ComputeTotalPrice(travelRate.FixedPrice.Value,
                booking.StartDateScheduled,
                booking.EndDateScheduled,
                (discount != null) ? discount.DiscountAmount : 0);

            booking.TransactionID = model.OrderId;
            booking.TravelRateID = travelRate.ID;
            booking.TotalPrice = totalPrice;
            booking.Currency = Constant.DEFAULT_CURRENCY;

            booking.Status = Constant.BOOKING_STATUS_QUEUED;

            decimal kmValue = 0;
            decimal.TryParse(Regex.Replace(model.Distance.ToLower(), "km", string.Empty).Trim(), out kmValue);
            booking.Kilometer = kmValue;

            booking.CreatedBy = userID;
            booking.DateCreated = DateTime.UtcNow;
            booking.UpdatedBy = userID;
            booking.DateUpdated = DateTime.UtcNow;

            Add(booking);

            if (model.DiscountId != 0)
            {
                var discountUsageHistory = new DiscountUsageHistory
                {
                    BookingID = booking.ID,
                    DiscountID = model.DiscountId,
                    DateCreated = DateTime.UtcNow,
                    CreatedBy = userID
                };

                _discountUsageHistoryRepository.Add(discountUsageHistory);
            }

            // Create a notifcation for customer
            var notification = new Notification
            {
                UserID = userID,
                Message = $"You've created a booking ({booking.BookingNo}) between dates {booking.StartDateScheduled} and {booking.EndDateScheduled}",
                IsClicked = true,
                CreatedBy = userID,
                DateCreated = DateTime.UtcNow,
                UpdatedBy = userID,
                DateUpdated = DateTime.UtcNow
            };

            _notificationRepository.Add(notification);

            notification = new Notification
            {
                UserID = booking.DriverUserID,
                Message = $"You've been booked ({booking.BookingNo}) between dates {booking.StartDateScheduled} and {booking.EndDateScheduled}",
                IsClicked = false,
                CreatedBy = userID,
                DateCreated = DateTime.UtcNow,
                UpdatedBy = userID,
                DateUpdated = DateTime.UtcNow
            };

            _notificationRepository.Add(notification);
        }

        public string CreateBookingNo()
        {
            var value = "BOOK";
            var year = DateTime.Now.Year
                .ToString()
                .Substring(DateTime.Now.Year
                .ToString().Length - 2);

            var bookings = _dbContext.Set<Booking>()
                .Where(m => m.BookingNo.Contains(value) & m.BookingNo.Substring(m.BookingNo.Length - 2) == year)
                .AsEnumerable();

            var number = "0";

            var bookingCount = (bookings.Count() + 1).ToString();
            if (bookingCount.Length == 4)
            {
                number = bookingCount;
            }
            else if (bookingCount.Length == 3)
            {
                number = $"0{bookingCount}";
            }
            else if (bookingCount.Length == 2)
            {
                number = $"00{bookingCount}";

            }
            else
            {
                number = $"000{bookingCount}";
            }

            return $"BOOK{number}{year}";
        }

        public decimal ComputeTotalPrice(decimal? price, DateTime? startDate, DateTime? endDate, decimal? discountAmount = null)
        {
            var totalDays = (startDate.HasValue && endDate.HasValue) ? 
                            (endDate.Value - startDate.Value).TotalDays + 1 : 0;
            totalDays = Math.Round(totalDays, 2);

            if (!discountAmount.HasValue)
                discountAmount = 0;

            var totalPrice = ((price.HasValue ? price.Value : Convert.ToDecimal(0.00)) * Convert.ToDecimal(totalDays)) - discountAmount.Value;
            return Math.Round(totalPrice, 2);
        }

        public int GetAvailableDriver(DateTime? startDate, DateTime? endDate)
        {
            // Get days that customer wants to book.
            int days = (endDate.Value.Date - startDate.Value.Date).Days + 1;
            var daysBooked = Enumerable.Range(0, days).Select(i => startDate.Value.AddDays(i).Date);
            //var dayOfWeeks = daysBooked.Select(m => m.DayOfWeek).Distinct().ToList();

            var dayOfWeeks = new List<DayOfWeek>();
            dayOfWeeks.Add(startDate.Value.DayOfWeek);
            dayOfWeeks.Add(endDate.Value.DayOfWeek);

            var dateToday = DateTime.UtcNow.ToLocalTime().Date;
            var bookings = Find(m => m.StartDateScheduled >= dateToday)
                .ToList();

            // Get days that are affected for the schedules that have been booked
            var reservedSchedules = GetReservedSchedules(bookings)
                .Where(m => daysBooked.Contains(m.ReservedDate));

            // Get all drivers
            var userIds = _userRepository.GetDrivers(isDeleted: false)
                .Select(m => m.ID)
                .ToList();

            var userAvailable = new List<int>();
            foreach (var userId in userIds)
            {
                // check if the driver already has a booking between the selected start and enddate for booking.
                var reservedSchedule = reservedSchedules.FirstOrDefault(m => m.DriverIds.Contains(userId));


                // check if the car of the driver on the selected dates are coding.
                var plateNumbers = GetDriverCoding(userId);
                var isDriverAvailable = true;

                if (plateNumbers.Count == 0)
                {
                    isDriverAvailable = false;
                }
                else
                {
                    foreach (var plateNumber in plateNumbers)
                    {
                        if (dayOfWeeks.Contains(plateNumber))
                        {
                            isDriverAvailable = false;
                        }

                    }
                }

                // Add driver to the list of available drivers for further booking process
                if (reservedSchedule == null && isDriverAvailable)
                    userAvailable.Add(userId);
            }

            // Compare total bookings of each driver for equal booking.
            var driverBookings = new Dictionary<int, int>();

            var keyValuePairs = Find(m => userAvailable.Contains(m.DriverUserID))
                .GroupBy(
                m => m.DriverUserID,
                m => m.DriverUserID,
                //(key, g) => driverBookings.Add(key, g.Count())
                (key, g) => new KeyValuePair<int, int>(key, g.Count())
                );

            foreach(var userId in userAvailable)
            {
                var user = keyValuePairs.FirstOrDefault(m => m.Key == userId);
                driverBookings.Add(userId, (!user.Equals(default)) ? user.Value : 0);
            }

            return driverBookings.OrderByDescending(m => m.Value)
                .OrderBy(m => m.Key)
                .FirstOrDefault().Key;
        }

        public List<ReservedSchedule> GetReservedSchedules(List<Booking> bookings)
        {
            var reservedSchedules = new List<ReservedSchedule>();

            foreach (var booking in bookings)
            {
                // Get days that are already booked.
                int days = (booking.EndDateScheduled.Date - booking.StartDateScheduled.Date).Days + 1;
                var dateReserved = Enumerable.Range(0, days).Select(i => booking.StartDateScheduled.AddDays(i).Date);

                // Store day in the reservedSchedules list along with the driver that is scheduled on that day
                foreach (var date in dateReserved)
                {
                    var reservedSchedule = reservedSchedules.FirstOrDefault(m => m.ReservedDate == date);

                    // Check if there is a record already existing for the specified date on that reservedSchedules list
                    if (reservedSchedule != null)
                    {
                        // Add driver to the existing record
                        reservedSchedules.FirstOrDefault(m => m.ReservedDate == date).DriverIds.Add(booking.DriverUserID);
                    }
                    else
                    {
                        // Create a new record if there is no record yet for the specified date.
                        reservedSchedules.Add(new ReservedSchedule
                        {
                            DriverIds = new List<int> { booking.DriverUserID },
                            ReservedDate = date
                        });
                    }
                }
            }

            return reservedSchedules;
        }

        public OrderViewModel GetBooking(int? id)
        {
            var booking = Get(id.Value);
            var bookingReview = _bookingReviewRepository.FirstOrDefault(m => m.BookingID == id.Value);

            var order = new OrderViewModel
            {
                BookingId = booking.ID,
                BookingNo = booking.BookingNo,
                DriverName = booking.DriverUser != null ? booking.DriverUser.FullName : string.Empty,
                CustomerID = booking.UserID,
                CustomerName = booking.User != null ? booking.User.FullName : string.Empty,
                PickupPoint = booking.TravelRate.PickupPoint.Name,
                DestinationPoint = booking.TravelRate.DestinationPoint.Name,
                StartDateSchedule = booking.StartDateScheduled,
                EndDateSchedule = booking.EndDateScheduled,
                Currency = booking.Currency,
                TotalPrice = booking.TotalPrice,
                Status = booking.Status,
                PaymentType = booking.PaymentType.Name,
                Seater = booking.Seater,
                BookingReviewID = bookingReview != null ? bookingReview.ID : 0,
                Message = bookingReview != null ? bookingReview.Message : string.Empty,
                Rating = bookingReview != null ? bookingReview.Rating : 0,
            };

            return order;
        }

        public List<AdminBooking> GetAdminBookings(string bookingNo = null, string status = null, DateTime? startDate = null, DateTime? endDate = null, bool? isDeleted = null)
        {
            var bookings = GetAllAsQuery();

            if (isDeleted.HasValue)
                bookings = bookings.Where(m => m.Deleted == isDeleted.Value);

            if (!string.IsNullOrEmpty(bookingNo))
                bookings = bookings.Where(m => m.BookingNo == bookingNo);

            if (!string.IsNullOrEmpty(status))
                bookings = bookings.Where(m => m.Status == status);

            if (startDate.HasValue)
            {
                var date = startDate.Value.Date;
                bookings = bookings.Where(m => m.StartDateScheduled >= date);
            }

            if (endDate.HasValue)
            {
                var date = endDate.Value.Date;
                bookings = bookings.Where(m => m.StartDateScheduled <= date);
            }

            //var test = bookings.ToList();

            var adminBookings = new List<AdminBooking>();
            bookings.ToList().ForEach(m => adminBookings.Add(new AdminBooking
            {
                BookingID = m.ID,
                BookingNo = m.BookingNo,
                CustomerName = m.User != null ? m.User.FullName  : string.Empty,
                DriverName = m.DriverUser != null ? m.DriverUser.FullName : string.Empty,
                TravelRateId = m.TravelRateID,
                PaymentType = m.PaymentType != null ? m.PaymentType.Name : string.Empty,
                Seater = m.Seater,
                TotalPrice = m.CurrencyPrice,
                StartDateScheduled = m.StartDateScheduled,
                EndDateScheduled = m.EndDateScheduled,
                DateCompleted = m.DateCompleted,
                Status = m.Status,
                DateCreated = m.DateCreated,
                CreatedBy = m.CreatedBy.ToString(),
                DateUpdated = m.DateUpdated,
                UpdatedBy = m.UpdatedBy.ToString()
            }));

            foreach(var booking in adminBookings)
            {
                var value = 0;
                int.TryParse(booking.CreatedBy, out value);
                var createdBy = _userRepository.Get(value);
                booking.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                int.TryParse(booking.UpdatedBy, out value);
                var updatedBy = _userRepository.Get(value);
                booking.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;

                var discountUsage = _discountUsageHistoryRepository.GetDiscountUsageHistoryByBookingId(booking.BookingID);
                booking.DiscountCode = discountUsage != null ? 
                    $"{discountUsage.Discount.CouponCode} - PHP {discountUsage.Discount.DiscountAmount}"
                    : string.Empty;
            }

            return adminBookings.ToList();

        }

        public AdminBooking GetAdminBooking(int? id)
        {
            var booking = Get(id.Value);

            var adminBooking = new AdminBooking
            {
                BookingID = booking.ID,
                BookingNo = booking.BookingNo,
                CustomerName = booking.User != null ? booking.User.FullName : string.Empty,
                DriverName = booking.DriverUser != null ? booking.DriverUser.FullName : string.Empty,
                TravelRateId = booking.TravelRateID,
                TravelRate = _travelRateRepository.GetTravelRate(booking.TravelRateID),
                PaymentType = booking.PaymentType != null ? booking.PaymentType.Name : string.Empty,
                Seater = booking.Seater,
                TotalPrice = booking.CurrencyPrice,
                StartDateScheduled = booking.StartDateScheduled,
                EndDateScheduled = booking.EndDateScheduled,
                DateCompleted = booking.DateCompleted,
                Status = booking.Status,
                DateCreated = booking.DateCreated,
                DateUpdated = booking.DateUpdated,
                BookingReview = _bookingReviewRepository.GetBookingReviewByBookingId(id.Value)
            };

            var createdBy = _userRepository.Get(booking.CreatedBy);
            adminBooking.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

            var updatedBy = _userRepository.Get(booking.UpdatedBy);
            adminBooking.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;

            var discountUsage = _discountUsageHistoryRepository.GetDiscountUsageHistoryByBookingId(adminBooking.BookingID);
            adminBooking.DiscountCode = discountUsage != null ?
                $"{discountUsage.Discount.CouponCode} - PHP {discountUsage.Discount.DiscountAmount}"
                : string.Empty;

            return adminBooking;
        }

        public List<DayOfWeek> GetDriverCoding(int userId = 0)
        {
            // Get last digit of the plate # to verify coding
            var PlateNumbers = _userCarRepository.GetCars(isDeleted: false)
                .Where(m => m.PlateNumber != null && m.UserId == userId)
                .Select(m => m.PlateNumber.Substring(m.PlateNumber.Length - 1))
                .Distinct()
                .ToList();

            var days = new List<DayOfWeek>();

            foreach (var platenumber in PlateNumbers)
            {
                if (platenumber == "1" || platenumber == "2")
                {
                    if (!days.Contains(DayOfWeek.Monday))
                        days.Add(DayOfWeek.Monday);
                }
                else if (platenumber == "3" || platenumber == "4")
                {
                    if (!days.Contains(DayOfWeek.Tuesday))
                        days.Add(DayOfWeek.Tuesday);
                }
                else if (platenumber == "5" || platenumber == "6")
                {
                    if (!days.Contains(DayOfWeek.Wednesday))
                        days.Add(DayOfWeek.Wednesday);
                }
                else if (platenumber == "7" || platenumber == "8")
                {
                    if (!days.Contains(DayOfWeek.Thursday))
                        days.Add(DayOfWeek.Thursday);
                }
                else if (platenumber == "9" || platenumber == "0")
                {
                    if (!days.Contains(DayOfWeek.Friday))
                        days.Add(DayOfWeek.Friday);
                }
            }

            return days;
        }
    }
}