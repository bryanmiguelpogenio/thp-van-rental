﻿using System;
using System.Collections.Generic;
using System.Linq;
using THP.Web.Areas.Admin.Models.Car;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;

namespace THP.Web.Models.DAL.Repositories
{
    public class UserCarRepository : Repository<UserCar>, IUserCarRepository
    {
        public readonly IDBContext _dbContext;
        private readonly IUserRepository _userRepository;

        public UserCarRepository(IDBContext dbContext, IUserRepository userRepository) :
            base(dbContext)
        {
            this._dbContext = dbContext;
            this._userRepository = userRepository;
        }

        public Car GetCar(int? id)
        {
            var userCar = Get(id.Value);

            var car = new Car
            {
                CarId = userCar.ID,
                UserId = userCar.UserID,
                DriverName = userCar.User.FullName,
                Brand = userCar.Brand,
                CarModel = userCar.Model,
                Color = userCar.Color,
                PlateNumber = userCar.PlateNumber,
                TotalKilometer = userCar.TotalKilometer,
                DateCreated = userCar.DateCreated,
                DateUpdated = userCar.DateUpdated
            };

            var createdBy = _userRepository.Get(userCar.CreatedBy);
            car.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

            var updatedBy = _userRepository.Get(userCar.UpdatedBy);
            car.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;

            return car;
        }

        public List<Car> GetCars(int? userId = null, bool? isDeleted = null)
        {
            var userCar = GetAllAsQuery();

            if (isDeleted.HasValue)
                userCar = userCar.Where(m => m.Deleted == isDeleted.Value);

            if (userId.HasValue && userId.Value != 0)
                userCar = userCar.Where(m => m.UserID == userId.Value);

            var cars = new List<Car>();
            cars = userCar.Select(m => new Car
            {
                CarId = m.ID,
                UserId = m.UserID,
                DriverName = m.User.Firstname + " " + m.User.Lastname,
                Brand = m.Brand,
                CarModel = m.Model,
                Color = m.Color,
                PlateNumber = m.PlateNumber,
                TotalKilometer = m.TotalKilometer,
                DateCreated = m.DateCreated,
                CreatedBy = m.CreatedBy.ToString(),
                DateUpdated = m.DateUpdated,
                UpdatedBy = m.UpdatedBy.ToString()
            }).ToList();

            foreach (var car in cars)
            {
                var value = 0;
                int.TryParse(car.CreatedBy, out value);
                var createdBy = _userRepository.Get(value);
                car.CreatedBy = createdBy != null ? createdBy.FullName : string.Empty;

                int.TryParse(car.UpdatedBy, out value);
                var updatedBy = _userRepository.Get(value);
                car.UpdatedBy = updatedBy != null ? updatedBy.FullName : string.Empty;
            }

            return cars;
        }
        public void UpdateCar(Car model)
        {
            var userCar = Get(model.CarId);

            userCar.UserID = model.UserId;
            userCar.Brand = model.Brand;
            userCar.Model = model.CarModel;
            userCar.Color = model.Color;
            userCar.PlateNumber = model.PlateNumber;
            userCar.TotalKilometer = model.TotalKilometer;
            userCar.DateUpdated = DateTime.UtcNow;
            userCar.UpdatedBy = AccountConstant.UserID;

            Update(userCar);

        }
        public void AddCar(Car model)
        {
            var userCar = new UserCar
            {
                UserID = model.UserId,
                Brand = model.Brand,
                Model = model.CarModel,
                Color = model.Color,
                PlateNumber = model.PlateNumber,
                TotalKilometer = model.TotalKilometer != null ? model.TotalKilometer : 0,
                DateUpdated = DateTime.UtcNow,
                UpdatedBy = AccountConstant.UserID,
                DateCreated = DateTime.UtcNow,
                CreatedBy = AccountConstant.UserID
            };

            Add(userCar);
        }
    }
}