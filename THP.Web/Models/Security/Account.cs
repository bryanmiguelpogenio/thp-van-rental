﻿namespace THP.Web.Models.Security
{
    public class Account
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Roles { get; set; }
    }
}