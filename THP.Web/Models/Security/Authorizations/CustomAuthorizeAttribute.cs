﻿using System.Web.Mvc;
using System.Web.Routing;
using THP.Web.Models.DAL;
using THP.Web.Models.Entities;

namespace THP.Web.Models.Security.Authorizations
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (string.IsNullOrEmpty(SessionPersister.Username))
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Account", action = "Login", area = "" }));
            }
            else
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(new DBContext(), new EncryptionService()))
                {
                    CustomPrincipal principal = new CustomPrincipal(unitOfWork.User.GetUser(SessionPersister.Username));

                    if (!principal.IsInRole(Roles))
                    {
                        filterContext.Result = new RedirectToRouteResult(new
                            RouteValueDictionary(new { controller = "Home", action = "Error", area = "" }));
                    }

                }
            }
        }
    }
}