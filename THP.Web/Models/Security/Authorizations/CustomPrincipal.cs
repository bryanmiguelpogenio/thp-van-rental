﻿using System.Linq;
using System.Security.Principal;

namespace THP.Web.Models.Security.Authorizations
{
    public class CustomPrincipal : IPrincipal
    {
        private Account account;

        public IIdentity Identity { get; set; }

        public CustomPrincipal(Account model)
        {
            this.account = model;
            this.Identity = new GenericIdentity(model.Username);
        }

        public bool IsInRole(string role)
        {
            var roles = role.Split(new char[] { ',' });
            return roles.Any(r => account.Roles.Contains(r));
        }
    }
}