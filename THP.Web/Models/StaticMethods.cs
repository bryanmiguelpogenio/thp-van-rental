﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using THP.Web.Models.Interface.DAL;

namespace THP.Web.Models
{
    public static class StaticMethods
    {
        public static string getUsername(object username)
        {
            return username != null ? username.ToString() : string.Empty;
        }

        public static int getUserID(object userID)
        {
            int value = 0;

            var username = userID != null ? userID.ToString() : string.Empty;
            int.TryParse(username, out value);

            return value;
        }

        public static string CreateBookingNo(IUnitOfWork _unitOfWork)
        {
            var value = "BOOK";
            var year = DateTime.Now.Year
                .ToString()
                .Substring(DateTime.Now.Year
                .ToString().Length - 2);

            var bookings = _unitOfWork.Booking.Find(m => m.BookingNo.Contains(value) & m.BookingNo.Substring(m.BookingNo.Length - 2) == year);
            var number = "0";

            var bookingCount = (bookings.Count() + 1).ToString();
            if (bookingCount.Length == 4)
            {
                number = bookingCount;
            }
            else if (bookingCount.Length == 3)
            {
                number = $"0{bookingCount}";
            }
            else if (bookingCount.Length == 2)
            {
                number = $"00{bookingCount}";

            }
            else
            {
                number = $"000{bookingCount}";
            }
            
            return $"BOOK{number}{year}";
        }

        public static string ToSortableString(this DateTime datetime)
        {
            return datetime.ToString(AccountConstant.DateTimeFormat);
        }
    }
}