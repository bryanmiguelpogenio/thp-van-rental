﻿//import { publicDecrypt } from "crypto";

var markers = {};
var API_KEY = "AIzaSyBpTyUgoI0FLvlc8WKBYqX2lOFDrldg-lc";
var map;
var originPosition; // start position
var destinationPosition; // end position
var directionsDisplay;

// callback method for getting current location
function showPosition() {

    // get current location
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(initMap, initMap);
    }
    else {
        alert("Geolocation is not supported by this browser.");
    }
}

// initialize and add the map
function initMap(position) {
    if (typeof position.code == "undefined") {
        originPosition = { lat: position.coords.latitude, lng: position.coords.longitude };
    }
    else {
        originPosition = { lat: 14.6090537, lng: 121.0222565 };
    }

    var myOptions = {
        center: new google.maps.LatLng(originPosition),
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        navigationControlOptions: { style: google.maps.NavigationControlStyle.SMALL },
        styles: [
            { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
            { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
            { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
            {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{ color: '#263c3f' }]
            },
            {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#6b9a76' }]
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{ color: '#38414e' }]
            },
            {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{ color: '#212a37' }]
            },
            {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#9ca5b3' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{ color: '#746855' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{ color: '#1f2835' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#f3d19c' }]
            },
            {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{ color: '#2f3948' }]
            },
            {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{ color: '#17263c' }]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#515c6d' }]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{ color: '#17263c' }]
            }
        ]
    };

    map = new google.maps.Map(document.getElementById("maparea"), myOptions);
    directionsDisplay = new google.maps.DirectionsRenderer({
        polylineOptions: {
            strokeColor: "#f3d19c"
        }
    });

    createMarker(1, originPosition, map);

    var geocoder = new google.maps.Geocoder;

    geocoder.geocode({ "location": originPosition }, function (results, status) {
        if (status === "OK") {
            //if (results[0]) {
            //    document.getElementById("GoingFrom").value = results[0].formatted_address;
            //} else {
            //    document.getElementById("GoingFrom").value = "No results found";
            //}
        } else {
            window.alert("Geocoder failed due to: " + status);
        }
    });
}

// method for creating a marker
function createMarker(id, position, map) {
    var marker = new google.maps.Marker({
        id: id,
        position: position,
        map: map,
        animation: google.maps.Animation.BOUNCE
    });

    map.panTo(new google.maps.LatLng(position));
    markers[id] = marker; // cache created marker to markers object with id as its key
}

// move marker from position current to moveto in t seconds
function animatedMove(marker, t, current, moveto) {

    var deltalat = (moveto.lat - current.lat) / 100;
    var deltalng = (moveto.lng - current.lng) / 100;

    var delay = 10 * t;
    for (var i = 0; i < 100; i++) {
        (function (ind) {
            setTimeout(
                function () {
                    var lat = marker.position.lat();
                    var lng = marker.position.lng();
                    lat += deltalat;
                    lng += deltalng;
                    latlng = new google.maps.LatLng(lat, lng);
                    marker.setPosition(latlng);
                }, delay * ind
            );
        })(i)
    }
}

function getBound(startPosition, endPosition) {
    var bounds = new google.maps.LatLngBounds();

    bounds.extend(new google.maps.LatLng(startPosition));
    bounds.extend(new google.maps.LatLng(endPosition));

    map.fitBounds(bounds); // auto-zoom
    map.panToBounds(bounds); // auto-center
}

// check if the address entered is valid or invalid
function isRegionValid(data) {

    if (data.status == "ZERO_RESULTS" || data.status == "NOT_FOUND") {
        alert("Sorry but the address that you have entered is invalid.")
        return false;
    }

    var addresses = data.results[0].address_components;
    var country = "NA";

    addresses.forEach(function (element) {

        if (element.short_name == "PH") {
            country = element.short_name;
        }
    });

    if (country != "NA") {
        return true;
    }
    else {
        alert("Sorry! We dont have any operations in that region yet.")
        return false;
    }
}

// get direction from origin to destination
function getDirection() {
    directionsDisplay.setMap(map);
    directionsDisplay.setOptions({ suppressMarkers: true });

    var request = {
        origin: originPosition,
        destination: destinationPosition,
        travelMode: google.maps.TravelMode.DRIVING
    };

    var directionsService = new google.maps.DirectionsService();
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });

    getDistance(originPosition, destinationPosition);
}

function getDistance(from, to)
{
    //$.ajax({
    //    type: "POST",
    //    url: "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="
    //        + from.lat + "," + from.lng
    //        + "&destinations="
    //        + to.lat + "," + to.lng
    //        + "&key="
    //        + API_KEY,
    //    crossDomain: true,
    //    //dataType: 'jsonp',
    //    contentType: 'application/json',
    //    headers: {
    //        'Access-Control-Allow-Credentials': true,
    //        'Access-Control-Allow-Origin': '*',
    //        'Access-Control-Allow-Methods': 'GET',
    //        'Access-Control-Allow-Headers': 'application/json',
    //    },
    //    xhrFields: { withCredentials: false },
    //    success: function (data) {
    //        alert(data);
    //    }
    //});

    var fromArray = [from];
    var toArray = [to];

    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
        {
            origins: fromArray,
            destinations: toArray,
            travelMode: 'DRIVING'
        }, distanceCallback);
}

function distanceCallback(response, status) {
    if (status == 'OK') {
        var origins = response.originAddresses;
        var destinations = response.destinationAddresses;

        for (var i = 0; i < origins.length; i++) {
            var results = response.rows[i].elements;
            for (var j = 0; j < results.length; j++) {
                var element = results[j];
                var distance = element.distance.text;
                var duration = element.duration.text;
                var from = origins[i];
                var to = destinations[j];

                $("#Distance").val(distance);
            }
        }
    }
}

function originPoint(element) {

    $.ajax({
        type: "POST",
        url: "https://maps.googleapis.com/maps/api/geocode/json?region=PH&language=tl&address="
            + $(element).children("option:selected").text()
            + "&key="
            + API_KEY,
        success: function (data) {

            if (!isRegionValid(data)) {
                return;
            }

            var currentPosition = originPosition;
            originPosition = data.results[0].geometry.location;

            if (typeof markers[1] == "undefined") {
                createMarker(1, originPosition, map);
            }
            else {
                animatedMove(markers[1], 1, currentPosition, originPosition);
            }

            if (typeof markers[2] != "undefined") {
                getBound(originPosition, destinationPosition);
                setTimeout(getDirection(), 5000);
            }
            else {
                map.panTo(new google.maps.LatLng(originPosition));
            }
        }
    });
}

function trackCustomerPoint(address) {

    $.ajax({
        type: "POST",
        url: "https://maps.googleapis.com/maps/api/geocode/json?region=PH&language=tl&address="
            + address
            + "&key="
            + API_KEY,
        success: function (data) {

            if (!isRegionValid(data)) {
                return;
            }

            var currentPosition = originPosition;
            originPosition = data.results[0].geometry.location;

            if (typeof markers[1] == "undefined") {
                createMarker(1, originPosition, map);
            }
            else {
                animatedMove(markers[1], 1, currentPosition, originPosition);
            }

            if (typeof markers[2] != "undefined") {
                getBound(originPosition, destinationPosition);
                setTimeout(getDirection(), 5000);
            }
            else {
                map.panTo(new google.maps.LatLng(originPosition));
            }
        }
    });
}


function destinationPoint(element) {

    $.ajax({
        type: "POST",
        url: "https://maps.googleapis.com/maps/api/geocode/json?region=PH&language=tl&address="
            + $(element).children("option:selected").text()
            + "&key="
            + API_KEY,
        success: function (data) {

            if (!isRegionValid(data)) {
                return;
            }

            var currentPosition = typeof destinationPosition != "undefined" ? destinationPosition : originPosition;
            destinationPosition = data.results[0].geometry.location;

            if (typeof markers[2] == "undefined") {
                createMarker(2, destinationPosition, map);
            }
            else {
                animatedMove(markers[2], 1, currentPosition, destinationPosition);
            }

            if (typeof markers[1] != "undefined") {
                getBound(originPosition, destinationPosition);
                setTimeout(getDirection(), 5000);
            }
            else {
                map.panTo(new google.maps.LatLng(destinationPosition));
            }
        }
    });
}

function trackDriverPoint(address) {

    $.ajax({
        type: "POST",
        url: "https://maps.googleapis.com/maps/api/geocode/json?region=PH&language=tl&latlng="
            + address
            + "&key="
            + API_KEY,
        success: function (data) {

            if (!isRegionValid(data)) {
                return;
            }

            var currentPosition = typeof destinationPosition != "undefined" ? destinationPosition : originPosition;
            destinationPosition = data.results[0].geometry.location;

            if (typeof markers[2] == "undefined") {
                createMarker(2, destinationPosition, map);
            }
            else {
                animatedMove(markers[2], 1, currentPosition, destinationPosition);
            }

            if (typeof markers[1] != "undefined") {
                getBound(originPosition, destinationPosition);
                setTimeout(getDirection(), 5000);
            }
            else {
                map.panTo(new google.maps.LatLng(destinationPosition));
            }
        }
    });
}

// Booking
$("#PickUpPoint").change(function () {
    originPoint(this);
})

$("#DestinationPoint").change(function () {
    destinationPoint(this);
})

// Home
$("#originLocationID").change(function () {
    originPoint(this);
})

$("#destinationLocationID").change(function () {
    destinationPoint(this);
})