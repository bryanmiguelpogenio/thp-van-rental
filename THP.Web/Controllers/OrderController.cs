﻿using System;
using System.Web.Mvc;
using THP.Web.Models;
using THP.Web.Models.DAL.Data.Order;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Controllers
{
    public class OrderController : Controller
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public OrderController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion


        #region Methods

        [CustomAuthorize]
        // GET: Order
        public ActionResult Details(int? id)
        {
            try
            {
                var model = _unitOfWork.Booking.GetBooking(id.Value);
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Details", new { id = id });
            }
        }

        [CustomAuthorize]
        [HttpPost]
        public ActionResult Details(OrderViewModel model)
        {
            try
            {
                var bookingReview = new BookingReview
                {
                    UserID = AccountConstant.UserID,
                    BookingID = model.BookingId,
                    Message = model.Message,
                    Rating = model.Rating,
                    DateCreated = DateTime.UtcNow,
                    CreatedBy = AccountConstant.UserID,
                    DateUpdated = DateTime.UtcNow,
                    UpdatedBy = AccountConstant.UserID,
                    Deleted = false
                };

                _unitOfWork.BookingReview.Add(bookingReview);
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                // Log Error Message
                TempData["ErrorMessage"] = ex.Message;
            }

            return RedirectToAction("Details", new { id = model.BookingId });
        }
        #endregion

    }
}