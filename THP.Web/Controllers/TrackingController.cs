﻿using System;
using System.Linq;
using System.Web.Mvc;
using THP.Web.Models;
using THP.Web.Models.DAL.Data.Tracking;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Controllers
{
    public class TrackingController : Controller
    {
        #region Fields

        public readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public TrackingController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region Methods

        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetMeetupLocation()
        {
            var userID = AccountConstant.UserID;
            var date = DateTime.Now;

            var booking = _unitOfWork.Booking
                .Find(m => m.UserID == userID && m.StartDateScheduled <= date && m.EndDateScheduled >= date)
                .OrderByDescending(m => m.ID)
                .FirstOrDefault();

            return Json(new { address = booking.TravelRate.PickupPoint.Name });
        }

        public JsonResult GetDriverLocation()
        {
            var userID = AccountConstant.UserID;
            var date = DateTime.Now;

            var booking = _unitOfWork.Booking
                .Find(m => m.UserID == userID && m.StartDateScheduled <= date && m.EndDateScheduled >= date)
                .OrderByDescending(m => m.ID)
                .FirstOrDefault();

            var driverCar = _unitOfWork.UserCar.FirstOrDefault(m => m.UserID == booking.DriverUserID);
            var location = driverCar.Location.Split(',').ToList();

            var geolocation = new GeolocationViewModel();

            if (location.Count == 2)
            {
                decimal.TryParse(location[0], out var value);
                geolocation.lat = value;

                decimal.TryParse(location[1], out value);
                geolocation.lang = value;
            }

            return Json(new { location });
        }

        #endregion
    }
}