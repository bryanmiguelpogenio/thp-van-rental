﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using THP.Web.Models;
using THP.Web.Models.DAL.Data.Booking;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.Configuration;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;
using Braintree;

namespace THP.Web.Controllers
{
    public class BookingController : Controller
    {
        #region Properties

        public readonly IUnitOfWork _unitOfWork;
        public readonly IBraintreeConfiguration _braintreeConfiguration;

        #endregion

        #region Ctor
        public BookingController(IUnitOfWork unitOfWork, IBraintreeConfiguration braintreeConfiguration)
        {
            this._unitOfWork = unitOfWork;
            this._braintreeConfiguration = braintreeConfiguration;
        }
        #endregion

        #region Utilities

        public List<string> GetDatesUnavailable(List<Booking> bookings)
        {
            var reservedSchedules = _unitOfWork.Booking.GetReservedSchedules(bookings);

            // Get all drivers
            var userIds = _unitOfWork.User.GetDrivers(isDeleted: false)
                .Select(m => m.ID);

            var unavailableDates = new List<string>();

            foreach (var reservedSchedule in  reservedSchedules)
            {
                // Check record if all the drivers have been booked on that day.
                var driverAvailable = userIds.Where(m => !reservedSchedule.DriverIds.Contains(m)).Count();

                // if no drivers are available store them in the list
                if (driverAvailable == 0)
                    unavailableDates.Add(reservedSchedule.ReservedDate.ToString("MM/dd/yyyy"));
            }
            
            return unavailableDates.OrderBy(m => m).ToList();
        }

        [HttpPost]
        public JsonResult GetTotalPrice(int pickUpPointID, int destinationPointID, DateTime? startDate = null, DateTime? endDate = null)
        {

            var travelRate = _unitOfWork.TravelRate
                .FirstOrDefault(m => m.PickupPointID == pickUpPointID & m.DestinationPointID == destinationPointID);

            //var datetest = DateTime.Now;
            //var start = DateTime.ParseExact(startDate, "MM/dd/yyyy hh:mm", null);
            //var end = DateTime.ParseExact(endDate, "MM/dd/yyyy hh:mm", null);

            var price = _unitOfWork.Booking.ComputeTotalPrice(travelRate.FixedPrice, startDate, endDate);
            return Json(price.ToString());
        }

        [HttpPost]
        public JsonResult GetDisabledDates()
        {
            var dateToday = DateTime.UtcNow.ToLocalTime().Date;
            var bookings = _unitOfWork.Booking
                .Find(m => m.StartDateScheduled >= dateToday)
                .ToList();

            var datesUnavailable = GetDatesUnavailable(bookings).ToArray();
            return Json(datesUnavailable, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CompleteBooking(int bookingId)
        {
            var booking = _unitOfWork.Booking.Get(bookingId);

            // Check if deleted 
            if (booking.Deleted)
            {
                return Json(new { Message = "Booking does not exist", IsSuccess = false });
            }

            // Check if date is already done
            var dateToday = DateTime.UtcNow;
            if (booking.EndDateScheduled > dateToday)
            {
                return Json(new { Message = "Schedule of booking is not yet finished", IsSuccess = false });
            }

            booking.Status = Constant.BOOKING_STATUS_COMPLETE;
            booking.DateCompleted = DateTime.UtcNow;
            booking.DateUpdated = DateTime.UtcNow;
            booking.UpdatedBy = AccountConstant.UserID;

            var userCar = _unitOfWork.UserCar
                .Find(m => m.UserID == AccountConstant.UserID)
                .LastOrDefault();

            if (userCar != null)
            {
                var currentTotalKm = userCar.TotalKilometer.HasValue ? userCar.TotalKilometer.Value : 0;
                userCar.TotalKilometer = currentTotalKm + booking.Kilometer;

                _unitOfWork.UserCar.Update(userCar);
            }

            _unitOfWork.Booking.Update(booking);

            // Create a notifcation for customer
            var notification = new Notification
            {
                UserID = booking.UserID,
                Message = $"{booking.BookingNo} - is now completed",
                IsClicked = true,
                CreatedBy = booking.UserID,
                DateCreated = DateTime.UtcNow,
                UpdatedBy = booking.UserID,
                DateUpdated = DateTime.UtcNow
            };

            _unitOfWork.Notification.Add(notification);

            notification = new Notification
            {
                UserID = booking.DriverUserID,
                Message = $"{booking.BookingNo} - is now completed",
                IsClicked = false,
                CreatedBy = booking.DriverUserID,
                DateCreated = DateTime.UtcNow,
                UpdatedBy = booking.DriverUserID,
                DateUpdated = DateTime.UtcNow
            };

            _unitOfWork.SaveChanges();
            return Json(new { Message = "Booking Complete!", IsSuccess = true });
        }

        [HttpPost]
        public JsonResult OnGoingBooking(int bookingId)
        {
            var booking = _unitOfWork.Booking.Get(bookingId);

            // Check if deleted 
            if (booking.Deleted)
            {
                return Json(new { Message = "Booking does not exist" });
            }

            // Check if date is already done
            var dateToday = DateTime.UtcNow;
            if (booking.EndDateScheduled > dateToday)
            {
                return Json(new { Message = "Schedule of booking is not yet starting" });
            }

            booking.Status = Constant.BOOKING_STATUS_ONGOING;
            booking.DateUpdated = DateTime.UtcNow;
            booking.UpdatedBy = AccountConstant.UserID;
            booking.DateCompleted = DateTime.UtcNow;

            _unitOfWork.Booking.Update(booking);

            return Json(new { Message = "Booking is now On Going!" });
        }

        [HttpPost]
        public JsonResult CancelBooking(int bookingId)
        {
            var booking = _unitOfWork.Booking.Get(bookingId);

            // Check if deleted 
            if (booking.Deleted)
            {
                return Json(new { Message = "Booking does not exist", IsSuccess = false });
            }

            // Check if date is already done
            //var dateToday = DateTime.UtcNow;
            //if (booking.EndDateScheduled > dateToday)
            //{
            //    return Json(new { Message = "Schedule of booking is not yet finished", IsSuccess = false });
            //}

            // Braintree gateway configuration
            var gateway = _braintreeConfiguration.GetGateway();
            var transaction = gateway.Transaction.Refund(booking.TransactionID, booking.TotalPrice);

            if (transaction.IsSuccess())
            {
                booking.Status = Constant.BOOKING_STATUS_CANCELLED;
                booking.DateUpdated = DateTime.UtcNow;
                booking.UpdatedBy = AccountConstant.UserID;

                _unitOfWork.Booking.Update(booking);

                // Create a notifcation for customer
                var notification = new Notification
                {
                    UserID = booking.UserID,
                    Message = $"{booking.BookingNo} - has been cancelled",
                    IsClicked = true,
                    CreatedBy = booking.UserID,
                    DateCreated = DateTime.UtcNow,
                    UpdatedBy = booking.UserID,
                    DateUpdated = DateTime.UtcNow
                };

                _unitOfWork.Notification.Add(notification);

                notification = new Notification
                {
                    UserID = booking.DriverUserID,
                    Message = $"{booking.BookingNo} - has been cancelled",
                    IsClicked = false,
                    CreatedBy = booking.DriverUserID,
                    DateCreated = DateTime.UtcNow,
                    UpdatedBy = booking.DriverUserID,
                    DateUpdated = DateTime.UtcNow
                };

                _unitOfWork.Notification.Add(notification);

                _unitOfWork.SaveChanges();

                return Json(new { Message = "Booking Cancelled!", IsSuccess = true });
            }
            else
            {

                return Json(new { Message = transaction.Message, IsSuccess = false });
            }
        }

        [HttpPost]
        public JsonResult BookingConfig()
        {
            var retval = false;

            if (AccountConstant.UserID != 0)
            {
                var booking = _unitOfWork.Booking.Find(m => m.UserID == AccountConstant.UserID)
                    .LastOrDefault();

                if (booking != null 
                    && (booking.Status != Constant.BOOKING_STATUS_COMPLETE 
                    && booking.Status != Constant.BOOKING_STATUS_CANCELLED)
                    )
                    retval = true;
            }

            return Json(new { hasBooking = retval });
        }

        #endregion

        #region Methods

        [CustomAuthorize]
        // GET: Booking
        public ActionResult Index()
        {
            BookingViewModel model = new BookingViewModel();

            try
            {
                //var locations = _unitOfWork.Location.Find(m => m.ParentLocationID != 0);

                _unitOfWork.Location
                    .Find(m => m.ID == 1)
                    .ToList()
                    .ForEach(m => model.lst_PickUpPointID.Add(
                        new SelectListItem
                        {
                            Text = m.Address,
                            Value = m.ID.ToString()
                        }));

                _unitOfWork.Location
                    .Find(m => m.ParentLocationID != 0)
                    .ToList()
                    .ForEach(m => model.lst_DestinationPointID.Add(
                        new SelectListItem
                        {
                            Text = m.Address,
                            Value = m.ID.ToString()
                        }));

                //var dateToday = DateTime.UtcNow.ToLocalTime().Date;
                //var bookings = _unitOfWork.Booking
                //    .Find(m => m.StartDateScheduled >= dateToday)
                //    .ToList();

                //model.lst_unavailableDates = GetDatesUnavailable(bookings);

                // Braintree gateway configuration
                var gateway = _braintreeConfiguration.GetGateway();
                gateway.Configuration.Timeout = 200000;

                var clientToken = gateway.ClientToken.Generate();
                ViewBag.ClientToken = clientToken;
                //ViewBag.ClientToken = "asasassa";
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [CustomAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(BookingViewModel model)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors).ToList();

                if (ModelState.IsValid)
                {
                    _unitOfWork.Booking.CreateBooking(model);
                    _unitOfWork.SaveChanges();

                    return RedirectToAction("Index", "Booking");
                }
            }
            catch (Exception ex)
            {
                // Log Error Message
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Index");
            }

            _unitOfWork.Location
                .Find(m => m.ID == 1)
                .ToList()
                .ForEach(m => model.lst_PickUpPointID.Add(
                    new SelectListItem
                    {
                        Text = m.Address,
                        Value = m.ID.ToString()
                    }));

            _unitOfWork.Location
                .Find(m => m.ParentLocationID != 0)
                .ToList()
                .ForEach(m => model.lst_DestinationPointID.Add(
                    new SelectListItem
                    {
                        Text = m.Address,
                        Value = m.ID.ToString()
                    }));

            return View(model);
        }

        #endregion
    }
}