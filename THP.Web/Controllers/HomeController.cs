﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using THP.Web.Models;
using THP.Web.Models.DAL.Data.Home;
using THP.Web.Models.Interface.Configuration;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Controllers
{
    public class HomeController : Controller
    {
        #region Fields
        public readonly IUnitOfWork _unitOfWork;
        public readonly IBraintreeConfiguration _braintreeConfiguration;
        #endregion

        #region Ctor
        public HomeController(IUnitOfWork unitOfWork, IBraintreeConfiguration braintreeConfiguration)
        {
            this._unitOfWork = unitOfWork;
            this._braintreeConfiguration = braintreeConfiguration;
        }
        #endregion

        #region Utilities

        public JsonResult HeaderMenu()
        {
            var menu = _unitOfWork.TopicPage.Find(m => m.Display && !m.Deleted).ToList();
            return Json(new { menu });
        }

        //public JsonResult HeaderConfig()
        //{
        //    var retval = false;

        //    if (AccountConstant.UserID != 0)
        //    {
        //        var booking = _unitOfWork.Booking.Find(m => m.UserID == AccountConstant.UserID)
        //            .LastOrDefault();

        //        if (booking.Status == Constant.BOOKING_STATUS_ONGOING)
        //            retval = true;
        //    }

        //    return Json(new { hasBooking =  retval });
        //}

        public JsonResult UserNotification()
        {
            var notifications = new List<NotificationViewModel>();

            int userId = 0;
            int.TryParse(System.Web.HttpContext.Current.Session["userID"].ToString(), out userId);

            if (userId != 0)
            {
                notifications = _unitOfWork.Notification
                    .Find(m => m.UserID == userId)
                    .OrderByDescending(m => m.DateCreated)
                    .Take(5)
                    .Select(m => new NotificationViewModel {
                        UserId = m.UserID,
                        Message = m.Message,
                    })
                    .ToList();

            }

            return Json(new { notifications = notifications });
        }
        #endregion

        #region Methods
        // Homepage
        public ActionResult Index()
        {
            var model = new HomeQuickBookingViewModel();

            try
            {
                _unitOfWork.Location
                    .Find(m => m.ID == 1)
                    .ToList()
                    .ForEach(m => model.lst_originLocationID.Add(
                        new SelectListItem
                        {
                            Text = m.Address,
                            Value = m.ID.ToString()
                        }));

                _unitOfWork.Location
                    .Find(m => m.ParentLocationID != 0)
                    .ToList()
                    .ForEach(m => model.lst_destinationLocationID.Add(
                        new SelectListItem
                        {
                            Text = m.Address,
                            Value = m.ID.ToString()
                        }));

                // Braintree gateway configuration
                var gateway = _braintreeConfiguration.GetGateway();
                gateway.Configuration.Timeout = 180000;

                var clientToken = gateway.ClientToken.Generate();
                ViewBag.ClientToken = clientToken;
                //ViewBag.ClientToken = "asasassa";
            }
            catch (Exception ex)
            {
                // Log Error Message
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Index(HomeQuickBookingViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.Booking.CreateBooking(model);
                    _unitOfWork.SaveChanges();

                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                // Log Error Message
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // Service
        public ActionResult Services()
        {
            return View();
        }

        public ActionResult FacebookPage()
        {
            return View();
        }

        // Error Page
        public ActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }

        public ActionResult Topic(string url)
        {
            var model = _unitOfWork.TopicPage.FirstOrDefault(m => m.UrlSlug == url && m.Display && !m.Deleted);

            if (model != null)
                return View(model);
            else
                return RedirectToAction("Error");
        }
        #endregion
    }
}
