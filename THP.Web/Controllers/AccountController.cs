﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using THP.Web.Models;
using THP.Web.Models.DAL.Data.Accounts;
using THP.Web.Models.DAL.Data.Enum;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Security;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Controllers
{
    public class AccountController : Controller
    {
        #region Properties
        public readonly IUnitOfWork _unitOfWork;
        public readonly IEncryptionService _encryptionService;
        #endregion

        #region Ctor
        public AccountController(IUnitOfWork unitOfWork, IEncryptionService encryptionService)
        {
            this._unitOfWork = unitOfWork;
            this._encryptionService = encryptionService;
        }
        #endregion

        #region Methods
        public ActionResult Login()
        {
            AccountLoginViewModel model = new AccountLoginViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(AccountLoginViewModel model, string returnUrl = null)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<string> lst_Errors = new List<string>();

                    var user = _unitOfWork.User.Get(m => m.Username == model.Username);

                    if (user != null && string.Compare(user.Username, model.Username, false) == 0)
                    {
                        if (user.Deleted)
                            lst_Errors.Add("The user you have entered is not active");

                        string encryptPassword = _encryptionService.CreatePasswordHash(model.Password, user.PasswordSaltKey);

                        if (encryptPassword != user.Password)
                            lst_Errors.Add("the password you have entered is incorrect");
                    }
                    else
                    {
                        lst_Errors.Add("The user name or password provided is incorrect.");
                    }

                    if (lst_Errors.Count > 0)
                    {
                        foreach (var status in lst_Errors)
                        {
                            ModelState.AddModelError("LoginOnError", status);
                        }
                    }
                    else
                    {
                        System.Web.HttpContext.Current.Session["username"] = model.Username;
                        System.Web.HttpContext.Current.Session["userID"] = user.ID;
                        System.Web.HttpContext.Current.Session["roleID"] = user.RoleID;

                        AccountConstant.Username = StaticMethods.getUsername(System.Web.HttpContext.Current.Session["username"]);
                        AccountConstant.UserID = StaticMethods.getUserID(System.Web.HttpContext.Current.Session["userID"]);

                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                        {
                            // Redirect to the page that you have been redirected
                            // in the first place
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            // Redirect to default page
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log Error Message
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Login");
            }

            return View();
        }
    
        public ActionResult Register()
        {
            AccountRegisterViewModel model = new AccountRegisterViewModel();

            _unitOfWork.Role.Find(m => !m.Deleted & (m.ID == 2 || m.ID == 3))
                .ToList()
                .ForEach(m => model.lstRole.Add(new SelectListItem
                {
                    Text = m.Name,
                    Value = m.ID.ToString()
                }));

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(AccountRegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    List<string> lst_Errors = new List<string>();

                    // Check if username already exists
                    var user = _unitOfWork.User.Get(m => m.Username == model.Username);
                    if (user != null) lst_Errors.Add("Username already exists.");

                    // Check if email address already exists
                    user = _unitOfWork.User.Get(m => m.EmailAddress == model.EmailAddress);
                    if (user != null) lst_Errors.Add("Email address is already being used.");

                    if (lst_Errors.Count > 0)
                    {
                        foreach (var status in lst_Errors)
                        {
                            ModelState.AddModelError("RegisterOnError", status);
                        }
                    }
                    else
                    {
                        var newUser = _unitOfWork.User.CreateUser(model);

                        if (model.RoleID == 3)
                        {
                            _unitOfWork.UserCar.Add(new UserCar
                            {
                                Brand = model.Brand,
                                Model = model.CarModel,
                                Color = model.Color,
                                PlateNumber = model.PlateNumber,
                                IsActive = true,
                                DateCreated = DateTime.UtcNow,
                                CreatedBy = newUser.ID,
                                DateUpdated = DateTime.UtcNow,
                                UpdatedBy = newUser.ID,
                            });
                        }

                        _unitOfWork.SaveChanges();

                        System.Web.HttpContext.Current.Session["username"] = model.Username;
                        System.Web.HttpContext.Current.Session["userID"] = newUser.ID;
                        System.Web.HttpContext.Current.Session["roleID"] = newUser.RoleID;

                        // Send email for account activation.
                        var activation_link = string.Format("<a href='{0}'>Activate Account</a>", string.Format("http://thprental.azurewebsites.net/api/user/activate/{0}", model.Username));

                        var body = string.Format("<p>Email From: {0} (thpvanrental.noreply@gmail.com)</p>"
                                            + "<p>Email To: {1} ({2})</p>"
                                            + "<p>Message:</p>"
                                            + "<p>Welcome to THP Van Rental Services to activate account please click {3}.</p>",
                                            "MVC Roles Encryption",
                                            model.Firstname + " " + model.Lastname,
                                            model.EmailAddress,
                                            activation_link);

                        var message = new MailMessage();
                        message.To.Add(new MailAddress(model.EmailAddress));
                        message.From = new MailAddress("thprentalservices@gmail.com");
                        message.Subject = "THP: Activation of Account";
                        message.Body = body;
                        message.IsBodyHtml = true;

                        var smtp = new SmtpClient
                        {
                            Host = "smtp.gmail.com",
                            Port = 587,
                            EnableSsl = true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = false,
                            Credentials = new NetworkCredential("thprentalservices@gmail.com", "Login@123")
                        };

                        await smtp.SendMailAsync(message);

                        return RedirectToAction("Index", "Home");
                    }

                }
            }
            catch (Exception ex)
            {
                // Log error message
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Register");
            }

            _unitOfWork.Role.Find(m => !m.Deleted & (m.ID == 2 || m.ID == 3))
                .ToList()
                .ForEach(m => model.lstRole.Add(new SelectListItem
                {
                    Text = m.Name,
                    Value = m.ID.ToString()
                }));

            return View(model);
        }

        [CustomAuthorize]
        public ActionResult AccountProfile()
        {
            ProfileViewModel model = new ProfileViewModel();
    
            var user = _unitOfWork.User.FirstOrDefault(m => !m.Deleted && m.Username == AccountConstant.Username);

            model.Firstname = user.Firstname;
            model.Middlename = user.Middlename;
            model.Lastname = user.Lastname;

            model.EmailAddress = user.EmailAddress;

            model.Username = user.Username;
            model.Password = user.Password;

            //model.RoleID = user.RoleID;

            //List<int> lstRoleID = new List<int>() { 2, 3 };

            //if (user.RoleID == 1)
            //    lstRoleID.Add(user.RoleID);

            //_unitOfWork.Role.Find(m => !m.Deleted && lstRoleID.Contains(m.ID))
            //    .ToList()
            //    .ForEach(m => model.lstRole.Add(new SelectListItem
            //    {
            //        Text = m.Name,
            //        Value = m.ID.ToString()
            //    }));

            model.RoleName = user.Role.Name;

            var userDetail = _unitOfWork.UserDetail.FirstOrDefault(m => !m.Deleted && m.UserID == user.ID);

            if (userDetail != null)
            {
                model.Birthdate = userDetail.Birthdate ?? null;
                model.CellphoneNo = userDetail.CellphoneNo ?? string.Empty;
                model.TelephoneNo = userDetail.TelephoneNo ?? string.Empty;
                model.Gender = userDetail.Gender ?? string.Empty;
                model.Address = userDetail.Address ?? string.Empty;
                model.Municipality = userDetail.Municipality ?? string.Empty;
                model.City = userDetail.City ?? string.Empty;
                model.Province = userDetail.Province ?? string.Empty;
                model.Region = userDetail.Region ?? string.Empty;
            }

            if (user.RoleID != (int)RoleType.Driver)
            {
                // Get Customer's booking(s)
                _unitOfWork.Booking.Find(m => m.UserID == user.ID)
                    .OrderByDescending(m => m.ID)
                    .ToList()
                    .ForEach(m => model.lstProfileBooking.Add(new ProfileBookingViewModel
                    {
                        Id = m.ID,
                        BookingNo = m.BookingNo,
                        DriverName = m.DriverUser.FullName,
                        CustomerName = m.User.FullName,
                        DateCompleted = m.DateCompleted.HasValue ? m.DateCompleted.Value.ToString() : string.Empty,
                        StartDateScheduled = m.StartDateScheduled,
                        EndDateScheduled = m.EndDateScheduled,
                        Route = $"{m.TravelRate.PickupPoint.Name} - {m.TravelRate.DestinationPoint.Name}",
                        TotalPrice = m.CurrencyPrice,
                        Status = m.Status
                    }));
            }
            else
            {
                // Get Driver's scheduled booking(s)
                _unitOfWork.Booking.Find(m => m.DriverUserID == user.ID && m.UserID != 0)
                    .OrderByDescending(m => m.ID)
                    .ToList()
                    .ForEach(m => model.lstProfileBooking.Add(new ProfileBookingViewModel
                    {
                        Id = m.ID,
                        BookingNo = m.BookingNo,
                        DriverName = m.DriverUser.FullName,
                        CustomerName = m.User.FullName,
                        DateCompleted = m.DateCompleted.HasValue ? m.DateCompleted.Value.ToString() : string.Empty,
                        StartDateScheduled = m.StartDateScheduled,
                        EndDateScheduled = m.EndDateScheduled,
                        Route = $"{m.TravelRate.PickupPoint.Name} - {m.TravelRate.DestinationPoint.Name}",
                        TotalPrice = m.CurrencyPrice,
                        Status = m.Status
                    }));
            }

            return View(model);
        }

        [CustomAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AccountProfile(ProfileViewModel model)
        {
            try
            {
                var userID = AccountConstant.UserID;
                var user = _unitOfWork.User.Get(userID);

                if (ModelState.IsValid)
                {

                    user.Firstname = model.Firstname;
                    user.Middlename = model.Middlename;
                    user.Lastname = model.Lastname;
                    user.Username = model.Username;
                    user.EmailAddress = model.EmailAddress;
                    //user.RoleID = model.RoleID;

                    user.UpdatedBy = userID;
                    user.DateUpdated = DateTime.UtcNow;

                    _unitOfWork.UserDetail.CreateUserDetail(model, userID);

                    _unitOfWork.SaveChanges();

                    return RedirectToAction("AccountProfile", "Account");
                }

                //List<int> lstRoleID = new List<int>() { 2, 3 };

                //if (user.RoleID == 1)
                //    lstRoleID.Add(user.RoleID);

                //_unitOfWork.Role.Find(m => !m.Deleted && lstRoleID.Contains(m.ID))
                //    .ToList()
                //    .ForEach(m => model.lstRole.Add(new SelectListItem
                //    {
                //        Text = m.Name,
                //        Value = m.ID.ToString()
                //    }));
            }
            catch (Exception ex)
            {
                // Log error message
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("AccountProfile");
            }

            return View(model);
        }

        [CustomAuthorize]
        public ActionResult Logout()
        {
            Session.Remove("username");
            Session.Remove("userID");
            Session.Remove("roleID");

            return RedirectToAction("Login", "Account");
        }
        #endregion
    }
}