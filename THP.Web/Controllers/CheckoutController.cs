﻿using Braintree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using THP.Web.Models.Interface.Configuration;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Security.Authorizations;

namespace THP.Web.Controllers
{
    public class CheckoutController : Controller
    {
        #region Fields

        IUnitOfWork _unitOfWork;
        IBraintreeConfiguration _braintreeConfiguration;

        #endregion

        #region Ctor

        public CheckoutController(IUnitOfWork unitOfWork, IBraintreeConfiguration braintreeConfiguration)
        {
            this._unitOfWork = unitOfWork;
            this._braintreeConfiguration = braintreeConfiguration;
        }

        #endregion

        #region Utilities

        [HttpPost]
        public JsonResult ApplyDiscountCode(string discountCode)
        {
            var discount = _unitOfWork.Discount.GetDiscountByDiscountCode(discountCode);

            var dateToday = DateTime.UtcNow;
            if (discount != null && discount.StartDate <= dateToday && discount.EndDate >= dateToday)
            {
                return Json(new { id = discount.ID, amount = discount.DiscountAmount, code = discount.CouponCode, ErrorMessage = string.Empty });
            }
            else
                return Json(new { id = 0, amount = string.Empty, code = string.Empty, ErrorMessage = "Discount does not exist." });
        }

        // GET: Checkout
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Checkout(string payment_method_nonce, string payment_start_date, string payment_end_date)
        {
            DateTime startDateTime = Convert.ToDateTime(payment_start_date);
            DateTime endDateTime = Convert.ToDateTime(payment_end_date);

            if (_unitOfWork.Booking.GetAvailableDriver(startDateTime, endDateTime) == 0)
            {
                TempData["Flash"] = "No driver is availabale for that selected days";
                return Json(new { ErrorMessage = "Error", orderId = "" });
            }

            var gateway = _braintreeConfiguration.GetGateway();

            var isPartial = false;
            var amount = Convert.ToDecimal(0);
            var totalAmount = Convert.ToDecimal(0);

            try
            {
                amount = Convert.ToDecimal(Request["amount"]);
                totalAmount = Convert.ToDecimal(Request["hiddenAmount"]);

                if (totalAmount != amount)
                {
                    isPartial = true;
                }
            }
            catch (FormatException ex)
            {
                TempData["Flash"] = "Error: 81503: Amount is an invalid format.";
                return Json(new { ErrorMessage = "Error", orderId = "" });
            }

            var nonce = Request["payment_method_nonce"];
            var request = new TransactionRequest
            {
                Amount = amount,
                PaymentMethodNonce = nonce,
                MerchantAccountId = "THPVanRental",
                Options = new TransactionOptionsRequest
                {
                    SubmitForSettlement = !isPartial ? true : false,
                }
            };

            Result<Transaction> result = gateway.Transaction.Sale(request);

            // applicable only for partial payment
            //if (isPartial)
            //{
            //    request = new TransactionRequest
            //    {
            //        Amount = amount,
            //        PaymentMethodNonce = nonce,
            //        MerchantAccountId = "THPVanRental",
            //    };

            //    gateway.Transaction.SubmitForPartialSettlement(result.Transaction.AuthorizedTransactionId, request);
            //}

            if (result.IsSuccess())
            {
                Transaction transaction = result.Target;
                return Json(new { ErrorMessage = "", orderId = transaction.Id });
            }
            else if (result.Transaction != null)
            {
                return Json(new { ErrorMessage = result.Message, orderId = "" });
            }
            else
            {
                string errorMessages = "";
                foreach (ValidationError error in result.Errors.DeepAll())
                {
                    errorMessages += $"Error: {(int)error.Code} - {error.Message}\n";
                }
                TempData["Flash"] = errorMessages;
                return Json(new { ErrorMessage = "Error", orderId = "" });
            }
        }

        #endregion
    }
}