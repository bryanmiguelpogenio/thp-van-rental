﻿using System;
using System.Collections.Generic;
using THP.Web.Models.DAL.Data.Accounts;
using THP.Web.Models.Entities.Data;
using THP.Web.THP.Mobile.Api.Models;

namespace THP.Web.THP.Mobile.Api.Extensions
{
    public static class ModelExtension
    {
        public static bool IsNull(this object model)
        {
            return model == null;
        }
        
        public static ICollection<GetBookingReviewsResponseModel> ToGetBookingReviewsResponseModel(this IEnumerable<BookingReview> model)
        {
            var result = new List<GetBookingReviewsResponseModel>();
            foreach(var review in model)
            {
                result.Add(new GetBookingReviewsResponseModel()
                {
                    BookingId = review.BookingID,
                    Id = review.ID,
                    Message = review.Message,
                    Rating = review.Rating,
                    UserId = review.UserID
                });
            }
            return result;
        }

        public static BookingReview ToAddBookingReviewRequestModel(this AddBookingReviewRequestModel model)
        {
            return new BookingReview()
            {
                BookingID = model.BookingId,
                CreatedBy = model.UserId,
                DateCreated = DateTime.Today,
                DateUpdated = DateTime.Today,
                Deleted = false,
                Message = model.Message,
                Rating = model.Rating,
                UpdatedBy = model.UserId,
                UserID = model.UserId 
            };
        }

        public static LoginResponseModel ToLoginResponseModel(this User model)
        {
            return new LoginResponseModel()
            {
                FirstName = model.Firstname,
                MiddleName = model.Middlename,
                LastName = model.Lastname,
                EmailAddress = model.EmailAddress,
                UserName = model.Username,
                RoleId = model.RoleID,
                Id = model.ID
            };
        }

        public static AccountRegisterViewModel ToRegisterViewModel(this RegisterRequestModel model)
        {
            return new AccountRegisterViewModel()
            {
                Firstname = model.Firstname == null ? "N/A" : model.Firstname,
                Middlename = model.Middlename == null ? "N/A" : model.Middlename,
                Lastname = model.Lastname == null ? "N/A" : model.Lastname,
                EmailAddress = model.EmailAddress,
                Username = model.Username,
                Password = model.Password == null ? "N/A" : model.Password,
                RoleID = model.RoleId
            };
        }

        public static UserDetail CreateRequestToUserDetailModel(this CreateUserDetailRequestModel model)
        {
            return new UserDetail()
            {
                Municipality = model.Municipality,
                Province = model.Province,
                Region = model.Region,
                TelephoneNo = model.TelephoneNo,
                Gender = model.Gender,
                City = model.City,
                Address = model.Address,
                Birthdate = model.Birthdate,
                CellphoneNo = model.CellphoneNo,
                CreatedBy = model.UserId,
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow,
                UserID = model.UserId,
                UpdatedBy = model.UserId,
                Deleted = false
            };
        }

        public static UserDetail UpdateRequestToUserDetailModel(this UpdateUserDetailRequestModel model)
        {
            return new UserDetail()
            {
                Municipality = model.Municipality,
                Province = model.Province,
                Region = model.Region,
                TelephoneNo = model.TelephoneNo,
                Gender = model.Gender,
                City = model.City,
                Address = model.Address,
                Birthdate = model.Birthdate,
                CellphoneNo = model.CellphoneNo,
                UserID = model.UserId,
                ID = model.Id
            };
        }

        public static GetUserDetailResponseModel UserDetailToGetUserDetailResponseModel(this UserDetail model)
        {
            GetUserDetailResponseModel result = null;
            if (!model.IsNull())
            {
                result = new GetUserDetailResponseModel()
                {
                    Municipality = model.Municipality,
                    Province = model.Province,
                    Region = model.Region,
                    TelephoneNo = model.TelephoneNo,
                    Gender = model.Gender,
                    City = model.City,
                    Address = model.Address,
                    Birthdate = model.Birthdate,
                    CellphoneNo = model.CellphoneNo,
                    Id = model.ID
                };
            }

            return result;
        }

        public static GetUserCarResponseModel UserCarToGetUserCarResponseModel(this UserCar model, User user)
        {
            return new GetUserCarResponseModel()
            {
                Id = model.ID,
                User = user.ToLoginResponseModel(),
                Brand = model.Brand,
                Color = model.Color,
                Model = model.Model,
                PlateNumber = model.PlateNumber,
                TotalKilometer = model.TotalKilometer,
                Location = model.Location
            };
        }

        public static ICollection<GetAllPromosResponseModel> DiscountToGetAllPromosResponseModel(this IEnumerable<Discount> model)
        {
            var result = new List<GetAllPromosResponseModel>();
            foreach (var discount in model)
            {
                result.Add(new GetAllPromosResponseModel()
                {
                    Id = discount.ID,
                    CouponCode = discount.CouponCode,
                    DiscountAmount = discount.DiscountAmount,
                    EndDate = discount.EndDate,
                    HasCouponCode = discount.HasCouponCode,
                    Name = discount.Name,
                    StartDate = discount.StartDate
                });
            }

            return result;
        }

        public static ICollection<GetAllBookingResponseModel> BookingsToGetAllBookingResponseModel(this IEnumerable<Booking> model)
        {
            var result = new List<GetAllBookingResponseModel>();
            foreach (var booking in model)
            {
                result.Add(booking.BookingToGetAllBookingResponseModel());
            }

            return result;
        }

        public static GetAllBookingResponseModel BookingToGetAllBookingResponseModel(this Booking model)
        {
            var result = new GetAllBookingResponseModel()
            {
                Id = model.ID,
                BookingNo = model.BookingNo,
                Currency = model.Currency,
                DateCompleted = model.DateCompleted,
                DriverUser = model.DriverUser.ToLoginResponseModel(),
                EndDateScheduled = model.EndDateScheduled,
                PaymentTypeId = model.PaymetTypeID,
                Seater = model.Seater,
                StartDateScheduled = model.StartDateScheduled,
                Status = model.Status,
                TotalPrice = model.TotalPrice,
                TravelRate = model.TravelRate.ToGetAllRatesResponseModel()
            };

            return result;
        }

        public static ICollection<GetAllRatesResponseModel> TravelRateToGetAllRatesResponseModel(this ICollection<TravelRate> model)
        {
            var result = new List<GetAllRatesResponseModel>();
            foreach (var travelRate in model)
            {
                result.Add(travelRate.ToGetAllRatesResponseModel());
            }

            return result;
        }

        private static GetAllRatesResponseModel ToGetAllRatesResponseModel(this TravelRate model)
        {
            return new GetAllRatesResponseModel()
            {
                Id = model.ID,
                DestinationPoint = model.DestinationPoint.ToLocationApiModel(),
                FixedPrice = model.FixedPrice,
                MaxHours = model.MaxHours,
                Overnight = model.Overnight,
                Overtime = model.Overtime,
                PickupPoint = model.PickupPoint.ToLocationApiModel()
            };
        }

        public static ICollection<GetAllDiscountResponseModel> DiscountToGetAllDiscountResponseModel(this ICollection<Discount> model)
        {
            var result = new List<GetAllDiscountResponseModel>();
            foreach (var discount in model)
            {
                result.Add(discount.ToGetAllDiscountResponseModel());
            }

            return result;
        }

        private static GetAllDiscountResponseModel ToGetAllDiscountResponseModel(this Discount model)
        {
            return new GetAllDiscountResponseModel()
            {
                Id = model.ID,
                CouponCode = model.CouponCode,
                DiscountAmount = model.DiscountAmount,
                EndDate = model.EndDate,
                Name = model.Name,
                StartDate = model.StartDate
            };
        }

        private static LocationApiModel ToLocationApiModel(this Location model)
        {
            return new LocationApiModel()
            {
                Description = model.Description,
                Id = model.ID,
                Name = model.Name,
                ParentLocationID = model.ParentLocationID
            };
        }

        public static ICollection<LocationApiModel> ToListLocationApiModel(this IEnumerable<Location> model)
        {
            ICollection<LocationApiModel> result = new List<LocationApiModel>();
            foreach(var location in model)
            {
                result.Add(location.ToLocationApiModel());
            }

            return result;
        }
    }
}