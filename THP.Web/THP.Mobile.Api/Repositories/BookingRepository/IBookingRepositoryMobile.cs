﻿using System.Collections.Generic;
using THP.Web.Models.DAL.Data.Booking;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.THP.Mobile.Api.Models;

namespace THP.Web.THP.Mobile.Api.Repositories.BookingRepository
{
    public interface IBookingRepositoryMobile : IRepository<Booking>
    {
        Booking CreateBooking(AddBookingRequestModel model);

        List<ReservedSchedule> GetReservedSchedules(List<Booking> bookings);
    }
}
