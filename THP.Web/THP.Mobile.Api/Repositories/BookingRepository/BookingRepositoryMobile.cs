﻿using System;
using System.Collections.Generic;
using System.Linq;
using THP.Web.Models;
using THP.Web.Models.DAL.Data.Booking;
using THP.Web.Models.DAL.Repositories;
using THP.Web.Models.Entities.Data;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;
using THP.Web.THP.Mobile.Api.Models;

namespace THP.Web.THP.Mobile.Api.Repositories.BookingRepository
{
    public class BookingRepositoryMobile : Repository<Booking>, IBookingRepositoryMobile
    {
        public readonly IDBContext _dbContext;
        private readonly IUserRepository _userRepository;
        private readonly IDiscountRepository _discountRepository;
        private readonly IDiscountUsageHistoryRepository _discountUsageHistoryRepository;
        private readonly IUserCarRepository _userCarRepository;
        private readonly INotificationRepository _notificationRepository;

        public BookingRepositoryMobile(
            IDBContext dbContext,
            IUserRepository userRepository, 
            IDiscountRepository discountRepository, 
            IDiscountUsageHistoryRepository discountUsageHistoryRepository,
            IUserCarRepository userCarRepository,
            INotificationRepository notificationRepository) :
            base(dbContext)
        {
            this._dbContext = dbContext;
            this._userRepository = userRepository;
            this._discountRepository = discountRepository;
            this._discountUsageHistoryRepository = discountUsageHistoryRepository;
            this._userCarRepository = userCarRepository;
            this._notificationRepository = notificationRepository;
        }

        public Booking CreateBooking(AddBookingRequestModel model)
        {
            var booking = new Booking();

            booking.BookingNo = CreateBookingNo();
            booking.UserID = model.UserId;
            booking.Seater = model.Seater;
            booking.StartDateScheduled = model.StartDateScheduled;
            booking.EndDateScheduled = model.EndDateScheduled;
            booking.TransactionID = model.TransactionId;

            booking.DriverUserID = GetAvailableDriver(model.StartDateScheduled, model.EndDateScheduled);
            if (booking.DriverUserID == 0)
                throw new HandledException("There's no available driver for your booking."); booking.PaymetTypeID = model.PaymentTypeId;

            var travelRate = _dbContext.Set<TravelRate>()
                .FirstOrDefault(x => x.ID == model.TravelRate.Id);
            var discount = _discountRepository.Get(model.DiscountId);

            booking.TravelRateID = travelRate.ID;
            booking.TotalPrice = ComputeTotalPrice(travelRate.FixedPrice.Value, model.StartDateScheduled, model.EndDateScheduled, discount != null ? discount.DiscountAmount : 0);
            booking.Currency = model.Currency;
            booking.Kilometer = travelRate.Kilometer;

            booking.Status = Constant.BOOKING_STATUS_QUEUED;

            booking.CreatedBy = model.UserId;
            booking.DateCreated = DateTime.UtcNow;
            booking.UpdatedBy = model.UserId;
            booking.DateUpdated = DateTime.UtcNow;

            var result = _dbContext.Set<Booking>().Add(booking);

            if (model.DiscountId != 0)
            {
                var discountUsageHistory = new DiscountUsageHistory
                {
                    BookingID = booking.ID,
                    DiscountID = model.DiscountId,
                    DateCreated = DateTime.UtcNow,
                    CreatedBy = model.UserId
                };

                _discountUsageHistoryRepository.Add(discountUsageHistory);
            }

            // Create a notifcation for customer
            var notifcation = new Notification
            {
                UserID = model.UserId,
                Message = $"You've created a booking ({booking.BookingNo}) between dates {booking.StartDateScheduled} and {booking.EndDateScheduled}",
                IsClicked = true,
                CreatedBy = model.UserId,
                DateCreated = DateTime.UtcNow,
                UpdatedBy = model.UserId,
                DateUpdated = DateTime.UtcNow
            };

            _notificationRepository.Add(notifcation);

            notifcation = new Notification
            {
                UserID = booking.DriverUserID,
                Message = $"You've been booked ({booking.BookingNo}) between dates {booking.StartDateScheduled} and {booking.EndDateScheduled}",
                IsClicked = false,
                CreatedBy = model.UserId,
                DateCreated = DateTime.UtcNow,
                UpdatedBy = model.UserId,
                DateUpdated = DateTime.UtcNow
            };

            _notificationRepository.Add(notifcation);

            _dbContext.SaveChanges();

            return result;
        }

        public int GetAvailableDriver(DateTime? startDate, DateTime? endDate)
        {
            // Get days that customer wants to book.
            int days = (endDate.Value.Date - startDate.Value.Date).Days + 1;
            var daysBooked = Enumerable.Range(0, days).Select(i => startDate.Value.AddDays(i).Date);
            //var dayOfWeeks = daysBooked.Select(m => m.DayOfWeek).Distinct().ToList();

            var dayOfWeeks = new List<DayOfWeek>();
            dayOfWeeks.Add(startDate.Value.DayOfWeek);
            dayOfWeeks.Add(endDate.Value.DayOfWeek);

            var dateToday = DateTime.UtcNow.ToLocalTime().Date;
            var bookings = Find(m => m.StartDateScheduled >= dateToday)
                .ToList();

            // Get days that are affected for the schedules that have been booked
            var reservedSchedules = GetReservedSchedules(bookings)
                .Where(m => daysBooked.Contains(m.ReservedDate));

            // Get all drivers
            var userIds = _userRepository.GetDrivers(isDeleted: false)
                .Select(m => m.ID)
                .ToList();

            var userAvailable = new List<int>();
            foreach (var userId in userIds)
            {
                // check if the driver already has a booking between the selected start and enddate for booking.
                var reservedSchedule = reservedSchedules.FirstOrDefault(m => m.DriverIds.Contains(userId));


                // check if the car of the driver on the selected dates are coding.
                var plateNumbers = GetDriverCoding(userId);
                var isDriverAvailable = true;

                if (plateNumbers.Count == 0)
                {
                    isDriverAvailable = false;
                }
                else
                {
                    foreach (var plateNumber in plateNumbers)
                    {
                        if (dayOfWeeks.Contains(plateNumber))
                        {
                            isDriverAvailable = false;
                        }

                    }
                }

                // Add driver to the list of available drivers for further booking process
                if (reservedSchedule == null && isDriverAvailable)
                    userAvailable.Add(userId);
            }

            // Compare total bookings of each driver for equal booking.
            var driverBookings = new Dictionary<int, int>();

            var keyValuePairs = Find(m => userAvailable.Contains(m.DriverUserID))
                .GroupBy(
                m => m.DriverUserID,
                m => m.DriverUserID,
                //(key, g) => driverBookings.Add(key, g.Count())
                (key, g) => new KeyValuePair<int, int>(key, g.Count())
                );

            foreach (var userId in userAvailable)
            {
                var user = keyValuePairs.FirstOrDefault(m => m.Key == userId);
                driverBookings.Add(userId, (!user.Equals(default)) ? user.Value : 0);
            }

            return driverBookings.OrderByDescending(m => m.Value)
                .OrderBy(m => m.Key)
                .FirstOrDefault().Key;
        }

        public List<ReservedSchedule> GetReservedSchedules(List<Booking> bookings)
        {
            var reservedSchedules = new List<ReservedSchedule>();

            foreach (var booking in bookings)
            {
                // Get days that are already booked.
                int days = (booking.EndDateScheduled.Date - booking.StartDateScheduled.Date).Days + 1;
                var dateReserved = Enumerable.Range(0, days).Select(i => booking.StartDateScheduled.AddDays(i).Date);

                // Store day in the reservedSchedules list along with the driver that is scheduled on that day
                foreach (var date in dateReserved)
                {
                    var reservedSchedule = reservedSchedules.FirstOrDefault(m => m.ReservedDate == date);

                    // Check if there is a record already existing for the specified date on that reservedSchedules list
                    if (reservedSchedule != null)
                    {
                        // Add driver to the existing record
                        reservedSchedules.FirstOrDefault(m => m.ReservedDate == date).DriverIds.Add(booking.DriverUserID);
                    }
                    else
                    {
                        // Create a new record if there is no record yet for the specified date.
                        reservedSchedules.Add(new ReservedSchedule
                        {
                            DriverIds = new List<int> { booking.DriverUserID },
                            ReservedDate = date
                        });
                    }
                }
            }

            return reservedSchedules;
        }

        public decimal ComputeTotalPrice(decimal? price, DateTime? startDate, DateTime? endDate, decimal? discountAmount = null)
        {
            var totalDays = (startDate.HasValue && endDate.HasValue) ?
                            (endDate.Value.Date - startDate.Value.Date).TotalDays + 1 : 0;
            totalDays = Math.Round(totalDays, 1);

            if (!discountAmount.HasValue)
                discountAmount = 0;

            return ((price.HasValue ? price.Value : Convert.ToDecimal(0.00)) * Convert.ToDecimal(totalDays)) - discountAmount.Value;
        }

        private string CreateBookingNo()
        {
            var year = DateTime.Now.Year
                .ToString()
                .Substring(DateTime.Now.Year
                .ToString().Length - 2);

            var bookings = _dbContext.Set<Booking>()
                .Where(m => m.BookingNo.Contains("BOOK") & m.BookingNo.Substring(m.BookingNo.Length - 2) == year)
                .AsEnumerable();

            var number = "0";

            var bookingCount = (bookings.Count() + 1).ToString();
            if (bookingCount.Length == 4)
            {
                number = bookingCount;
            }
            else if (bookingCount.Length == 3)
            {
                number = $"0{bookingCount}";
            }
            else if (bookingCount.Length == 2)
            {
                number = $"00{bookingCount}";

            }
            else
            {
                number = $"000{bookingCount}";
            }

            return $"BOOK{number}{year}";
        }

        public List<DayOfWeek> GetDriverCoding(int userId = 0)
        {
            // Get last digit of the plate # to verify coding
            var PlateNumbers = _userCarRepository.GetCars(isDeleted: false)
                .Where(m => m.PlateNumber != null && m.UserId == userId)
                .Select(m => m.PlateNumber.Substring(m.PlateNumber.Length - 1))
                .Distinct()
                .ToList();

            var days = new List<DayOfWeek>();

            foreach (var platenumber in PlateNumbers)
            {
                if (platenumber == "1" || platenumber == "2")
                {
                    if (!days.Contains(DayOfWeek.Monday))
                        days.Add(DayOfWeek.Monday);
                }
                else if (platenumber == "3" || platenumber == "4")
                {
                    if (!days.Contains(DayOfWeek.Tuesday))
                        days.Add(DayOfWeek.Tuesday);
                }
                else if (platenumber == "5" || platenumber == "6")
                {
                    if (!days.Contains(DayOfWeek.Wednesday))
                        days.Add(DayOfWeek.Wednesday);
                }
                else if (platenumber == "7" || platenumber == "8")
                {
                    if (!days.Contains(DayOfWeek.Thursday))
                        days.Add(DayOfWeek.Thursday);
                }
                else if (platenumber == "9" || platenumber == "0")
                {
                    if (!days.Contains(DayOfWeek.Friday))
                        days.Add(DayOfWeek.Friday);
                }
            }

            return days;
        }
    }
}