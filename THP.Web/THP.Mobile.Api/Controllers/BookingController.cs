﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using THP.Web.Models;
using THP.Web.Models.Interface.Configuration;
using THP.Web.Models.Interface.DAL;
using THP.Web.THP.Mobile.Api.Extensions;
using THP.Web.THP.Mobile.Api.Models;

namespace THP.Web.THP.Mobile.Api.Controllers
{
    [RoutePrefix("api/booking")]
    public class BookingController : ApiController
    {
        public readonly IUnitOfWork _unitOfWork;
        public readonly IBraintreeConfiguration _braintreeConfiguration;

        public BookingController(IUnitOfWork unitOfWork, IBraintreeConfiguration braintreeConfiguration)
        {
            _unitOfWork = unitOfWork;
            _braintreeConfiguration = braintreeConfiguration;
        }

        [HttpPost]
        public IHttpActionResult AddBooking(AddBookingRequestModel model)
        {
            try
            {
                var result = _unitOfWork.MobileBooking.CreateBooking(model);
                _unitOfWork.SaveChanges();

                return Ok(result.BookingToGetAllBookingResponseModel());
            }
            catch(HandledException)
            {
                return StatusCode(HttpStatusCode.Unused);
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("{userId}")]
        public IHttpActionResult GetAllBooking(int userId)
        {
            try
            {
                var result = _unitOfWork.MobileBooking.Find(x => x.UserID == userId && !x.Deleted).OrderByDescending(x => x.DateCreated).ToList();

                return Ok(result.BookingsToGetAllBookingResponseModel());
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("driver/{driverId}")]
        public IHttpActionResult GetAllDriverBooking(int driverId)
        {
            try
            {
                var result = _unitOfWork.MobileBooking.Find(x => x.DriverUserID == driverId && !x.Deleted).OrderByDescending(x => x.DateCreated).ToList();

                return Ok(result.BookingsToGetAllBookingResponseModel());
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpPut]
        public IHttpActionResult UpdateBooking(DeleteBookingRequestModel model)
        {
            try
            {
                var booking = _unitOfWork.MobileBooking.Get(model.Id);
                booking.Status = model.Status;
                _unitOfWork.Booking.Update(booking);
                _unitOfWork.SaveChanges();

                return Ok();
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        public IHttpActionResult GetDatesUnavailable()
        {
            var bookings = _unitOfWork.Booking.Find(x => (x.Status != Constant.BOOKING_STATUS_QUEUED || x.Status != Constant.BOOKING_STATUS_ONGOING) && !x.Deleted).ToList();
            var reservedSchedules = _unitOfWork.MobileBooking.GetReservedSchedules(bookings);

            // Get all drivers
            var userIds = _unitOfWork.User.GetDrivers(isDeleted: false)
                .Select(m => m.ID);

            var unavailableDates = new List<string>();

            foreach (var reservedSchedule in reservedSchedules)
            {
                // Check record if all the drivers have been booked on that day.
                var driverAvailable = userIds.Where(m => !reservedSchedule.DriverIds.Contains(m)).Count();

                // if no drivers are available store them in the list
                if (driverAvailable == 0)
                    unavailableDates.Add(reservedSchedule.ReservedDate.ToString("MM/dd/yyyy"));
            }

            return Ok(unavailableDates.OrderBy(m => m).ToList());
        }
    }
}
