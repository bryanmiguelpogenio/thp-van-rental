﻿using System.Web.Http;
using Braintree;
using THP.Web.Models.Interface.Configuration;
using THP.Web.THP.Mobile.Api.Models;

namespace THP.Web.THP.Mobile.Api.Controllers
{
    [RoutePrefix("api/checkout")]
    public class CheckoutController : ApiController
    {
        IBraintreeConfiguration _braintreeConfiguration;

        public CheckoutController(IBraintreeConfiguration braintreeConfiguration)
        {
            _braintreeConfiguration = braintreeConfiguration;
        }

        [Route("getclienttoken")]
        [HttpGet]
        public IHttpActionResult GetBraintreeClientToken()
        {
            try
            {
                var gateway = _braintreeConfiguration.CreateGateway();
                var result = new BraintreeTokenModel(gateway.ClientToken.Generate());

                return Ok(result);
            }
            catch
            {
                return InternalServerError();
            } 
        }

        [Route("paywithpaypal")]
        [HttpPost]
        public IHttpActionResult PayWithPaypal(PayWithPaypalRequestModel request)
        {
            try
            {
                var gateway = _braintreeConfiguration.CreateGateway();

                var transactionRequest = new TransactionRequest
                {
                    Amount = request.Amount,
                    PaymentMethodNonce = request.Token,
                    Options = new TransactionOptionsRequest
                    {
                        SubmitForSettlement = true
                    }
                };

                Result<Transaction> result = gateway.Transaction.Sale(transactionRequest);
                return Ok(result);
            }
            catch
            {
                return InternalServerError();
            }
        }
    }
}
