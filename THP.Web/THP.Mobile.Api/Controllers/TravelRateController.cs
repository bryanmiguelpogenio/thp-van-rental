﻿using System;
using System.Linq;
using System.Web.Http;
using THP.Web.Models.Interface.DAL;
using THP.Web.THP.Mobile.Api.Extensions;

namespace THP.Web.THP.Mobile.Api.Controllers
{
    [RoutePrefix("api/travelrate")]
    public class TravelRateController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public TravelRateController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public IHttpActionResult GetAllRates()
        {
            try
            {
                var result = _unitOfWork.TravelRate.Find(x => !x.Deleted).ToList();

                return Ok(result.TravelRateToGetAllRatesResponseModel());
            }
            catch
            {
                return InternalServerError();
            }
        }
    }
}
