﻿using System;
using System.Linq;
using System.Web.Http;
using THP.Web.Models.Interface.DAL;
using THP.Web.THP.Mobile.Api.Extensions;

namespace THP.Web.THP.Mobile.Api.Controllers
{
    [RoutePrefix("api/discount")]
    public class DiscountController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public DiscountController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public IHttpActionResult GetAllDiscounts()
        {
            try
            {
                var result = _unitOfWork.Discount.Find(x => !x.Deleted && x.StartDate <= DateTime.Today && x.EndDate >= DateTime.Today).ToList();

                return Ok(result.DiscountToGetAllDiscountResponseModel());
            }
            catch
            {
                return InternalServerError();
            }
        }
    }
}
