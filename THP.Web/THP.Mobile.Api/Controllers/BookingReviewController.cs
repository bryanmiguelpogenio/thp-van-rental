﻿using System.Web.Http;
using THP.Web.Models.Interface.DAL;
using THP.Web.THP.Mobile.Api.Extensions;
using THP.Web.THP.Mobile.Api.Models;

namespace THP.Web.THP.Mobile.Api.Controllers
{
    [RoutePrefix("api/bookingreview")]
    public class BookingReviewController : ApiController
    {
        public readonly IUnitOfWork _unitOfWork;

        public BookingReviewController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost]
        public IHttpActionResult AddBookingReview(AddBookingReviewRequestModel model)
        {
            try
            {
                _unitOfWork.BookingReview.AddBookingReview(model.ToAddBookingReviewRequestModel());
                _unitOfWork.SaveChanges();

                return Ok();
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("{userId}")]
        public IHttpActionResult GetBookingReviews(int userId)
        {
            try
            {
                var result = _unitOfWork.BookingReview.Find(x => !x.Deleted && x.UserID == userId);
                return Ok(result.ToGetBookingReviewsResponseModel());
            }
            catch
            {
                return InternalServerError();
            }
        }
    }
}
