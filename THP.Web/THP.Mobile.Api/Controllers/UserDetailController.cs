﻿using System.Web.Http;
using THP.Web.Models.Interface.DAL;
using THP.Web.THP.Mobile.Api.Extensions;
using THP.Web.THP.Mobile.Api.Models;

namespace THP.Web.THP.Mobile.Api.Controllers
{
    [RoutePrefix("api/userdetail")]
    public class UserDetailController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserDetailController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        [HttpPost]
        public IHttpActionResult CreateUserDetail(CreateUserDetailRequestModel model)
        {
            try
            {
                var result = _unitOfWork.UserDetail.CreateUserDetailMobile(model.CreateRequestToUserDetailModel());
                return Ok(result);
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpPut]
        public IHttpActionResult UpdateUserDetail(UpdateUserDetailRequestModel model)
        {
            try
            {
                _unitOfWork.UserDetail.UpdateUserDetailMobile(model.UpdateRequestToUserDetailModel());
                _unitOfWork.SaveChanges();
                return Ok();
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("{userId}")]
        public IHttpActionResult GetUserDetail([FromUri]int userId)
        {
            try
            {
                var result = _unitOfWork.UserDetail.FirstOrDefault(x => x.UserID == userId);
                return Ok(result.UserDetailToGetUserDetailResponseModel());
            }
            catch
            {
                return InternalServerError();
            }
        }
    }
}
