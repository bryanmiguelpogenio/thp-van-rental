﻿using System.Web.Http;
using THP.Web.Models.Interface.DAL;
using THP.Web.THP.Mobile.Api.Extensions;

namespace THP.Web.THP.Mobile.Api.Controllers
{
    [RoutePrefix("api/location")]
    public class LocationController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public LocationController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet]
        public IHttpActionResult GetAllLocations()
        {
            try
            {
                var result = _unitOfWork.Location.Find(x => !x.Deleted);

                return Ok(result.ToListLocationApiModel());
            }
            catch
            {
                return InternalServerError();
            }
        }
    }
}
