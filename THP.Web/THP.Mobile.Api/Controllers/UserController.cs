﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Security;
using THP.Web.THP.Mobile.Api.Extensions;
using THP.Web.THP.Mobile.Api.Models;

namespace THP.Web.THP.Mobile.Api.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEncryptionService _encryptionService;

        public UserController(IUnitOfWork unitOfWork, IEncryptionService encryptionService)
        {
            _unitOfWork = unitOfWork;
            _encryptionService = encryptionService;
        }

        [HttpGet]
        [Route("role")]
        public IHttpActionResult GetRoles()
        {
            try
            {
                var result = _unitOfWork.Role.Find(x => !x.Deleted);
                return Ok(result);

            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("externallogin")]
        public IHttpActionResult GoogleFacebookLogin(RegisterRequestModel model)
        {
            try
            {
                var result = _unitOfWork.User.Get(x => x.EmailAddress == model.EmailAddress);
                if (result.IsNull())
                {
                    _unitOfWork.User.CreateUser(model.ToRegisterViewModel());
                    _unitOfWork.SaveChanges();
                    result = _unitOfWork.User.Get(x => x.EmailAddress == model.EmailAddress);
                }

                return Ok(result.ToLoginResponseModel());
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("login")]
        public IHttpActionResult Login(LoginRequestModel model)
        {
            try
            {
                LoginResponseModel result = null;
                var user = _unitOfWork.User.Get(x => x.Username == model.Username && !x.Deleted && x.Verified);
                if (!user.IsNull() && string.Compare(user.Username, model.Username, false) == 0)
                {
                    var encryptedPassword = _encryptionService.CreatePasswordHash(model.Password, user.PasswordSaltKey);
                    if (encryptedPassword == user.Password)
                    {
                        result = user.ToLoginResponseModel();
                    }
                }

                return Ok(result);
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> Register(RegisterRequestModel model)
        {
            try
            {
                var user = _unitOfWork.User.Get(x => (x.Username == model.Username || x.EmailAddress == model.EmailAddress) && !x.Deleted);
                if (user.IsNull())
                {
                    _unitOfWork.User.CreateUser(model.ToRegisterViewModel());
                    _unitOfWork.SaveChanges();
                    await SendConfirmationEmail(model);
                }
                
                return Ok(user.IsNull() ? 0 : user.ID);
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpPut]
        public IHttpActionResult UpdateUser(UpdateUserRequestModel model)
        {
            try
            {
                var user = _unitOfWork.User.FirstOrDefault(x => x.ID == model.Id);
                user.Lastname = model.Lastname;
                user.Firstname = model.Firstname;
                user.Middlename = model.Middlename;
                user.Username = model.Username;
                user.Password = model.Password == string.Empty ? user.Password : _encryptionService.CreatePasswordHash(model.Password, user.PasswordSaltKey);
                user.UpdatedBy = user.ID;
                user.DateUpdated = DateTime.UtcNow;
                _unitOfWork.User.Update(user);
                _unitOfWork.SaveChanges();
                return Ok();
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("activate/{username}")]
        public IHttpActionResult ActivateUser(string username)
        {
            try
            {
                var user = _unitOfWork.User.FirstOrDefault(x => x.Username == username);
                if (!user.IsNull())
                {
                    user.UpdatedBy = user.ID;
                    user.DateUpdated = DateTime.UtcNow;
                    user.Verified = true;
                    _unitOfWork.User.Update(user);
                    _unitOfWork.SaveChanges();
                }
                return Ok();
            }
            catch
            {
                return InternalServerError();
            }
        }

        private async Task SendConfirmationEmail(RegisterRequestModel model)
        {
            var activation_link = string.Format("<a href='{0}'>Activate Account</a>", string.Format("http://thprental.azurewebsites.net/api/user/activate/{0}", model.Username));

            var body = string.Format("<p>Email From: {0} (thprentalservices@gmail.com)</p>"
                                + "<p>Email To: {1} ({2})</p>"
                                + "<p>Message:</p>"
                                + "<p>Welcome to THP Van Rental Services to activate account please click {3}.</p>",
                                "MVC Roles Encryption",
                                model.Firstname + " " + model.Lastname,
                                model.EmailAddress,
                                activation_link);

            var message = new MailMessage();
            message.To.Add(new MailAddress(model.EmailAddress));
            message.From = new MailAddress("thprentalservices@gmail.com");
            message.Subject = "THP: Activation of Account";
            message.Body = body;
            message.IsBodyHtml = true;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("thprentalservices@gmail.com", "Login@123")
            };

            await smtp.SendMailAsync(message);
        }
    }
}
