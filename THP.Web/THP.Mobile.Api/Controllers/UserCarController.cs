﻿using System.Web.Http;
using THP.Web.Models.Interface.DAL;
using THP.Web.THP.Mobile.Api.Extensions;
using THP.Web.THP.Mobile.Api.Models;

namespace THP.Web.THP.Mobile.Api.Controllers
{
    [RoutePrefix("api/usercar")]
    public class UserCarController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserCarController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        
        [HttpGet]
        public IHttpActionResult GetUserCar(int id)
        {
            try
            {
                var result = _unitOfWork.UserCar.FirstOrDefault(x => x.UserID == id);
                var user = _unitOfWork.User.Get(result.UserID);
                return Ok(result.UserCarToGetUserCarResponseModel(user));
            }
            catch
            {
                return InternalServerError();
            }
        }

        [HttpPut]
        public IHttpActionResult UpdateUserCarLocation(UpdateUserCarLocationRequestModel model)
        {
            try
            {
                var userCar = _unitOfWork.UserCar.FirstOrDefault(x => x.ID == model.Id);
                userCar.Location = model.Location;
                _unitOfWork.UserCar.Update(userCar);
                _unitOfWork.SaveChanges();

                return Ok();
            }
            catch
            {
                return InternalServerError();
            }
        }
    }
}
