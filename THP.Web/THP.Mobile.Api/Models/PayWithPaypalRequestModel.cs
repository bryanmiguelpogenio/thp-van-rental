﻿namespace THP.Web.THP.Mobile.Api.Models
{
    using Newtonsoft.Json;

    public class PayWithPaypalRequestModel
    {
        [JsonProperty("nonce")]
        public string Token { get; set; }

        [JsonProperty("chargeAmount")]
        public decimal Amount { get; set; }
    }
}
