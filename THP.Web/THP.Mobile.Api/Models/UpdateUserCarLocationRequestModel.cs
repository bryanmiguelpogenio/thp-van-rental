﻿namespace THP.Web.THP.Mobile.Api.Models
{
    public class UpdateUserCarLocationRequestModel
    {
        public int Id { get; set; }

        public string Location { get; set; }
    }
}