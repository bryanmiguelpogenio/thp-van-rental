﻿namespace THP.Web.THP.Mobile.Api.Models
{
    using Newtonsoft.Json;

    public class GetAllRatesResponseModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("pickupPoint")]
        public LocationApiModel PickupPoint { get; set; }

        [JsonProperty("destinationPoint")]
        public LocationApiModel DestinationPoint { get; set; }

        [JsonProperty("fixedPrice")]
        public decimal? FixedPrice { get; set; }

        [JsonProperty("overtime")]
        public decimal? Overtime { get; set; }

        [JsonProperty("overnight")]
        public decimal? Overnight { get; set; }

        [JsonProperty("maxHours")]
        public int MaxHours { get; set; }
    }
}
