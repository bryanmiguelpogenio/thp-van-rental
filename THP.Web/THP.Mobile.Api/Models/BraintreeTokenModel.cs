﻿namespace THP.Web.THP.Mobile.Api.Models
{
    using Newtonsoft.Json;

    public class BraintreeTokenModel
    {
        public BraintreeTokenModel(string token)
        {
            this.Token = token;
        }

        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
