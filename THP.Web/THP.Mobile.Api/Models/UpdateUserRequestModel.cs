﻿namespace THP.Web.THP.Mobile.Api.Models
{
    using Newtonsoft.Json;

    public class UpdateUserRequestModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("firstName")]
        public string Firstname { get; set; }

        [JsonProperty("middleName")]
        public string Middlename { get; set; }

        [JsonProperty("lastName")]
        public string Lastname { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("roleId")]
        public int RoleId { get; set; }
    }
}
