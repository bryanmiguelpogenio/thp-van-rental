﻿namespace THP.Web.THP.Mobile.Api.Models
{
    using Newtonsoft.Json;

    public class LoginResponseModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("middleName")]
        public string MiddleName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("roleId")]
        public int RoleId { get; set; }
    }
}
