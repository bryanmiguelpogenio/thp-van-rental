﻿using Newtonsoft.Json;

namespace THP.Web.THP.Mobile.Api.Models
{
    public class LoginRequestModel
    {
        [JsonProperty("userName")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}