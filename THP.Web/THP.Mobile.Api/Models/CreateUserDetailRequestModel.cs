﻿namespace THP.Web.THP.Mobile.Api.Models
{
    using System;
    using Newtonsoft.Json;

    public class CreateUserDetailRequestModel
    {
        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("birthdate")]
        public DateTime? Birthdate { get; set; }

        [JsonProperty("cellphoneNo")]
        public string CellphoneNo { get; set; }

        [JsonProperty("telephoneNo")]
        public string TelephoneNo { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("municipality")]
        public string Municipality { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("province")]
        public string Province { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }
    }
}
