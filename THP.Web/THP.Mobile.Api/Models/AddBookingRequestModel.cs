﻿using System;
using Newtonsoft.Json;

namespace THP.Web.THP.Mobile.Api.Models
{
    public class AddBookingRequestModel
    {
        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("travelRate")]
        public GetAllRatesResponseModel TravelRate { get; set; }

        [JsonProperty("paymentTypeId")]
        public int PaymentTypeId { get; set; }

        [JsonProperty("seater")]
        public int Seater { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("startDateScheduled")]
        public DateTime StartDateScheduled { get; set; }

        [JsonProperty("endDateScheduled")]
        public DateTime EndDateScheduled { get; set; }

        [JsonProperty("discountId")]
        public int DiscountId { get; set; }

        [JsonProperty("transactionId")]
        public string TransactionId { get; set; }
    }
}