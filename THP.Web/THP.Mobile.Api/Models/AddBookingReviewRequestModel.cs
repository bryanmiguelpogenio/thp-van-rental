﻿namespace THP.Web.THP.Mobile.Api.Models
{
    using Newtonsoft.Json;

    public class AddBookingReviewRequestModel
    {
        [JsonProperty("bookingId")]
        public int BookingId { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("rating")]
        public int Rating { get; set; }
    }
}
