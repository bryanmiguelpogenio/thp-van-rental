﻿using Newtonsoft.Json;

namespace THP.Web.THP.Mobile.Api.Models
{
    public class RegisterRequestModel
    {
        [JsonProperty("firstName")]
        public string Firstname { get; set; }

        [JsonProperty("middleName")]
        public string Middlename { get; set; }

        [JsonProperty("lastName")]
        public string Lastname { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("userName")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("roleId")]
        public int RoleId { get; set; }
    }
}