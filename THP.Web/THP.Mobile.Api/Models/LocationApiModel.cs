﻿namespace THP.Web.THP.Mobile.Api.Models
{
    using Newtonsoft.Json;

    public class LocationApiModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("parentLocationId")]
        public int ParentLocationID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
