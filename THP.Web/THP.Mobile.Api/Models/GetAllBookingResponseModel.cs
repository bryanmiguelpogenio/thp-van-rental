﻿using System;
using Newtonsoft.Json;
using THP.Web.Models.Entities.Data;

namespace THP.Web.THP.Mobile.Api.Models
{
    public class GetAllBookingResponseModel
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("bookingNo")]
        public string BookingNo { get; set; }

        [JsonProperty("driverUser")]
        public LoginResponseModel DriverUser { get; set; }

        [JsonProperty("travelRate")]
        public GetAllRatesResponseModel TravelRate { get; set; }

        [JsonProperty("paymentTypeId")]
        public int PaymentTypeId { get; set; }

        [JsonProperty("seater")]
        public int Seater { get; set; }

        [JsonProperty("totalPrice")]
        public decimal TotalPrice { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("startDateScheduled")]
        public DateTime StartDateScheduled { get; set; }

        [JsonProperty("endDateScheduled")]
        public DateTime EndDateScheduled { get; set; }

        [JsonProperty("dateCompleted")]
        public DateTime? DateCompleted { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
