﻿using Newtonsoft.Json;

namespace THP.Web.THP.Mobile.Api.Models
{
    public class GetUserCarResponseModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("driver")]
        public LoginResponseModel User { get; set; }

        [JsonProperty("brand")]
        public string Brand { get; set; }

        [JsonProperty("model")]
        public string Model { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }

        [JsonProperty("plateNumber")]
        public string PlateNumber { get; set; }

        [JsonProperty("totalKilometer")]
        public decimal? TotalKilometer { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }
    }
}
