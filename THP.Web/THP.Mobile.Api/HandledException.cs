﻿using System;
using System.Runtime.Serialization;

namespace THP.Web.THP.Mobile.Api
{
    public class HandledException : Exception
    {
        public HandledException()
        {
        }

        public HandledException(string message) : base(message)
        {
        }

        public HandledException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected HandledException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}