﻿DROP TABLE [dbo].[User];
DROP TABLE [dbo].[UserDetail];
DROP TABLE [dbo].[Role];
DROP TABLE [dbo].[Page];
DROP TABLE [dbo].[UserCar];
DROP TABLE [dbo].[UserCarSpecification];
DROP TABLE [dbo].[Specification];
DROP TABLE [dbo].[ServiceType];
DROP TABLE [dbo].[PaymentType];
DROP TABLE [dbo].[TravelRate];
DROP TABLE [dbo].[Booking];
DROP TABLE [dbo].[Location];
DROP TABLE [dbo].[Discount];
DROP TABLE [dbo].[DiscountUsageHistory];
DROP TABLE [dbo].[BookingReview];
DROP TABLE [dbo].[TopicPage];
DROP TABLE [dbo].[Notification];

CREATE TABLE [dbo].[User]
(
	[ID] 			    INT 			NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
    [Firstname] 	    NVARCHAR(50) 	NOT NULL, 
    [Middlename] 	    NVARCHAR(50) 	NOT NULL, 
    [Lastname] 		    NVARCHAR(50) 	NOT NULL, 
    [EmailAddress]	    NVARCHAR(50) 	NOT NULL, 
    [Username] 		    NVARCHAR(50) 	NOT NULL, 
    [Password] 		    NVARCHAR(MAX) 	NOT NULL, 
    [PasswordSaltKey] 	NVARCHAR(MAX) 	NOT NULL, 
    [RoleID] 		    INT 			NOT NULL 	DEFAULT 0, 
    [Verified] 		    BIT 			NOT NULL 	DEFAULT 0, 
    [Deleted] 		    BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[UserDetail]
(
	[ID] 			INT 			NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
    [UserID] 		INT 			NOT NULL 	DEFAULT 0, 
    [Birthdate] 	DATETIME 		NULL, 
    [CellphoneNo] 	NVARCHAR(12) 	NULL, 
    [TelephoneNo] 	NVARCHAR(7) 	NULL, 
    [Gender] 		NVARCHAR(6) 	NULL, 
    [Address] 		NVARCHAR(MAX) 	NULL, 
    [Municipality] 	NVARCHAR(50) 	NULL, 
    [City] 			NVARCHAR(50) 	NULL, 
    [Province] 		NVARCHAR(50) 	NULL, 
    [Region] 		NVARCHAR(50) 	NULL,
    [Deleted] 		BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[Role]
(
	[ID] 			INT 			NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
    [Name] 			NVARCHAR(50) 	NOT NULL, 
    [Description] 	NVARCHAR(MAX) 	NULL,
    [Deleted] 		BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[Page]
(
	[ID]			INT				NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
    [ParentID]		INT				NOT NULL 	DEFAULT 0, 
    [Name]			NVARCHAR(50)	NOT NULL, 
	[Controller]	NVARCHAR(255) 	NULL,
	[Action]		NVARCHAR(255) 	NULL,
	[Display] 		BIT 			NOT NULL 	DEFAULT 1, 
    [DisplayOrder]	INT				NOT NULL 	DEFAULT 0,
	[Deleted] 		BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[UserCar]
(
	[ID] 			    INT 	        NOT NULL 	PRIMARY KEY IDENTITY(1,1),
    [UserID]            INT             NOT NULL, 
    [Brand]             NVARCHAR(100)   NOT NULL,
    [Model]             NVARCHAR(100)   NOT NULL,
    [Color]             NVARCHAR(100)   NOT NULL,
    [PlateNumber]       NVARCHAR(10)    NOT NULL,
    [IsActive]          BIT             NOT NULL    DEFAULT 1,
    [TotalKilometer]    DECIMAL(7, 2)   NULL,
    [Location]			NVARCHAR(MAX)   NULL		DEFAULT '',
    [Deleted] 		    BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[UserCarSpecification]
(
	[ID] 			    INT 	        NOT NULL 	PRIMARY KEY IDENTITY(1,1),
    [UserCarID]         INT             NOT NULL, 
    [SpecificationID]   INT             NOT NULL,
    [Value]             NVARCHAR(MAX)   NOT NULL,
    [Deleted] 		    BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[Specification]
(
	[ID] 			INT 			NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
    [Name] 			NVARCHAR(50) 	NOT NULL, 
    [Description] 	NVARCHAR(MAX) 	NULL,
    [Deleted] 		BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[ServiceType]
(
	[ID] 			INT 			NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
    [Name] 			NVARCHAR(50) 	NOT NULL, 
    [Description] 	NVARCHAR(MAX) 	NULL,
    [Deleted] 		BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[PaymentType]
(
	[ID] 			    INT 	        NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
    [Name]              NVARCHAR(50)    NOT NULL,
    [Description]       NVARCHAR(255)   NULL,
    [Deleted] 		    BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[TravelRate]
(
	[ID] 			    	INT 	        NOT NULL 	PRIMARY KEY IDENTITY(1,1),
    [PickupPointID]       	INT    			NOT NULL,
    [DestinationPointID]  	INT    			NOT NULL,
    [Kilometer]  			DECIMAL(7, 2)    			NOT NULL	DEFAULT 0,
    [FixedPrice]        	DECIMAL(7, 2)   NOT NULL    DEFAULT 0,
    [Overtime]          	DECIMAL(7, 2)   NOT NULL    DEFAULT 0,
    [Overnight]         	DECIMAL(7, 2)   NOT NULL    DEFAULT 0,
    [MaxHours] 	        	INT 			NOT NULL 	DEFAULT 0, 
    [Deleted] 		    	BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	    	INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	    	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	    	INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	    	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[Location]
(
	[ID] 			    INT 	        NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
    [ParentLocationID] 	INT 			NOT NULL 	DEFAULT 0, 
    [Name]              NVARCHAR(50)    NOT NULL,
    [Description]       NVARCHAR(255)   NULL,
    [Deleted] 		    BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[Booking]
(
	[ID]			        INT				NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
	[BookingNo]		        NVARCHAR(10) 	NOT NULL,
    [UserID]		        INT				NOT NULL,
    [DriverUserID]		    INT 	        NOT NULL,
    [TravelRateID]		    INT				NOT NULL,
    [PaymetTypeID]		    INT				NOT NULL, 
    [TransactionID]		    NVARCHAR(100)	NOT NULL, 
    [Seater]		        INT				NOT NULL    DEFAULT 1, 
	[TotalPrice]	        DECIMAL(7, 2) 	NOT NULL,
	[Kilometer]				DECIMAL(7, 2) 	NOT NULL,
	[Currency]		        NVARCHAR(10) 	NOT NULL    DEFAULT 'PHP',
    [StartDateScheduled]    DATETIME 	    NOT NULL,
    [EndDateScheduled]      DATETIME 	    NOT NULL,
    [DateCompleted] 	    DATETIME 	    NULL,
    [Status]                NVARCHAR(50)    NOT NULL,
	[Deleted]               BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	        INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	        DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	        INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	        DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[Discount]
(
	[ID] 			    INT 	        NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
    [Name]              NVARCHAR(50)    NOT NULL,
	[DiscountAmount]	DECIMAL(7, 2) 	NOT NULL	DEFAULT 0,
    [HasCouponCode]     BIT				NOT NULL	DEFAULT 0,
	[CouponCode]		NVARCHAR(20) 	NULL,
    [StartDate] 	    DATETIME 		NOT NULL, 
    [EndDate] 			DATETIME 		NOT NULL, 
    [Deleted] 		    BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[DiscountUsageHistory]
(
	[ID] 			    INT 	        NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
    [DiscountID] 	    INT 			NOT NULL,
    [BookingID] 	    INT 			NOT NULL,
    [CreatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(),
)

CREATE TABLE [dbo].[BookingReview]
(
	[ID] 			    INT 	        NOT NULL 	PRIMARY KEY IDENTITY(1,1), 
    [BookingID] 	    INT 			NOT NULL, 
    [UserID]            INT				NOT NULL,
	[Message]			NVARCHAR(MAX) 	NOT NULL,
    [Rating]			INT				NOT NULL	DEFAULT 0,
    [Deleted] 		    BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	    INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	    DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[TopicPage]
(
	[ID]			INT				NOT NULL 	PRIMARY KEY IDENTITY(1,1),
    [Name]			NVARCHAR(50)	NOT NULL, 
	[UrlSlug]		NVARCHAR(255) 	NOT NULL,
	[Html]			NVARCHAR(MAX)   NULL,
	[Display] 		BIT 			NOT NULL 	DEFAULT 1, 
    [DisplayOrder]	INT				NOT NULL 	DEFAULT 0,
	[Deleted] 		BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

CREATE TABLE [dbo].[Notification]
(
	[ID]			INT				NOT NULL 	PRIMARY KEY IDENTITY(1,1),
    [UserID]		INT				NOT NULL,
    [Message]		NVARCHAR(MAX)   NULL,
    [IsClicked]     BIT             NOT NULL    DEFAULT 0, 
	[Deleted] 		BIT 			NOT NULL 	DEFAULT 0, 
    [CreatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateCreated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE(), 
    [UpdatedBy] 	INT 			NOT NULL 	DEFAULT 0, 
    [DateUpdated] 	DATETIME 		NOT NULL 	DEFAULT GETUTCDATE()
)

-- Password: Login@123
INSERT INTO [dbo].[User]
(Firstname,Middlename,Lastname,EmailAddress,Username,Password,PasswordSaltKey,RoleID,Verified) 
VALUES 
('Bryan Miguel','Yukong','Pogenio','bryanmiguelpogenio@gmail.com','Administrator',
'454918BCD02BA1344B76095C0411F55590288879','nRVIyzs=', 1, 1);
INSERT INTO [dbo].[User]
(Firstname,Middlename,Lastname,EmailAddress,Username,Password,PasswordSaltKey,RoleID,Verified) 
VALUES 
('Juan Miguel','Dimansalang','Dela Cruz','driver01@gmail.com','driver01',
'212C947E8790BE4DA843230D8464BA26F13C4C52','UZDtSo8=', 3, 1);
INSERT INTO [dbo].[User]
(Firstname,Middlename,Lastname,EmailAddress,Username,Password,PasswordSaltKey,RoleID,Verified) 
VALUES 
('Gregorio','Pulag','Del Pilar','driver02@gmail.com','driver02',
'350FBA0CA37AE592772B8E83D14CAAA9FC63EC7F','iDhinOM=', 3, 1);
INSERT INTO [dbo].[User]
(Firstname,Middlename,Lastname,EmailAddress,Username,Password,PasswordSaltKey,RoleID,Verified) 
VALUES 
('Jose','Potracio','Rizal','driver03@gmail.com','driver03',
'CE9E8B5642C8AC0E91E9FEF2A754783D3B7B99FF','eBait9Q=', 3, 1);

INSERT INTO [dbo].[Role](Name, Description) VALUES ('Admin', 'Administator Role');
INSERT INTO [dbo].[Role](Name, Description) VALUES ('Customer', 'Customer Role');
INSERT INTO [dbo].[Role](Name, Description) VALUES ('Driver', 'Driver Role');

INSERT INTO [dbo].[PaymentType](Name, Description) VALUES ('Credit Card', 'Pay using your credit card');
INSERT INTO [dbo].[PaymentType](Name, Description) VALUES ('Paypal', 'Pay using your paypal');
INSERT INTO [dbo].[PaymentType](Name, Description) VALUES ('Cash On Hand', 'Pay on the day of booking');

INSERT INTO [dbo].[ServiceType](Name, Description) VALUES ('Land Tours', 'Be there on your targeted tourist destinations 
anywhere in the Philippines. We can acommodate you with a safe and comfotable trip.');
INSERT INTO [dbo].[ServiceType](Name, Description) VALUES ('Hotel/Airport Services', 'Book with us and we can give you 
a solution to your large lauggages. We can transer your group in any hotel in Metro Manila');

INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (0, 'Metro Manila');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (1, 'NAIA');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (0, 'Cavite');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Kawit');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Noveleta');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Rosario');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Bacoor');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Imus');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Gen, Trias');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Dasmarinas');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Trece Martires');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Tanza');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Naic');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Ternate');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Maragondon');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Indang');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Mendez');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Amadeo');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Alfonzo');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Bailen');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Magallanes');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Silang');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (3, 'Carmona');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (0, 'Laguna');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'San Pedro');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Splash Island');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Binan');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Sta. Rosa');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Cabuyao');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Calamba');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Los Banos');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Calauan');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Pandin Lake');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Alaminos');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'San Pablo');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Sta. Cruz');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Nagcarlan');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Majayjay');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Pagsanjan');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Lumban');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Paete');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Pangil');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (24, 'Famy');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (0, 'Quezon');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Real');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Infanta');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Mauban');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Dolores');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Tiaong');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Villa Escudero');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Candelaria');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Sariaya');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Lucena');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Tayabas');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Pagbilao');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Atimonan');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Gumaca');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Gen, Luna');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'San Andres Port');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'Calauag');
INSERT INTO [dbo].[Location](ParentLocationID, Name) VALUES (44, 'San Narciso');

INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 1, 0, 3500, 200, 10);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 2, 15.6, 2500, 250, 10);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 3, 64.3, 2500, 250, 10);
																  
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 4, 27.3, 3600, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 5, 31.0, 3600, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 6, 33.0, 4000, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 7, 28.3, 3500, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 8, 26.7, 3600, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 9, 50.4, 3700, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 10, 50.1, 4700, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 11, 61.7, 4900, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 12, 41.4, 4200, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 13, 50.8, 4500, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 14, 64.1, 5000, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 15, 68.1, 4800, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 16, 65.8, 5000, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 17, 76.6, 5500, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 18, 68.0, 5200, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 19, 81.0, 5700, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 20, 68.9, 5300, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 21, 73.4, 5500, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 22, 54.1, 4800, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 23, 38.9, 4400, 0, 0);
																  																 
																																 
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 24, 33.5, 4200, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 25, 33.5, 4200, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 26, 35.6, 4100, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 27, 38.1, 4200, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 28, 51.3, 4500, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 29, 51.5, 4600, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 30, 53.3, 5000, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 31, 68.9, 5300, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 32, 86.6, 5700, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 33, 92.8, 6200, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 34, 78.2, 5700, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 35, 85.8, 6200, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 36, 92.3, 6200, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 37, 91.3, 6500, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 38, 104, 6700, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 39, 98.4, 6300, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 40, 107, 6700, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 41, 88.6, 6000, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 42, 75.4, 5500, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 43, 75.2, 5500, 0, 0);
																  																 
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 44, 90.3, 7000, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 45, 90.3, 7000, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 46, 128, 7500, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 47, 142, 7900, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 48, 95.7, 6400, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 49, 98.2, 6300, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 50, 94.3, 6200, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 51, 110, 7400, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 52, 135, 7500, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 53, 151, 8300, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 54, 141, 7800, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 55, 148, 8600, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 56, 175, 9500, 0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 57, 204, 10200 ,0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 58, 238, 11200 ,0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 59, 311, 14500 ,0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 60, 249, 12000 ,0, 0);
INSERT INTO [dbo].[TravelRate](PickupPointID, DestinationPointID, Kilometer, FixedPrice, Overtime, MaxHours) VALUES(1, 61, 279, 13400 ,0, 0);

INSERT INTO [dbo].[UserCar](UserID, Brand, Model, Color, PlateNumber, IsActive, TotalKilometer) VALUES (2, 'Toyota', 'Grandia', 'Gray', 'THP 123', 1, 500)
INSERT INTO [dbo].[UserCar](UserID, Brand, Model, Color, PlateNumber, IsActive, TotalKilometer) VALUES (3, 'Nissan', 'NV350', 'Black', 'THP 234', 1, 500)
INSERT INTO [dbo].[UserCar](UserID, Brand, Model, Color, PlateNumber, IsActive, TotalKilometer) VALUES (4, 'Toyota', 'Alphard', 'White', 'THP 345', 1, 500)

INSERT INTO [dbo].[Discount](Name, DiscountAmount, HasCouponCode, CouponCode, StartDate, EndDate) VALUES ('Discount 1', 100, 1, 'THPDISCOUNT1', DATEADD(DAY, -30, GETDATE()), DATEADD(DAY, 30, GETDATE()))
INSERT INTO [dbo].[Discount](Name, DiscountAmount, HasCouponCode, CouponCode, StartDate, EndDate) VALUES ('Discount 2', 200, 1, 'THPDISCOUNT2', DATEADD(DAY, -30, GETDATE()), DATEADD(DAY, 30, GETDATE()))
INSERT INTO [dbo].[Discount](Name, DiscountAmount, HasCouponCode, CouponCode, StartDate, EndDate) VALUES ('Discount 3', 300, 1, 'THPDISCOUNT3', DATEADD(DAY, -30, GETDATE()), DATEADD(DAY, 30, GETDATE()))
INSERT INTO [dbo].[Discount](Name, DiscountAmount, HasCouponCode, CouponCode, StartDate, EndDate) VALUES ('Discount 4', 400, 1, 'THPDISCOUNT4', DATEADD(DAY, -30, GETDATE()), DATEADD(DAY, 30, GETDATE()))
INSERT INTO [dbo].[Discount](Name, DiscountAmount, HasCouponCode, CouponCode, StartDate, EndDate) VALUES ('Discount 5', 500, 1, 'THPDISCOUNT4', DATEADD(DAY, -30, GETDATE()), DATEADD(DAY, 30, GETDATE()))