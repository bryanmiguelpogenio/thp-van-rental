﻿using System.Web;
using System.Web.Optimization;

namespace THP.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.3.1.min.js",
                        "~/Scripts/jquery.unobtrusive-ajax.min.js",
                        "~/Scripts/jquery.validate.min.js",
                        "~/Scripts/jquery.validate.unobtrusive.min.js",
                        "~/Scripts/expressive.annotations.validate.js",
                        "~/Scripts/expressive.annotations.validate.min.js",
                        "~/Scripts/jquery-migrate.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/Client/popper.min.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/Client/plugins/gijgo.js",
                      "~/Scripts/Client/plugins/vegas.min.js",
                      "~/Scripts/Client/plugins/isotope.min.js",
                      "~/Scripts/Client/plugins/owl.carousel.min.js",
                      "~/Scripts/Client/plugins/waypoints.min.js",
                      "~/Scripts/Client/plugins/counterup.min.js",
                      "~/Scripts/Client/plugins/mb.YTPlayer.js",
                      "~/Scripts/Client/plugins/magnific-popup.min.js",
                      "~/Scripts/Client/plugins/slicknav.min.js",
                      "~/Scripts/Client/main.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Client/css/bootstrap.min.css",
                      "~/Content/Client/css/plugins/vegas.min.css",
                      "~/Content/Client/css/plugins/slicknav.min.css",
                      "~/Content/Client/css/plugins/magnific-popup.css",
                      "~/Content/Client/css/plugins/owl.carousel.min.css",
                      "~/Content/Client/css/plugins/gijgo.css",
                      "~/Content/Client/css/font-awesome.css",
                      "~/Content/Client/css/reset.css",
                      "~/Content/Client/style.css",
                      "~/Content/Client/css/responsive.css",
                      "~/Content/Client/custom-style.css"));
        }
    }
}
