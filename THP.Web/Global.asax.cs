﻿using System.Globalization;
using System.Threading;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusive.Validators;
using THP.Web.Models.Configuration;
using THP.Web.Models.DAL;
using THP.Web.Models.DAL.Repositories;
using THP.Web.Models.Entities;
using THP.Web.Models.Interface.Configuration;
using THP.Web.Models.Interface.DAL;
using THP.Web.Models.Interface.Entities;
using THP.Web.Models.Interface.Security;
using THP.Web.Models.Security;

namespace THP.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //CultureInfo newCulture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            //newCulture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";
            //newCulture.DateTimeFormat.DateSeparator = "/";
            //Thread.CurrentThread.CurrentCulture = newCulture;

            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(WebApiApplication).Assembly);
            builder.RegisterApiControllers(typeof(WebApiApplication).Assembly);

            //--> Data Access Layer(s)
            builder.RegisterType<DBContext>().As<IDBContext>().InstancePerLifetimeScope();

            //--> Repositories
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>));
            builder.RegisterType<BookingRepository>().As<IBookingRepository>().InstancePerLifetimeScope();
            builder.RegisterType<LocationRepository>().As<ILocationRepository>().InstancePerLifetimeScope();
            builder.RegisterType<PageRepository>().As<IPageRepository>().InstancePerLifetimeScope();
            builder.RegisterType<PaymentTypeRepository>().As<IPaymentTypeRepository>().InstancePerLifetimeScope();
            builder.RegisterType<RoleRepository>().As<IRoleRepository>().InstancePerLifetimeScope();
            builder.RegisterType<ServiceTypeRepository>().As<IServiceTypeRepository>().InstancePerLifetimeScope();
            builder.RegisterType<SpecificationRepository>().As<ISpecificationRepository>().InstancePerLifetimeScope();
            builder.RegisterType<TravelRateRepository>().As<ITravelRateRepository>().InstancePerLifetimeScope();
            builder.RegisterType<UserCarRepository>().As<IUserCarRepository>().InstancePerLifetimeScope();
            builder.RegisterType<UserCarSpecificationRepository>().As<IUserCarSpecificationRepository>().InstancePerLifetimeScope();
            builder.RegisterType<UserDetailRepository>().As<IUserDetailRepository>().InstancePerLifetimeScope();
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerLifetimeScope();
            builder.RegisterType<EncryptionService>().As<IEncryptionService>().InstancePerLifetimeScope();

            //--> Braintree
            builder.RegisterType<BraintreeConfiguration>().As<IBraintreeConfiguration>().InstancePerLifetimeScope();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver((IContainer)container);

            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredIfAttribute), typeof(RequiredIfValidator));
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(AssertThatAttribute), typeof(AssertThatValidator));
        }
    }
}
